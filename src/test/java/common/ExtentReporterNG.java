package common;



import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;


 
public class ExtentReporterNG implements ITestListener{
static ExtentReports extent;
	
	public static ExtentReports getReportObject()
	{
		
		String path =System.getProperty("user.dir")+"\\reports\\Extentindex.html";
		ExtentHtmlReporter reporter = new ExtentHtmlReporter(path);
		reporter.config().setReportName("Web Automation Results");
		reporter.config().setDocumentTitle("Test Results");
		
		extent = new ExtentReports();
		extent.attachReporter(reporter);
		extent.setSystemInfo("Tester", "Rahul Shetty");
		return extent;
		
	}

	public void onTestStart(ITestResult result) {
		// TODO Auto-generated method stub
		
	}

	public void onTestSuccess(ITestResult result) {
		// TODO Auto-generated method stub
		
	}

	public void onTestFailure(ITestResult result) {
		// TODO Auto-generated method stub
		
	}

	public void onTestSkipped(ITestResult result) {
		// TODO Auto-generated method stub
		
	}

	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
		// TODO Auto-generated method stub
		
	}

	public void onStart(ITestContext context) {
		// TODO Auto-generated method stub
		
	}

	public void onFinish(ITestContext context) {
		// TODO Auto-generated method stub
		
	}
}

//    private ExtentReports extent;
// 
//    public void generateReport(List<XmlSuite> xmlSuites, List<ISuite> suites, String outputDirectory) {
//        extent = new ExtentReports(outputDirectory + File.separator + "ExtentReportsTestNGRGGG.html", true);
// 
//        for (ISuite suite : suites) {
//            Map<String, ISuiteResult> result = suite.getResults();
// 
//            for (ISuiteResult r : result.values()) {
//                ITestContext context = r.getTestContext();
// 
//                buildTestNodes(context.getPassedTests(), LogStatus.PASS);
//                buildTestNodes(context.getFailedTests(), LogStatus.FAIL);
//                buildTestNodes(context.getSkippedTests(), LogStatus.SKIP);
//            }
//        }
// 
//        extent.flush();
//        extent.close();
//    }
// 
//    private void buildTestNodes(IResultMap tests, LogStatus status) {
//        ExtentTest test;
// 
//        if (tests.size() > 0) {
//            for (ITestResult result : tests.getAllResults()) {
//                test = extent.startTest(result.getMethod().getMethodName());
// 
//                /*test.getTest(). = getTime(result.getStartMillis());
//                test.getTest().endedTime = getTime(result.getEndMillis());*/
// 
//                for (String group : result.getMethod().getGroups())
//                    test.assignCategory(group);
// 
//                String message = "Test " + status.toString().toLowerCase() + "ed";
// 
//                if (result.getThrowable() != null)
//                    message = result.getThrowable().getMessage();
// 
//                test.log(status, message);
// 
//                extent.endTest(test);
//            }
//        }
//    }
// 
//    private Date getTime(long millis) {
//        Calendar calendar = Calendar.getInstance();
//        calendar.setTimeInMillis(millis);
//        return calendar.getTime();        
//    }
//}