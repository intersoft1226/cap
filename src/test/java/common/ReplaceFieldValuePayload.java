package common;

import com.fasterxml.jackson.annotation.PropertyAccessor;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ser.std.ObjectArraySerializer;
import com.fasterxml.jackson.datatype.hibernate4.Hibernate4Module;
import gherkin.deps.com.google.gson.Gson;
import pojo.Children;
import pojo.Custom;
import pojo.EmailAcquisition;
//import pojo.TestPojo;

public class ReplaceFieldValuePayload extends Utils{
	String updatedPayload = null;
	String fieldName;


	public String updatedPayloadData(EmailAcquisition strToJson) throws JsonProcessingException {
		ObjectMapper obj = new ObjectMapper();	
		obj.setSerializationInclusion(Include.NON_NULL);		
		obj.setVisibility(PropertyAccessor.FIELD, Visibility.ANY);
		obj.registerModule(new Hibernate4Module());
		updatedPayload = obj.writerWithDefaultPrettyPrinter().writeValueAsString(strToJson);
		FileLoggers.info(updatedPayload);
		System.out.println("Updated Json is " + updatedPayload);
		return updatedPayload;
	}

	public String replaceFieldValue(String payload, String fieldName, String fieldValue)
			throws JsonProcessingException {
		Gson gson = new Gson();
		gson.fromJson(payload, EmailAcquisition.class);
		EmailAcquisition strToJson = gson.fromJson(payload, EmailAcquisition.class);
		this.fieldName = fieldName;
		System.out.println(this.fieldName);
		switch (fieldName) {
		case "email":
			strToJson.setEmail(fieldValue);
			break;
		case "id":
			break;
		case "addr1":
			strToJson.setAddr1(fieldValue);
			break;
		case "addr2":
			strToJson.setAddr2(fieldValue);
			break;
		case "firstName":
			strToJson.setFirstName(fieldValue);
			break;
		case "lastName":
			strToJson.setLastName(fieldValue);
			break;
		case "city":
			strToJson.setCity(fieldValue);
			break;
		case "postalCode":
			strToJson.setPostalCode(fieldValue);
			break;
		case "province":
			strToJson.setProvince(fieldValue);
			break;
		case "state":
			strToJson.setState(fieldValue);
			break;
		case "title":
			strToJson.setTitle(fieldValue);
			break;
		case "phone":
			strToJson.setPhone(fieldValue);
			break;
		case "mocode":
			strToJson.setMocode(fieldValue);
			break;
		case "mobilePhone":
			strToJson.setMobilePhone(fieldValue);
			break;
		case "zipcode":
			strToJson.setZipcode(fieldValue);
			break;
		case "babyFields":
			int value = Integer.parseInt(fieldValue);
			strToJson.setBabyFields(value);
			break;
		case "Children_firstName":
			List<pojo.Children> listA = strToJson.getChildren();
			listA.get(0).setFirstName(fieldValue);
			break;
		case "firstNameUnknown":
			List<pojo.Children> listB = strToJson.getChildren();
			listB.get(0).setFirstNameUnknown(fieldValue);
			break;
		case "hasDateOfBirth":
			List<pojo.Children> listC = strToJson.getChildren();
			listC.get(0).setHasDateOfBirth(fieldValue);
			break;
		case "dateOfBirth":
			List<pojo.Children> listD = strToJson.getChildren();
			listD.get(0).setDateOfBirth(fieldValue);
			break;
		case "expectedBirthYear":
			List<pojo.Children> listE = strToJson.getChildren();
			listE.get(0).setExpectedBirthYear(fieldValue);
			break;
		case "expectedBirthMonth":
			List<pojo.Children> listF = strToJson.getChildren();
			listF.get(0).setExpectedBirthMonth(fieldValue);
			break;
		case "gender":
			List<pojo.Children> listG = strToJson.getChildren();			
			listG.get(0).setGender(fieldValue);			
			break;
		case "emailconsent2":
			strToJson.setEmailconsent("");
			strToJson.setEmailconsent2("");
			break;
		case "CmpName":
			strToJson.setCmpName(fieldValue);
			break;
		case "CmpDeploymentStrategy":
			strToJson.setCmpDeploymentStrategy(fieldValue);
			break;
		case "CmpVariationID":
			strToJson.setCmpVariationID(fieldValue);
			break;
		case "CmpConcept":
			strToJson.setCmpConcept(fieldValue);
			break;
		case "CmpFormat":
			strToJson.setCmpFormat(fieldValue);
			break;
		case "CmpDevice":
			strToJson.setCmpDevice(fieldValue);
			break;
		case "guardian":
			strToJson.setGuardian(fieldValue);
			break;
		case "genderThirdChild":
			List<pojo.Children> listH = strToJson.getChildren();			
			listH.get(2).setGender(fieldValue);			
			break;
		}
		updatedPayloadData(strToJson);
		return updatedPayload;
	}


	public String customReplaceFieldValue(String Payload, String FieldName, String FieldValue, int setCount)
			throws JsonMappingException, JsonProcessingException {
		JSONObject Payload_Updated = new JSONObject(Payload);
		JSONArray array = Payload_Updated.getJSONArray("custom");
		Payload_Updated = new JSONObject(Payload);
		array = Payload_Updated.getJSONArray("custom");
		System.out.println(array);
		JSONObject customObject = (JSONObject) array.get(setCount);
		customObject.remove("id");
		customObject.remove("value");
		customObject.put("id", FieldName);
		customObject.put("value", FieldValue);
		System.out.println(Payload_Updated);
		updatedPayload = Payload_Updated.toString();
		return updatedPayload;
	}

	public String customRemoveFieldId(String Payload, String FieldName, String FieldValue, int setCount)
			throws JsonMappingException, JsonProcessingException {
		JSONObject Payload_Updated = new JSONObject(Payload);
		JSONArray array = Payload_Updated.getJSONArray("custom");
		Payload_Updated = new JSONObject(Payload);
		array = Payload_Updated.getJSONArray("custom");
		System.out.println(array);
		JSONObject customObject = (JSONObject) array.get(setCount);
		customObject.remove("id");
		System.out.println(Payload_Updated);
		updatedPayload = Payload_Updated.toString();
		FileLoggers.info("Updated Payload for customRemoveFieldId is " + updatedPayload);
		return updatedPayload;
	}

	// ***********************

	public void blankAndNullPayloadGenerator(String payload, String fieldName, String fieldValue) throws JsonProcessingException {

		Gson gson = new Gson();
		gson.fromJson(payload, EmailAcquisition.class);
		EmailAcquisition strToJson = gson.fromJson(payload, EmailAcquisition.class);
//		convertCustomIdAsBlank();
//		System.out.println("Updated Json is " + updatedPayload);
	}
	
	

}
