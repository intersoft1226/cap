package common;

import io.restassured.specification.RequestSpecification;

public enum APIResources {
	
	EmailAcquistionAPI("mti-api/cap/v1/email/"),
	MobileAcquisitionAPI("mti-api/cap/v1/mobile/"),
	DataCollectionAPI("mti-api/cap/v1/data-collection/"),
	InstantOfferNoESPAPI("mti-api/cap/v1/instant-offer/noesp"),
	InstantOfferAPI("mti-api/cap/v1/instant-offer/");

	private String resource;

	APIResources(String resource) {
		this.resource = resource;
	}

	public String getResource() {
		return resource;
	}

	

}
