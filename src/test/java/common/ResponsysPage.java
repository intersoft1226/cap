package common;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Objects;

import io.restassured.path.json.JsonPath;

public class ResponsysPage extends Utils {
	
	ResponsysPage(){
		try {
			baseURL = getGlobalProperty("responsePageURL");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// "https://storage.googleapis.com/mtim2m-test-template-assets/templates/CTSAPI_Anirudh/emailNew.html",
	String baseURL;
	String environment;
	String assestPath = "-template-assets/templates/";
	String templateType;
	String templateID;
	String htmlPage;
	String responsysURL;
	String queryTemplate;
	String columnValue;
	public JsonPath jsonPathResponse;
	


	public String getHtmlPage(String templateType, String response) {

		String apiType = templateType;

		switch (apiType) {
		case "MobileAcquisitionAPI":
			htmlPage = mobileHtml(response);
			break;
		case "EmailAcquistionAPI":
			htmlPage = emailHtml(response);
			break;
		case "DataCollectionAPI":
			htmlPage = dataCollectionHtml(response);
			break;
		case "InstantOfferAPI":
			htmlPage = instantofferHtml(response);
			break;
		case "InstantOfferNoESPAPI":
			htmlPage = instantofferNoESPHtml(response);
			break;
		default:
			FileLoggers.info("Template type is not correct");
		}

		return htmlPage;
	}

	public String createUrl(String resourceType, String templateName, String response) {
		try {
			environment = getGlobalProperty("Environment");
		} catch (IOException e) {
			FileLoggers.info("Not able to read the environment from global property file");
		}	
		templateID = templateName + "/";
		queryTemplate= templateName;
		htmlPage = getHtmlPage(resourceType, response);
		if (htmlPage != null) {
			responsysURL = baseURL + environment + assestPath + templateID + htmlPage;
			return responsysURL;
		} else {
			responsysURL = null;
			return responsysURL;
		}
	}

	public String emailHtml(String apiResponse) {
		String emailPage = "";
		jsonPathResponse = new JsonPath(apiResponse);
		String message = jsonPathResponse.get("results.emailResult.errorMessages[0]");
		Boolean success = jsonPathResponse.get("results.emailResult.success");

		if (jsonPathResponse.getInt("statusCode") == 200
				&& Objects.nonNull(jsonPathResponse.get("results.emailResult"))) {
			if (jsonPathResponse.getBoolean("results.emailResult.isNewUser")) {
				emailPage = "emailNew.html";
			} else if ((boolean) jsonPathResponse.get("results.emailResult.isSpam")) {
				emailPage = "emailSpam.html";
			} else if ((boolean) jsonPathResponse.get("results.emailResult.isUndel")) {
				emailPage = "EmailUndel.html";
			} else if ((boolean) jsonPathResponse.get("results.emailResult.isLti")) {
				emailPage = "emailLti.html";
			} else if ((boolean) jsonPathResponse.get("results.emailResult.isOptIn")) {
				emailPage = "emailOptOut.html";
			} else {
				emailPage = "emailExisting.html";
			}

		} else {
			emailPage = "apiError.html";
		}
		return emailPage;
	}

	public String mobileHtml(String apiResponse) {
		String mobilePage = "";
		jsonPathResponse = new JsonPath(apiResponse);
		try {
			if (jsonPathResponse.getBoolean(("results.smsResult.success"))) {
				if (jsonPathResponse.getBoolean(("results.smsResult.isNewUser"))) {
					mobilePage = "mobileNew.html";
				} else if ((jsonPathResponse.get(("results.smsResult.isNewUser"))).equals(false)) {
					mobilePage = "mobileExisting.html";
				} else {
					mobilePage = "mobileError.html";
				}
			} else {
			}
		} catch (Exception e) {
			mobilePage = "mobileError.html";
		}
		return mobilePage;

	}

	public String dataCollectionHtml(String apiResponse) {
		jsonPathResponse = new JsonPath(apiResponse);
		String dataCollectionHtml;
		if (jsonPathResponse.getInt("statusCode") == 200) {
			dataCollectionHtml = "success.html";
		} else {
			dataCollectionHtml = "error.html";
		}

		return dataCollectionHtml;
	}

	public String instantofferHtml(String apiResponse) {
		String instantofferHtml = "";
				
		jsonPathResponse = new JsonPath(apiResponse);
		if (jsonPathResponse.getInt("statusCode") == 200 && Objects.isNull(jsonPathResponse.get("results.myOffersResult"))) {
				if (jsonPathResponse.getBoolean("results.emailResult.isNewUser")) {
					instantofferHtml = "emailNew.html";
				} else if (jsonPathResponse.getBoolean("results.emailResult.isSpam")) {
					instantofferHtml = "emailSpam.html";
				} else if (jsonPathResponse.getBoolean("results.emailResult.isUndel")) {
					instantofferHtml = "emailUndel.html";
				} else if (jsonPathResponse.getBoolean("results.emailResult.isLti")) {
					instantofferHtml = "emailLti.html";
				} else if (jsonPathResponse.getBoolean("results.emailResult.isOptIn")) {
					instantofferHtml = "emailoptOut.html";
				} else if(!jsonPathResponse.getBoolean("results.emailResult.success")){
					instantofferHtml = "espApiError.html";
				} else {
					instantofferHtml = "emailExisting.html";
				}
				
		}else if(jsonPathResponse.getInt("statusCode") == 200 && Objects.nonNull(jsonPathResponse.get("results.myOffersResult"))) {
			if (jsonPathResponse.getInt(("results.myOffersResult.offerStatusCd")) == 500
					|| jsonPathResponse.getInt(("results.myOffersResult.offerStatusCd")) == 701
					|| jsonPathResponse.get("results.myOffersResult.offerStatusCd").toString() == null) {
				instantofferHtml = "myOffersError.html";
			} else if (jsonPathResponse.getInt(("results.myOffersResult.offerStatusCd")) == 132 ) {
				String offerType = null;
				try {
					offerType = getresult("offerCodeType");
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if(offerType.equals("sourceId")) {
					instantofferHtml = "myOffersError.html";
				}else {
					instantofferHtml = "myOffersProxyCodeInvalid.html";	
				}
				
			} else if (jsonPathResponse.getInt(("results.myOffersResult.offerStatusCd")) == 0) {
						if (jsonPathResponse.getBoolean("results.emailResult.isLti")||jsonPathResponse.getBoolean("results.emailResult.isOptIn"))
						{
							instantofferHtml = "myOffersSuccessLti.html";
						}else {
							instantofferHtml = "myOffersSuccess.html";
						}	
			} else if (jsonPathResponse.getInt(("results.myOffersResult.offerStatusCd")) == 615
					|| jsonPathResponse.getInt(("results.myOffersResult.offerStatusCd")) == 1) {
				instantofferHtml = "myOffersOfferNotAvailable.html";
			} else if (jsonPathResponse.getInt(("results.myOffersResult.offerStatusCd")) == 580) {
				instantofferHtml = "myOffersProxyCodeUnavailable.html";
				}
			} 
			else if (jsonPathResponse.getInt(("statusCode")) == 400) {
				instantofferHtml = null;
			}
		
			
		return instantofferHtml;
	}

	public String instantofferNoESPHtml(String apiResponse) {
		String instantofferNoESPHtml = null;
		jsonPathResponse = new JsonPath(apiResponse);
		if (jsonPathResponse.getInt("statusCode") == 200) {
			if (jsonPathResponse.getInt(("results.myOffersResult.offerStatusCd")) == 500
					|| jsonPathResponse.getInt(("results.myOffersResult.offerStatusCd")) == 701
					|| jsonPathResponse.get("results.myOffersResult.offerStatusCd").toString() == null) {
				instantofferNoESPHtml = "myOffersError.html";
			} else if (jsonPathResponse.getInt(("results.myOffersResult.offerStatusCd")) == 132) {
				instantofferNoESPHtml = "myOffersProxyCodeInvalid.html";
			} else if (jsonPathResponse.getInt(("results.myOffersResult.offerStatusCd")) == 0) {
				instantofferNoESPHtml = "myOffersSuccess.html";
			} else if (jsonPathResponse.getInt(("results.myOffersResult.offerStatusCd")) == 615
					|| jsonPathResponse.getInt(("results.myOffersResult.offerStatusCd")) == 1) {
				instantofferNoESPHtml = "myOffersOfferNotAvailable.html";
			} else if (jsonPathResponse.getInt(("results.myOffersResult.offerStatusCd")) == 580) {
				instantofferNoESPHtml = "myOffersProxyCodeUnavailable.html";
			}
		} else if (jsonPathResponse.getInt(("statusCode")) == 400) {
			instantofferNoESPHtml = null;
		}
		return instantofferNoESPHtml;
	}
	
	public String getresult(String columnName) throws SQLException {
		String query = "SELECT * FROM template T INNER JOIN instant_offer I ON T.templatekey = I.templatekey WHERE T.templateid = '"+queryTemplate+"'"; 
		ResultSet queryresult =CommonLib.db.getSQLResults(query);
		
		while (queryresult.next()) {
			String columnValue = queryresult.getString("createdBy");
			System.out.println(columnValue);
		}
		
		return columnValue;
	}
}