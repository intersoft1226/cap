package common;

import java.io.IOException;
//import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

//import com.fasterxml.jackson.core.JsonProcessingException;
//import com.fasterxml.jackson.databind.JsonMappingException;
//import com.fasterxml.jackson.databind.ObjectMapper;

import fieldValidations.Address1;
import fieldValidations.Address2;
import fieldValidations.BabyField;
import fieldValidations.BounceXCustomFields;
import fieldValidations.Children;
import fieldValidations.City;
import fieldValidations.CustomFields;
import fieldValidations.Email;
import fieldValidations.EmailConsent2;
import fieldValidations.FirstName;
import fieldValidations.Guardian;
import fieldValidations.LastName;
import fieldValidations.MoCode;
import fieldValidations.MobilePhone;
import fieldValidations.Phone;
import fieldValidations.PostalCode;
import fieldValidations.Province;
import fieldValidations.State;
import fieldValidations.Title;
import fieldValidations.ZipCode;
import io.cucumber.core.api.Scenario;
//import pojo.Custom;

public class TemplateValidation {
	String id;
	String value = null;
	int colCount;
	int setCount=0;
	
	public void fieldValidation(String methodName, String resourceType, String payloads, String sheetPath,
			String sheetName, Integer rowIndex, Scenario scenario) throws IOException, SQLException {
		GetListofFields fields = new GetListofFields();
		List<String> fieldList = fields.fieldlist(payloads);
		scenario.write("List of Fields in template  >> "+fieldList);
		Iterator<String> itr = fieldList.iterator();
		while (itr.hasNext()) {
			String fieldName = itr.next();
			switch (fieldName) {
			case "custom":
				CustomFields cf = new CustomFields();
				DataSet ds = new DataSet();
				List<String> mylist1 = new ArrayList<String>();
				mylist1 = ds.getDataList(CommonLib.sheetPath, CommonLib.sheetName, CommonLib.rowIndex);
				colCount = mylist1.size();
				for (int t = 50; t <= colCount - 2; t++) {
					id = mylist1.get(t); // test26
					value = mylist1.get(t + 1); // value26
					if (id != null) {
						cf.validateCustomType(methodName, resourceType, payloads, id, value, setCount);
						setCount++;
						t++;
					} else {
						break;
					}
				}
				scenario.write("Validated Custom Field validations.");
				break;
			case "id":
				break;
			case "email":
				Email email = new Email();
				email.validatingAllEmailScenarios(methodName, resourceType, payloads);
				scenario.write("Validated Email Field validations.");
				break;
			case "addr1":
				Address1 adr = new Address1();
				adr.validatingAllAddress1Scenarios(methodName, resourceType, payloads);
				scenario.write("Validated Address1 Field validations.");
				break;
			case "addr2":
				Address2 adr2 = new Address2();
				adr2.validatingAllAddress2Scenarios(methodName, resourceType, payloads);
				scenario.write("Validated Address2 Field validations.");
				break;
			case "firstName":
				FirstName firstName = new FirstName();
				firstName.validatingAllFirstNameScenarios(methodName, resourceType, payloads);
				scenario.write("Validated FirstName Field validations.");
				break;
			case "lastName":
				LastName lastName = new LastName();
				lastName.validatingAllLastNameScenarios(methodName, resourceType, payloads);
				scenario.write("Validated LastName Field validations.");
				break;
			case "city":
				City city = new City();
				city.validatingAllCityScenarios(methodName, resourceType, payloads);
				scenario.write("Validated City Field validations.");
				break;
			case "postalCode":
				PostalCode postalCode = new PostalCode();
				postalCode.validatingAllPostalCodeScenarios(methodName, resourceType, payloads);
				scenario.write("Validated PostalCode Field validations.");
				break;
			case "province":
				Province province = new Province();
				province.validatingAllProvinceScenarios(methodName, resourceType, payloads);
				scenario.write("Validated Province Field validations.");
				break;
			case "state":
				State state = new State();
				state.validatingAllStateScenarios(methodName, resourceType, payloads);
				scenario.write("Validated State Field validations.");
				break;
			case "title":
				Title title = new Title();
				title.validatingAllTitleScenarios(methodName, resourceType, payloads);
				scenario.write("Validated Title Field validations.");
				break;
			case "phone":
				Phone phone = new Phone();
				phone.validatingAllPhoneScenarios(methodName, resourceType, payloads);
				scenario.write("Validated Phone Field validations.");
				break;
			case "mocode":
				MoCode moCode = new MoCode();
				moCode.validatingAllMoCodeScenarios(methodName, resourceType, payloads);
				scenario.write("Validated MoCode Field validations.");
				break;
			case "mobilePhone":
				MobilePhone mobilePhone = new MobilePhone();
				mobilePhone.validatingAllMobilePhoneScenarios(methodName, resourceType, payloads);
				scenario.write("Validated MobilePhone Field validations.");
				break;
			case "zipCode":
				ZipCode zip = new ZipCode();
				zip.validatingAllZipCodeScenarios(methodName, resourceType, payloads);
				scenario.write("Validated ZipCode Field validations.");
				break;
			case "babyFields":
				BabyField babyField = new BabyField();
				babyField.validatingAllBabyFieldScenarios(methodName, resourceType, payloads);
				scenario.write("Validated BabyField Field validations.");
				break;
			case "children":
				Children children = new Children();
				children.validatingAllChildrenFieldScenarios(methodName, resourceType, payloads);
				scenario.write("Validated Children Field validations.");
				break;
			case "emailconsent2":
				EmailConsent2 emailConsent = new EmailConsent2();
				emailConsent.EmailConsent2Mandatory(methodName, resourceType, payloads);
				scenario.write("Validated EmailConsent2 Field validations.");
				break;
			case "CmpName":
				BounceXCustomFields bounceXCustomFields = new BounceXCustomFields();
				bounceXCustomFields.CmpNameMaxLength(methodName, resourceType, payloads);
				scenario.write("Validated CmpName Field validations.");
				break;
			case "CmpDeploymentStrategy":
				BounceXCustomFields bounceXCustomFields2 = new BounceXCustomFields();
				bounceXCustomFields2.CmpDeploymentStrategyMaxLength(methodName, resourceType, payloads);
				scenario.write("Validated CmpDeploymentStrategy Field validations.");
				break;
			case "CmpVariationID":
				BounceXCustomFields bounceXCustomFields3 = new BounceXCustomFields();
				bounceXCustomFields3.CmpVariationIDMaxLength(methodName, resourceType, payloads);
				scenario.write("Validated CmpVariationID Field validations.");
				break;
			case "CmpConcept":
				BounceXCustomFields bounceXCustomFields4 = new BounceXCustomFields();
				bounceXCustomFields4.CmpConceptMaxLength(methodName, resourceType, payloads);
				scenario.write("Validated CmpConcept Field validations.");
				break;
			case "CmpFormat":
				BounceXCustomFields bounceXCustomFields5 = new BounceXCustomFields();
				bounceXCustomFields5.CmpFormatMaxLength(methodName, resourceType, payloads);
				scenario.write("Validated CmpFormat Field validations.");
				break;
			case "CmpDevice":
				BounceXCustomFields bounceXCustomFields6 = new BounceXCustomFields();
				bounceXCustomFields6.CmpDeviceMaxLength(methodName, resourceType, payloads);
				scenario.write("Validated CmpDevice Field validations.");
				break;
			case "guardian":
				Guardian guardian = new Guardian();
				guardian.guardianExceedingMaxLength(methodName, resourceType, payloads);
				scenario.write("Validated Guardian Field validations.");
				break;
			}

		}
	}
}
