package common;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;



public class GetListofFields {	
	public List<String> fieldlist(String payload) throws JsonMappingException, JsonProcessingException {	
	ObjectMapper obj = new ObjectMapper();
	HashMap<String, Object> map = new HashMap<String, Object>();
	// converting JSON string to Map for fetching Fields List
	map = obj.readValue(payload, new TypeReference<HashMap<String, Object>>() {
	});
	List<String> mylist = new ArrayList<String>();
	for (Entry<String, Object> entry : map.entrySet()) {
		mylist.add(entry.getKey());
	}
	FileLoggers.info("Fetched Fields List -- > " + mylist);
	return mylist;
	}

}
