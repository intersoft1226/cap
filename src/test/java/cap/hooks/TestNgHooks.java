package cap.hooks;

import java.io.IOException;
import java.sql.SQLException;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import common.CommonLib;
import common.DataBaseConnection;
import common.FileLoggers;

public class TestNgHooks {
	//public DataBaseConnection db ;
	@BeforeSuite
	public void createDbConnection() {
		FileLoggers.initLogger();
		CommonLib.db= new DataBaseConnection("m2m");
		try {
			CommonLib.db.dbConnection();
		} catch (IOException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		

		//FileLoggers.initLogger();

	}
	
	@AfterSuite
	public void closeDbConnection() {
		try {
			CommonLib.db.closeConnection();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
