package fieldValidations;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.json.JSONObject;
import org.junit.Assert;

import common.FileLoggers;
import common.ReplaceFieldValuePayload;
import common.RequestAPI;
import common.ResponseAPI;
import common.Utils;
import common.ValidationMessages;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class PostalCode extends Utils{


	RequestSpecification req;
	ReplaceFieldValuePayload pup = new ReplaceFieldValuePayload();
	String updatedpayload;
	Response response;
	RequestAPI api = new RequestAPI();
	ResponseAPI respapi = new ResponseAPI();
	int expectedStatusCode = 400;
	String requestId;
	String responseId;
	String templateId;
	String expectedErrorMessage = "Email Field format is invalid";
//	String expectedErrorMessage = " Field value is invalid";
//	String expectedMaxLengthErrorMessage = "Email Field should be max length 128";
//	String expected_MaxLengthErrorMessage = " Field should be max length 50";
	String responsePage;
	
	public void validatingAllPostalCodeScenarios(String methodName, String resourceType, String payload) throws IOException {
		postalCodeExceedingMaxLength(methodName,  resourceType,  payload);
		postalCodeWithInvalidFormat(methodName,  resourceType,  payload);
		postalcodeWithSpecialCharacters(methodName,  resourceType,  payload);
		postalCodeWithInvalidFormat2(methodName,  resourceType,  payload);
		postalCodeWithInvalidFormat3(methodName,  resourceType,  payload);
	}

	
	public void postalCodeExceedingMaxLength(String methodName, String resourceType, String payload) throws IOException {
		FileLoggers.info("*****Validating PostalCode Fields postalCodeExceedingMaxLength scenario*****");
		String fieldName = "postalCode";
		String fieldValue = "postalCodepostalCodewithexceedingmaxlengthinrequestformatforfieldvalidation";
		ValidationMessages message = ValidationMessages.valueOf("POSTALCODEEXCEEDINGMAXLENGTH");
		expectedErrorMessage = message.getValidationMessage(); 
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 400, expectedErrorMessage);
//		postalCodeWithInvalidFormat(methodName,  resource,  payload);
	}

	public void postalCodeWithInvalidFormat(String methodName, String resourceType, String payload) throws IOException {
		FileLoggers.info("*****Validating PostalCode Fields - postalCodeWithInvalidFormat scenario*****");
		String fieldName = "postalCode";
		String fieldValue = "AAA AAA";
		ValidationMessages message = ValidationMessages.valueOf("POSTALCODEWITHINVALIDFORMAT");
		expectedErrorMessage = message.getValidationMessage(); 
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 400, expectedErrorMessage);		
//		postalcodeWithSpecialCharacters(methodName,  resource,  payload);
	}
	
	public void postalcodeWithSpecialCharacters(String methodName, String resourceType, String payload) throws IOException {
		FileLoggers.info("*****Validating PostalCode Fields - postalcodeWithSpecialCharacters scenario*****");
		String fieldName = "postalCode";
		String fieldValue = "A2A @A1";
		ValidationMessages message = ValidationMessages.valueOf("POSTALCODEWITHSPECIALCHARACTER");
		expectedErrorMessage = message.getValidationMessage();
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 400, expectedErrorMessage);
//		postalCodeWithInvalidFormat2(methodName,  resource,  payload);
	}
	public void postalCodeWithInvalidFormat2(String methodName, String resourceType, String payload) throws IOException {
		FileLoggers.info("*****Validating PostalCode Fields - postalCodeWithInvalidFormat2 scenario*****");
		String fieldName = "postalCode";
		String fieldValue = "AA AAAA";
		ValidationMessages message = ValidationMessages.valueOf("POSTALCODEWITHINVALIDFORMAT2");
		expectedErrorMessage = message.getValidationMessage(); 
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 400, expectedErrorMessage);		
//		postalCodeWithInvalidFormat3(methodName,  resource,  payload);
	}
	public void postalCodeWithInvalidFormat3(String methodName, String resourceType, String payload) throws IOException {
		FileLoggers.info("*****Validating PostalCode Fields - postalCodeWithInvalidFormat3 scenario*****");
		String fieldName = "postalCode";
		String fieldValue = "AAA 111";
		ValidationMessages message = ValidationMessages.valueOf("POSTALCODEWITHINVALIDFORMAT3");
		expectedErrorMessage = message.getValidationMessage(); 
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 400, expectedErrorMessage);	
		FileLoggers.info("*****Validation PostalCode Fields Completed*****");	
	}
	
	








}
