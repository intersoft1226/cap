package fieldValidations;

import static org.junit.Assert.assertEquals;
import java.io.IOException;
import org.json.*;
import org.junit.Assert;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

import common.FileLoggers;
import common.ReplaceFieldValuePayload;
import common.RequestAPI;
import common.ResponseAPI;
import common.Utils;
import common.ValidationMessages;
//import org.junit.Test;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class Email extends Utils{
	RequestSpecification req;
	ReplaceFieldValuePayload pup = new ReplaceFieldValuePayload();
	String updatedPayload;
	Response response;
	RequestAPI api = new RequestAPI();
	ResponseAPI respapi = new ResponseAPI();
	int expectedStatusCode = 400;
	String requestId;
	String responseId;
	String templateId;
	String expectedErrorMessage = "Email Field format is invalid";
	String expectedMaxLengthErrorMessage = "Email Field should be max length 128";
	String responsePage;
	
	
	public void validatingAllEmailScenarios(String methodName, String resourceType, String payload) throws IOException {
		emailWithoutAtTheRate(methodName, resourceType, payload);
		emailWithoutDot(methodName, resourceType, payload);
		emailWithNoPrefixbeforeAtTheRate(methodName, resourceType, payload);
		emailWithEmailIdExceedingMaxlength(methodName, resourceType, payload);
		emailWithMultipleAtTheRateInEmail(methodName, resourceType, payload);
		emailWithSpecialCharacters(methodName, resourceType, payload);
		emailWithSpace(methodName, resourceType, payload);
		emailWithLeadingSpace(methodName, resourceType, payload);
		emailWithTrailingSpace(methodName, resourceType, payload);	

	}

	public void emailWithoutAtTheRate(String methodName, String resourceType, String payload) throws IOException {
		FileLoggers.info("****Validating Email Fields - emailWithoutAtTheRate******");
		String fieldName = "email";
		String fieldValue = "testemail.com";
		ValidationMessages message = ValidationMessages.valueOf("EMAILWITHOUTATTHERATE");
		expectedErrorMessage = message.getValidationMessage(); 
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 400, expectedErrorMessage );
//		emailWithoutDot(methodName, resource, payload);

	}

	public void emailWithoutDot(String methodName, String resourceType, String payload) throws IOException {
		FileLoggers.info("******Validating Email Fields - emailWithoutDot scenario******");
		String fieldName = "email";
		String fieldValue = "test@emailcom";
		ValidationMessages message = ValidationMessages.valueOf("EMAILWITHOUTDOT");
		expectedErrorMessage = message.getValidationMessage(); 
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 400, expectedErrorMessage );
//		emailWithNoPrefixbeforeAtTheRate(methodName, resource, payload);
	}

	public void emailWithNoPrefixbeforeAtTheRate(String methodName, String resourceType, String payload)
			throws IOException {
		FileLoggers.info("******Validating Email Fields - emailWithNoPrefixbeforeAtTheRate scenario******");
		String fieldName = "email";
		String fieldValue = "@email.com";
		ValidationMessages message = ValidationMessages.valueOf("EMAILWITHNOPREFIXBEFOREATTHERATE");
		expectedErrorMessage = message.getValidationMessage(); 
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 400, expectedErrorMessage );
//		emailWithEmailIdExceedingMaxlength(methodName, resource, payload);
	}

	public void emailWithEmailIdExceedingMaxlength(String methodName, String resourceType, String payload)
			throws IOException {
		FileLoggers.info("******Validating Email Fields - emailWithEmailIdExceedingMaxlength scenario******");
		String fieldName = "email";
		String fieldValue = "testmorethan128characterstestmorethan128characterstestmorethan128characterstestmorethan128characterstestmorethan128characters@email.com";
		ValidationMessages message = ValidationMessages.valueOf("EMAILWITHEMAILIDEXCEEDINGMAXLENGTH");
		expectedErrorMessage = message.getValidationMessage(); 
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 400, expectedMaxLengthErrorMessage );
//		emailWithMultipleAtTheRateInEmail(methodName, resource, payload);
	}

	public void emailWithMultipleAtTheRateInEmail(String methodName, String resourceType, String payload)
			throws IOException {
		FileLoggers.info("******Validating Email Fields - emailWithMultipleAtTheRateInEmail scenario******");
		String fieldName = "email";
		String fieldValue = "test@new@email.com";
		ValidationMessages message = ValidationMessages.valueOf("EMAILWITHMULTIPLEATTHERATEINEMAIL");
		expectedErrorMessage = message.getValidationMessage(); 
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 400, expectedErrorMessage );
//		emailWithSpecialCharacters(methodName, resource, payload);
	}

	public void emailWithSpecialCharacters(String methodName, String resourceType, String payload) throws IOException {
		FileLoggers.info("******Validating Email Fields - emailWithSpecialCharacters scenario******");
		String fieldName = "email";
		String fieldValue = "t@#}estnew@email.com";
		ValidationMessages message = ValidationMessages.valueOf("EMAILWITHSPECIALCHARACTERS");
		expectedErrorMessage = message.getValidationMessage(); 
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 400, expectedErrorMessage );
//		emailWithSpace(methodName, resource, payload);
	}

	public void emailWithQuotedString(String methodName, String resourceType, String payload) throws IOException {
		FileLoggers.info("******Validating Email Fields - emailWithQuotedString scenario******");
		String fieldName = "email";
		String fieldValue = "test'quote'new@email.com";
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 400, expectedErrorMessage );
	}

	public void emailWithSpace(String methodName, String resourceType, String payload) throws IOException {
		FileLoggers.info("******Validating Email Fields - emailWithSpace scenario******");
		String fieldName = "email";
		String fieldValue = "test spacenew@email.com";
		ValidationMessages message = ValidationMessages.valueOf("EMAILWITHSPACE");
		expectedErrorMessage = message.getValidationMessage(); 
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 400, expectedErrorMessage );
//		emailWithLeadingSpace(methodName, resource, payload);
	}

	public void emailWithLeadingSpace(String methodName, String resourceType, String payload) throws IOException {
		FileLoggers.info("******Validating Email Fields - emailWithLeadingSpace scenario******");
		String fieldName = "email";
		String fieldValue = "  spacenew@email.com";
		expectedErrorMessage="";
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 200, expectedErrorMessage );
//		emailWithTrailingSpace(methodName, resource, payload);
	}

	public void emailWithTrailingSpace(String methodName, String resourceType, String payload) throws IOException {
		FileLoggers.info("******Validating Email Fields - emailWithTrailingSpace scenario******");
		String fieldName = "email";
		String fieldValue = "trailspacenew@email.com    ";
		expectedErrorMessage="";
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 200, expectedErrorMessage );
		FileLoggers.info("Validation Email Fields Completed");
	}
}
