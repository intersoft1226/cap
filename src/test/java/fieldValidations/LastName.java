package fieldValidations;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.json.JSONObject;
import org.junit.Assert;

import common.FileLoggers;
import common.ReplaceFieldValuePayload;
import common.RequestAPI;
import common.ResponseAPI;
import common.Utils;
import common.ValidationMessages;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class LastName extends Utils{
	RequestSpecification req;
	ReplaceFieldValuePayload pup = new ReplaceFieldValuePayload();
	String updatedpayload;
	Response response;
	RequestAPI api = new RequestAPI();
	ResponseAPI respapi = new ResponseAPI();
	int expectedStatusCode = 400;
	String requestId;
	String responseId;
	String templateId;
	String expectedErrorMessage = "Email Field format is invalid";
//	String expectedErrorMessage = " Field value is invalid";
//	String expectedMaxLengthErrorMessage = "Email Field should be max length 128";
//	String expected_MaxLengthErrorMessage = " Field should be max length 50";
	String responsePage;

	public void validatingAllLastNameScenarios(String methodName, String resourceType, String payload) throws IOException {
		lastNameExceedingMaxLength(methodName,  resourceType,  payload);
		lastNameAcceptingAlphanumeric(methodName,  resourceType,  payload);
		lastNameAcceptingSpecialCharacters(methodName,  resourceType,  payload);
		
	}

	public void lastNameExceedingMaxLength(String methodName, String resourceType, String payload) throws IOException {
		FileLoggers.info("*****Validating LastName Fields lastNameExceedingMaxLength scenario*****");
		String fieldName = "lastName";
		String fieldValue = "lastNamewithexceedingmaxlengthinrequestformatforfieldvalidation";
		ValidationMessages message = ValidationMessages.valueOf("LASTNAMEEXCEEDINGMAXLENGTH");
		expectedErrorMessage = message.getValidationMessage(); 
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 400, expectedErrorMessage);
//		lastNameAcceptingAlphanumeric(methodName,  resource,  payload);
	}

	public void lastNameAcceptingAlphanumeric(String methodName, String resourceType, String payload) throws IOException {
		FileLoggers.info("*****Validating LastName Fields - lastNameAcceptingAlphanumeric scenario*****");
		String fieldName = "lastName";
		String fieldValue = "firstName12345";
		expectedErrorMessage = "";
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 200, expectedErrorMessage);		
//		lastNameAcceptingSpecialCharacters(methodName,  resource,  payload);
	}
	
	public void lastNameAcceptingSpecialCharacters(String methodName, String resourceType, String payload) throws IOException {
		FileLoggers.info("*****Validating LastName Fields - lastNameAcceptingSpecialCharacters scenario*****");
		String fieldName = "lastName";
		String fieldValue = "lastName!@#$%^&*(){}~!12345";
		expectedErrorMessage = "";
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 200, expectedErrorMessage);
		FileLoggers.info("*****Validation Email Fields Completed*****");
	}
	
	




}
