package fieldValidations;



import static org.junit.Assert.assertEquals;
import java.io.IOException;
import org.json.*;
import org.junit.Assert;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

import common.FileLoggers;
import common.ReplaceFieldValuePayload;
import common.RequestAPI;
import common.ResponseAPI;
import common.Utils;
//import org.junit.Test;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class UnPrintableCharacters extends Utils{
	String fieldName;
	RequestSpecification req;
	ReplaceFieldValuePayload pup = new ReplaceFieldValuePayload();
	String updatedPayload;
	Response response;
	RequestAPI api = new RequestAPI();
	ResponseAPI respapi = new ResponseAPI();
	int expectedStatusCode = 400;
	String requestId;
	String responseId;
	String templateId;	
//	String expectedMaxLengthErrorMessage = "Email Field should be max length 128";
	String responsePage;

	public void validateUnPrintableCharacter(String methodName, String resourceType, String payload, String fieldName) throws IOException {
		FileLoggers.info("Validating Un-Printable Characters");
		this.fieldName =fieldName;
		String fieldValue = "";
		String expectedErrorMessage = "The following field(s) - First Name - contain unprintable character(s)";
		customResponseValidation(payload, fieldName, fieldValue, resourceType, methodName, 400, expectedErrorMessage );
	}
	}