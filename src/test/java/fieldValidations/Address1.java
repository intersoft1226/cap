package fieldValidations;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.json.JSONObject;
import org.junit.Assert;

import common.FileLoggers;
import common.ReplaceFieldValuePayload;
import common.RequestAPI;
import common.ResponseAPI;
import common.Utils;
import common.ValidationMessages;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class Address1 extends Utils{

	RequestSpecification req;
	Response response;
	int expectedStatusCode = 400;
	String requestId;
	String responseId;
	String templateId;
	String responsePage;
	String expectedErrorMessage;
	ReplaceFieldValuePayload pup = new ReplaceFieldValuePayload();
	RequestAPI api = new RequestAPI();
	ResponseAPI respapi = new ResponseAPI();
	
	public void validatingAllAddress1Scenarios(String methodName, String resourceType, String payload) throws IOException {
		address1ExceedingMaxLength(methodName,  resourceType,  payload);
		address1AcceptingAlphanumeric(methodName,  resourceType,  payload);
		address1AcceptingSpecialCharacters(methodName,  resourceType,  payload);
	}

	public void address1ExceedingMaxLength(String methodName, String resourceType, String payload) throws IOException {
		FileLoggers.info("*****Validating Address1 Fields - address1ExceedingMaxLength scenario*****");
		String fieldName = "addr1";
		String fieldValue = "address1withexceedingmaxlengthinrequestformatforfieldvalidation";
		ValidationMessages message = ValidationMessages.valueOf("ADDRESS1EXCEEDINGMAXLENGTH");
		expectedErrorMessage = message.getValidationMessage(); 
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 400, expectedErrorMessage);
//		address1AcceptingAlphanumeric(methodName,  resource,  payload);
	}

	public void address1AcceptingAlphanumeric(String methodName, String resourceType, String payload) throws IOException {
		FileLoggers.info("*****Validating Address1 Fields - address1AcceptingAlphanumeric scenario*****");
		String fieldName = "addr1";
		String fieldValue = "address12345";
		expectedErrorMessage = "";
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 200, expectedErrorMessage);		
//		address1AcceptingSpecialCharacters(methodName,  resource,  payload);
	}
	
	public void address1AcceptingSpecialCharacters(String methodName, String resourceType, String payload) throws IOException {
		FileLoggers.info("*****Validating Address1 Fields - address1AcceptingSpecialCharacters scenario*****");
		String fieldName = "addr1";
		String fieldValue = "address!@#$%^&*(){}~!12345";
		expectedErrorMessage = "";
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 200, expectedErrorMessage);
		FileLoggers.info("Validation Address1 Fields Completed");
	}
	
	

}
