package fieldValidations;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.json.JSONArray;
import org.json.JSONObject;

import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.hibernate4.Hibernate4Module;

import common.CommonLib;
import common.DataBaseConnection;
import common.FileLoggers;
import common.ReplaceFieldValuePayload;
import common.RequestAPI;
import common.ResponseAPI;
import common.Utils;
import common.ValidationMessages;
import gherkin.deps.com.google.gson.Gson;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import pojo.EmailAcquisition;

public class BabyField extends Utils {
	RequestSpecification req;
	String updatedPayload;
	Response response;
	RequestAPI api = new RequestAPI();
	ResponseAPI respApi = new ResponseAPI();
	int expectedStatusCode = 400;
	String requestId;
	String responseId;
	String templateId;
	String expectedErrorMessage = "Baby Field format is invalid";
	String expected_ErrorMessage = " Field value is invalid";
	String expectedMaxLengthErrorMessage = "Baby Field should be max length 128";
	String responsePage;
	
	
	public void validatingAllBabyFieldScenarios(String methodName, String resourceType, String payload) throws IOException, SQLException {
		babyFieldExceedingMaxValue(methodName, resourceType, payload);
		babyFieldWithLessThanMinValue(methodName, resourceType, payload);
		babyFieldAcceptingZero(methodName, resourceType, payload);
		babyFieldCountLessThanChildrenCount(methodName, resourceType, payload);
	}

	public void babyFieldExceedingMaxValue(String methodName, String resourceType, String payload) throws IOException, SQLException {
		FileLoggers.info("*********babyFieldExceedingMaxValue*************");
		String fieldName = "babyFields";
		String fieldValue = "4";		
		ValidationMessages message = ValidationMessages.valueOf("BABYFIELDEXCEEDINGMAXVALUE");
		expectedErrorMessage = message.getValidationMessage(); 
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 400, expectedErrorMessage);
//		babyFieldWithLessThanMinValue(methodName, resourceType, payload);
	}

	public void babyFieldWithLessThanMinValue(String methodName, String resourceType, String payload) throws IOException, SQLException {
		FileLoggers.info("*********babyFieldWithLessThanMinValue*************");
		String fieldName = "babyFields";
		String fieldValue = "-1";
		ValidationMessages message = ValidationMessages.valueOf("BABYFIELDWITHLESSTHANMINVALUE");
		expectedErrorMessage = message.getValidationMessage(); 
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 400, expectedErrorMessage);
//		babyFieldAcceptingZero(methodName, resource, payload);
	}
	
	
	public void babyFieldAcceptingZero(String methodName, String resourceType, String payload) throws IOException, SQLException {
		FileLoggers.info("*********babyFieldAcceptingZero*************");
		String fieldName = "babyFields";
		String fieldValue = "0";
		expectedErrorMessage = "";
		JSONObject jsobj = new JSONObject(payload);
		templateId = jsobj.getString("id");
		String query = "SELECT required FROM `form_field` ff INNER JOIN Template t ON ff.templatekey = t.templatekey WHERE ff.FieldId ="+ "'" + fieldName + "'" +"and t.TemplateId ="+ "'"+templateId+"'";
		FileLoggers.info("SQL Query for Checking ISRequired BabyField field -> "+query);
//		DataBaseConnection db = new DataBaseConnection();
		ResultSet rs2 = CommonLib.db.getSQLResults(query);
		if(rs2.next()) {
		String isRequired= rs2.getString(1);
		System.out.println(isRequired);
		FileLoggers.info("****************ISrequired in BabyFields***  "+isRequired.equals("0"));
		if(isRequired.equals("0")) {
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 200, expectedErrorMessage);
		}
//		babyFieldCountLessThanChildrenCount(methodName, resource, payload);
		}
	}
	public void babyFieldCountLessThanChildrenCount(String methodName, String resourceType, String payload) throws IOException {
		FileLoggers.info("*********babyFieldCountLessThanChildrenCount*************");
		String fieldName = "babyFields";
		String fieldValue = "3";
		//Removing Children data
		JSONObject jsobj = new JSONObject(payload);
		Object BabyFieldCount =   jsobj.get(fieldName);  //jsobj.getString("babyFields");		
		if(Integer.parseInt(BabyFieldCount.toString())>1) {
		JSONObject updatedPayload = new JSONObject(payload);
		JSONArray array = updatedPayload.getJSONArray("children");	
		updatedPayload = new JSONObject(payload);
		array = updatedPayload.getJSONArray("children");
		System.out.println(array);
		JSONObject customObject = (JSONObject) array.get(1);
		customObject.remove("firstNameUnknown");		
		customObject.remove("firstName");
		customObject.remove("hasDateOfBirth");
		customObject.remove("expectedBirthYear");
		customObject.remove("expectedBirthMonth");
		customObject.remove("dateOfBirth");
		customObject.remove("gender");
		System.out.println(updatedPayload);
		String payloadAfterRemovingObject = updatedPayload.toString();			
		Gson gson = new Gson();
		gson.fromJson(payloadAfterRemovingObject, EmailAcquisition.class);
		EmailAcquisition strToJson = gson.fromJson(payloadAfterRemovingObject, EmailAcquisition.class);
		ObjectMapper obj = new ObjectMapper();
		obj.setSerializationInclusion(Include.NON_NULL);
		obj.setSerializationInclusion(Include.NON_EMPTY);
		obj.setVisibility(PropertyAccessor.FIELD, Visibility.ANY);
		obj.registerModule(new Hibernate4Module());
		payload = obj.writerWithDefaultPrettyPrinter().writeValueAsString(strToJson);
		FileLoggers.info(payload);
		System.out.println("Updated Json is " + payload);
		String load = payload;
		String payloadUpdated= load.replaceAll("\\{ }\\,", " ");		
//		expectedErrorMessage = "Child information must be supplied";
		ValidationMessages message = ValidationMessages.valueOf("BABYFIELDCOUNTLESSTHANCHILDRENCOUNT");
		expectedErrorMessage = message.getValidationMessage(); 
		responseValidation(payloadUpdated, fieldName, fieldValue, resourceType, methodName, 400, expectedErrorMessage);	
	
	}
	}
	// To be included in functional test cases
	
}
