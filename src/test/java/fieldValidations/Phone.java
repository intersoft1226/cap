package fieldValidations;

import static org.junit.Assert.assertEquals;
import java.io.IOException;
import org.json.JSONObject;
import org.junit.Assert;

import common.FileLoggers;
import common.ReplaceFieldValuePayload;
import common.RequestAPI;
import common.ResponseAPI;
import common.Utils;
import common.ValidationMessages;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class Phone extends Utils{
	RequestSpecification req;
	ReplaceFieldValuePayload pup = new ReplaceFieldValuePayload();
	String updatedpayload;
	Response response;
	RequestAPI api = new RequestAPI();
	ResponseAPI respapi = new ResponseAPI();
	int expectedStatusCode = 400;
	String requestId;
	String responseId;
	String templateId;
	String expectedErrorMessage = "Email Field format is invalid";
//	String expectedErrorMessage = " Field value is invalid";
//	String expectedMaxLengthErrorMessage = "Email Field should be max length 128";
//	String expected_MaxLengthErrorMessage = " Field should be max length 50";
	String responsePage;
	
	public void validatingAllPhoneScenarios(String methodName, String resourceType, String payload) throws IOException {
		phoneExceedingMaxLength(methodName,  resourceType,  payload);
		phoneWithLessThanMinLength(methodName,  resourceType,  payload);
		phoneWithTwoHypensInvalidFormat(methodName,  resourceType,  payload);
		phoneWithAlphaNumeric(methodName,  resourceType,  payload);
		phoneWith12Digit(methodName,  resourceType,  payload);
	}

	
	public void phoneExceedingMaxLength(String methodName, String resourceType, String payload) throws IOException {
		FileLoggers.info("*****Validating Phone Field phoneExceedingMaxLength scenario*****");
		String fieldName = "phone";
		String fieldValue = "1234567890123456789011";
		ValidationMessages message = ValidationMessages.valueOf("PHONEEXCEEDINGMAXLENGTH");
		expectedErrorMessage = message.getValidationMessage(); 
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 400, expectedErrorMessage);
//		phoneWithLessThanMinLength(methodName,  resource,  payload);
	}
	
	public void phoneWithLessThanMinLength(String methodName, String resourceType, String payload) throws IOException {
		FileLoggers.info("*****Validating Phone Fields - phoneWithLessThanMinLength scenario*****");
		String fieldName = "phone";
		String fieldValue = "1234";
		ValidationMessages message = ValidationMessages.valueOf("PHONEWITHLESSTHANMINLENGTH");
		expectedErrorMessage = message.getValidationMessage(); 
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 400, expectedErrorMessage);
//		phoneWithTwoHypensInvalidFormat(methodName,  resource,  payload);
	}

	public void phoneWithTwoHypensInvalidFormat(String methodName, String resourceType, String payload) throws IOException {
		FileLoggers.info("*****Validating Phone Fields - phoneWithTwoHypensInvalidFormat scenario*****");
		String fieldName = "phone";
		String fieldValue = "12-1123-1112";
		ValidationMessages message = ValidationMessages.valueOf("PHONEWITHTWOHYPENSINVALIDFORMAT");
		expectedErrorMessage = message.getValidationMessage(); 
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 400, expectedErrorMessage);		
//		phoneWithAlphaNumeric(methodName,  resource,  payload);
	}
	
	public void phoneWithSingleHypensInvalidFormat(String methodName, String resourceType, String payload) throws IOException {
		FileLoggers.info("*****Validating Phone Fields - phoneWithSingleHypensInvalidFormat scenario*****");
		String fieldName = "phone";
		String fieldValue = "123-1121212";
		ValidationMessages message = ValidationMessages.valueOf("PHONEWITHSINGLEHYPENSINVALIDFORMAT");
		expectedErrorMessage = message.getValidationMessage(); 
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 400, expectedErrorMessage);
//		phoneWithAlphaNumeric(methodName,  resource,  payload);
	}
	public void phoneWithAlphaNumeric(String methodName, String resourceType, String payload) throws IOException {
		FileLoggers.info("*****Validating Phone Fields - phoneWithAlphaNumeric scenario*****");
		String fieldName = "phone";
		String fieldValue = "1234a12345";
		ValidationMessages message = ValidationMessages.valueOf("PHONEWITHALPHANUMERIC");
		expectedErrorMessage = message.getValidationMessage(); 
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 400, expectedErrorMessage);		
//		phoneWith12Digit(methodName,  resource,  payload);
	}
	public void phoneWith12Digit(String methodName, String resourceType, String payload) throws IOException {
		FileLoggers.info("*****Validating Phone Fields - phoneWith12Digit scenario*****");
		String fieldName = "phone";
		String fieldValue = "112345678912";
		ValidationMessages message = ValidationMessages.valueOf("PHONEWITH12DIGIT");
		expectedErrorMessage = message.getValidationMessage(); 
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 400, expectedErrorMessage);	
		FileLoggers.info("Validation Phone Fields Completed");		
	}
	
	











}
