package pojo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.cucumber.datatable.dependency.com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.cucumber.datatable.dependency.com.fasterxml.jackson.annotation.JsonIgnoreType;

//@JsonIgnoreProperties(value={"hibernateLazyInitializer","handler","fieldHandler"})
@JsonAutoDetect(fieldVisibility = Visibility.ANY)
@JsonInclude(Include.NON_NULL)
public class EmailAcquisition {
	private String id;
	private String email;
	private String zipCode;
	private String title;
	private String addr1;
	private String addr2;
	private String city;
	private String state;
	private String firstName;
	private String lastName;
	private String province;
	private String phone;
	private int babyFields;
	private List<Children> children;
	private List<Custom> custom;
	private String emailconsent;
	private String emailconsent2;	
	private String mocode;
	private String dateQuestion;
	private String dateQuestionValue;
	private String guardian;
	private String mobilePhone;
	private String postalCode;
	private String CmpDeploymentStrategy;
	private String CmpName;
	private String CmpVariationID;
	private String CmpConcept;
	private String CmpFormat;
	private String CmpDevice;
	private String sourceID;
	
	public String getSourceID() {
		return sourceID;
	}
	public void setSourceID(String sourceID) {
		this.sourceID = sourceID;
	}
	@JsonProperty(value="postalCode")
	public String getPostalCode() {
		return postalCode;
	}
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	@JsonProperty(value="id")
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	@JsonProperty(value="email")
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	@JsonProperty(value="zipCode")
	public String getZipcode() {
		return zipCode;
	}
	public void setZipcode(String zipCode) {
		this.zipCode = zipCode;
	}
	@JsonProperty(value="title")
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	@JsonProperty(value="addr1")
	public String getAddr1() {
		return addr1;
	}
	public void setAddr1(String addr1) {
		this.addr1 = addr1;
	}
	@JsonProperty(value="addr2")
	public String getAddr2() {
		return addr2;
	}
	public void setAddr2(String addr2) {
		this.addr2 = addr2;
	}
	@JsonProperty(value="city")
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	@JsonProperty(value="state")
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	@JsonProperty(value="firstName")
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	@JsonProperty(value="lastName")
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	@JsonProperty(value="province")
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	@JsonProperty(value="phone")
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	@JsonProperty(value="babyFields")
	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	public int getBabyFields() {
		return babyFields;
	}
	public void setBabyFields(int fieldValue) {
		this.babyFields = fieldValue;
	}
	@JsonProperty(value="children")
	public List<Children> getChildren() {
		return children;
	}
	public void setChildren(List<Children> children) {
		this.children = children;
	}
	@JsonProperty(value="custom")
	public List<Custom> getCustom() {
		return custom;
	}
	public void setCustom(List<Custom> custom) {
		this.custom = custom;
	}
	@JsonProperty(value="emailconsent")
	public String getEmailconsent() {
		return emailconsent;
	}
	public void setEmailconsent(String emailconsent) {
		this.emailconsent = emailconsent;
	}
	@JsonProperty(value="mocode")
	public String getMocode() {
		return mocode;
	}
	public void setMocode(String fieldValue) {
		this.mocode = fieldValue;
	}
	@JsonProperty(value="dateQuestion")
	public String getDateQuestion() {
		return dateQuestion;
	}
	public void setDateQuestion(String dateQuestion) {
		this.dateQuestion = dateQuestion;
	}
	@JsonProperty(value="dateQuestionValue")
	public String getDateQuestionValue() {
		return dateQuestionValue;
	}
	public void setDateQuestionValue(String dateQuestionValue) {
		this.dateQuestionValue = dateQuestionValue;
	}
	@JsonProperty(value="guardian")
	public String getGuardian() {
		return guardian;
	}
	public void setGuardian(String guardian) {
		this.guardian = guardian;
	}
	@JsonProperty(value="mobilePhone")
	public String getMobilePhone() {
		return mobilePhone;
	}
	@JsonProperty(value="CmpDeploymentStrategy")
	public String getCmpDeploymentStrategy() {
		return CmpDeploymentStrategy;
	}
	public void setCmpDeploymentStrategy(String cmpDeploymentStrategy) {
		CmpDeploymentStrategy = cmpDeploymentStrategy;
	}
	@JsonProperty(value="CmpName")
	public String getCmpName() {
		return CmpName;
	}
	public void setCmpName(String cmpName) {
		CmpName = cmpName;
	}
	@JsonProperty(value="CmpVariationID")
	public String getCmpVariationID() {
		return CmpVariationID;
	}
	public void setCmpVariationID(String cmpVariationID) {
		CmpVariationID = cmpVariationID;
	}
	@JsonProperty(value="CmpConcept")
	public String getCmpConcept() {
		return CmpConcept;
	}
	public void setCmpConcept(String cmpConcept) {
		CmpConcept = cmpConcept;
	}
	@JsonProperty(value="CmpFormat")
	public String getCmpFormat() {
		return CmpFormat;
	}
	public void setCmpFormat(String cmpFormat) {
		CmpFormat = cmpFormat;
	}
	@JsonProperty(value="CmpDevice")
	public String getCmpDevice() {
		return CmpDevice;
	}
	public void setCmpDevice(String cmpDevice) {
		CmpDevice = cmpDevice;
	}
	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}
	public String getEmailconsent2() {
		return emailconsent2;
	}
	public void setEmailconsent2(String emailconsent2) {
		this.emailconsent2 = emailconsent2;
	}
	
	
	
	

}
