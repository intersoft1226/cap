# Data Collection Field validations covered for fields - Children[1]
Feature: Validating Sign Up services for Field "BabyField-Children" of template type Data Collection. 


@smokeTest
Scenario Outline: DC082 - Data Collection Template Test- Validate the response when blank FIRSTNAMEUNKNOWN of First children is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex  | code |SheetPath											|SheetName					  				|Message					          				|
	|83        | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     |CHILDRENFIRSTNAMEUNKNOWNBLANK_1    |
	


@smokeTest
Scenario Outline: DC083 - Data Collection Template Test- Validate the response when blank FIRSTNAME of First children is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message					   |
	|84       | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     |CHILDRENFIRSTNAMEBLANK_1    |	
	


@smokeTest
Scenario Outline: DC084 - Data Collection Template Test- Validate the response when blank HASDOB of First children is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message				    |
	|85       | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     |CHILDRENHASDOBBLANK_1    |	
	

@smokeTest
Scenario Outline: DC085 - Data Collection Template Test- Validate the response when blank DATE OF BIRTH of First children is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message				         |
	|86       | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     |CHILDRENDATEOFBIRTHBLANK_1    |	
	
	

@smokeTest
Scenario Outline: DC086 - Data Collection Template Test- Validate the response when DATEOF BIRTH of First children is  in FUTURE is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message				              |
	|87       | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     |CHILDRENWITHFUTUREDATEOFBIRTH_1    |	



@smokeTest
Scenario Outline: DC087 - Data Collection Template Test- Validate the response when  EXPECTED BIRTH MONTH is BLANK of First children is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message				                |
	|88       | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     |CHILDRENEXPECTEDBIRTHMONTHBLANK_1    |	
	
	

@smokeTest
Scenario Outline: DC088 - Data Collection Template Test- Validate the response when ExpectedBirthYear = current year AND ExpectedMonth<currentMonth of First children is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message				                   |
	|89       | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     |CHILDRENWITHPASTEXPECTEDBIRTHMONTH_1    |	
	

		

@smokeTest
Scenario Outline: DC089 - Data Collection Template Test- Validate the response when blank CHILDREN EXPECTED BIRTH YEAR of First children is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message				               |
	|90       | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     |CHILDRENEXPECTEDBIRTHYEARBLANK_1    |
	

@smokeTest
Scenario Outline: DC090 - Data Collection Template Test- Validate the response when Future EXPECTED BIRTH YEAR of First children is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message				                    |
	|91       | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     |CHILDRENWITHFUTUREEXPECTEDBIRTHYEAR_1    |	
	
# *****This is not implemented yet since cant able to set Null values
#@smokeTest
#Scenario Outline: DC091 - Data Collection Template Test- Validate the response when CHILDREN WITH GENDER NULL of First children is sent in request.
#	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
#	When  When user hit "post" requests with "null" "gender" field
#	Then  User should received response status <code>
#	And   User validated the response <Message> received
#	
#	Examples:	
#	|Rowindex | code |SheetPath							|SheetName					  |Message				       |
#	|92       | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     |CHILDRENWITHGENDERNULL_1    |			
	
	

@smokeTest
Scenario Outline: DC092 - Data Collection Template Test- Validate the response when Invalid Gender Value of First children is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath									|SheetName					  				|Message				               			 |
	|93       | 400  |SheetPath_DataCollection  |SheetName_DataCollection     |CHILDRENWITHINVALIDGENDERVALUE_1    |	
	
	

@smokeTest
Scenario Outline: DC093 - Data Collection Template Test- Validate the response when ExpectedBirthMonth < 0 of First children is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message				                      |
	|94       | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     |CHILDRENWITHINVALIDEXPECTEDBIRTHMONTH_1    |	
	

@smokeTest
Scenario Outline: DC094 - Data Collection Template Test- Validate the response when ExpectedBirthMonth > 12 of First children is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message				                      |
	|95       | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     |CHILDRENWITHINVALIDEXPECTEDBIRTHMONTH_1    |		
	

@smokeTest
Scenario Outline: DC095 - Data Collection Template Test- Validate the response when ExpectedBirthYear < current year of First children is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message				                   |
	|96       | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     |CHILDRENWITHPASTEXPECTEDBIRTHMONTH_1    |	
	
	

@smokeTest
Scenario Outline: DC096 - Data Collection Template Test- Validate the response when blank FIRSTNAMEUNKNOWN of Second children is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message					          |
	|97        | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     |CHILDRENFIRSTNAMEUNKNOWNBLANK_2    |
	

	
@smokeTest
Scenario Outline: DC097 - Data Collection Template Test- Validate the response when blank FIRSTNAME of Second children is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message					   |
	|98       | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     |CHILDRENFIRSTNAMEBLANK_2    |	
	


@smokeTest
Scenario Outline: DC098 - Data Collection Template Test- Validate the response when blank HASDOB of Second children is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message				    |
	|99       | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     |CHILDRENHASDOBBLANK_2    |	
	

@smokeTest
Scenario Outline: DC099 - Data Collection Template Test- Validate the response when blank DATE OF BIRTH of Second children is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message				         |
	|100       | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     |CHILDRENDATEOFBIRTHBLANK_2    |	
	
	

@smokeTest
Scenario Outline: DC100 - Data Collection Template Test- Validate the response when DATEOF BIRTH of Second children is  in FUTURE is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message				              |
	|101       | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     |CHILDRENWITHFUTUREDATEOFBIRTH_2    |	



@smokeTest
Scenario Outline: DC101 - Data Collection Template Test- Validate the response when  EXPECTED BIRTH MONTH is BLANK of Second children is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message				                |
	|102       | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     |CHILDRENEXPECTEDBIRTHMONTHBLANK_2    |	
	
	

@smokeTest
Scenario Outline: DC102 - Data Collection Template Test- Validate the response when ExpectedBirthYear = current year AND ExpectedMonth<currentMonth of Second children is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message				                   |
	|103       | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     |CHILDRENWITHPASTEXPECTEDBIRTHMONTH_2    |	
	

		

@smokeTest
Scenario Outline: DC103 - Data Collection Template Test- Validate the response when blank CHILDREN EXPECTED BIRTH YEAR of Second children is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message				               |
	|104       | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     |CHILDRENEXPECTEDBIRTHYEARBLANK_2    |
	

@smokeTest
Scenario Outline: DC104 - Data Collection Template Test- Validate the response when Future EXPECTED BIRTH YEAR of Second children is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message				                    |
	|105       | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     |CHILDRENWITHFUTUREEXPECTEDBIRTHYEAR_2    |	
	
# *****This is not implemented yet since cant able to set Null values
#@smokeTest
#Scenario Outline: DC105 - Data Collection Template Test- Validate the response when CHILDREN WITH GENDER NULL of Second children is sent in request.
#	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
#	When  When user hit "post" requests with "null" "gender" field
#	Then  User should received response status <code>
#	And   User validated the response <Message> received
#	
#	Examples:	
#	|Rowindex | code |SheetPath							|SheetName					  |Message				       |
#	|106       | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     |CHILDRENWITHGENDERNULL_2    |			
	
	

@smokeTest
Scenario Outline: DC106 - Data Collection Template Test- Validate the response when Invalid Gender Value of Second children is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath											|SheetName					  				|Message				              			 |
	|107      | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     |CHILDRENWITHINVALIDGENDERVALUE_2    |	
	
	

@smokeTest
Scenario Outline: DC107 - Data Collection Template Test- Validate the response when ExpectedBirthMonth < 0 of Second children is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message				                      |
	|108       | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     |CHILDRENWITHINVALIDEXPECTEDBIRTHMONTH_2    |	
	

@smokeTest
Scenario Outline: DC108 - Data Collection Template Test- Validate the response when ExpectedBirthMonth > 12 of Second children is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message				                      |
	|109       | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     |CHILDRENWITHINVALIDEXPECTEDBIRTHMONTH_2    |		
	

@smokeTest
Scenario Outline: DC109 - Data Collection Template Test- Validate the response when ExpectedBirthYear < current year of Second children is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message				                   |
	|110       | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     |CHILDRENWITHPASTEXPECTEDBIRTHMONTH_2    |	
	
		
	
		
	
	