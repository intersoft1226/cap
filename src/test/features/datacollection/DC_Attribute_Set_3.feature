# Data Collection Field validations covered for Custom fields
Feature: Validating Sign Up services for Field "Custom fields" of template type Data Collection. 

@checkTest12
@smokeTest
Scenario Outline: DC110 - Data Collection Template Test- Validate the response when blank custom id of CheckBoxtype Custom field data is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  user hit "post" requests with "blank" "customFieldId" field
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex 	| code		 |SheetPath												|SheetName					  				|Message					     |
	|110        | 400  		 |SheetPath_DataCollection   		  |SheetName_DataCollection     |CUSTOMBLANKFIELDID    |
	

@smokeTest
Scenario Outline: DC111 - Data Collection Template Test- Validate the response when blank custom value of CheckBoxtype Custom field data is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  user hit "post" requests with "blank" "customFieldValue" field
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex 	| code		 |SheetPath												|SheetName					  				|Message					        |
	|111        | 400  		 |SheetPath_DataCollection   		  |SheetName_DataCollection     |CUSTOMBLANKFIELDVALUE    |
	


@smokeTest
Scenario Outline: DC112 - Data Collection Template Test- Validate the response when Invalid custom value of CheckBox type Custom field is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  user hit "post" requests with "invalid" "invalidCustomFieldValue" field
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex 	| code		 |SheetPath												|SheetName					  				|Message			   		        |
	|112        | 400  		 |SheetPath_DataCollection   		  |SheetName_DataCollection     |CUSTOMINVALIDFIELDVALUE    |
	
	

@smokeTest
Scenario Outline: DC113 - Data Collection Template Test- Validate the response when Custom Field with multiple valid values for Checkbox type is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath											|SheetName									  |Message    |
	|113      | 200  |SheetPath_DataCollection   		|SheetName_DataCollection     | success   |		
	
	
@smokeTest
Scenario Outline: DC114 - Data Collection Template Test- Validate the response when Non Mandatory Blank Custom CheckBox type Field Id is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  user hit "post" requests with "blank" "customFieldId" field
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath											|SheetName									  |Message                      |
	|114      | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     | CUSTOMNONMANDATORYBLANKID   |	
	
	

@smokeTest
Scenario Outline: DC115 - Data Collection Template Test- Validate the response when Non Mandatory Invalid Custom Field Value is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  user hit "post" requests with "invalid" "invalidCustomFieldValue" field
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath											|SheetName									  |Message                      |
	|115      | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     | CUSTOMINVALIDFIELDVALUE     |		
		
			


@smokeTest
Scenario Outline: DC116 - Data Collection Template Test- Validate the response when Non Mandatory Custom Field with multiple valid values is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath											|SheetName									  |Message    |
	|116      | 200  |SheetPath_DataCollection   		|SheetName_DataCollection     | success   |		
	

	
@smokeTest
Scenario Outline: DC117 - Data Collection Template Test- Validate the response when blank custom id of Radio field type Custom field data is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  user hit "post" requests with "blank" "customFieldId" field
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex 	| code		 |SheetPath												|SheetName					  				|Message					     |
	|117        | 400  		 |SheetPath_DataCollection   		  |SheetName_DataCollection     |CUSTOMBLANKFIELDID    |
	
	
	
@smokeTest
Scenario Outline: DC118 - Data Collection Template Test- Validate the response when blank custom value of Radio type Custom field data is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  user hit "post" requests with "blank" "customFieldValue" field
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex 	| code		 |SheetPath												|SheetName					  				|Message					        |
	|118        | 400  		 |SheetPath_DataCollection   		  |SheetName_DataCollection     |CUSTOMBLANKFIELDVALUE    |
	


@smokeTest
Scenario Outline: DC119 - Data Collection Template Test- Validate the response when Invalid custom value of Radio type Custom field is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  user hit "post" requests with "invalid" "invalidCustomFieldValue" field
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex 	| code		 |SheetPath												|SheetName					  				|Message			   		        |
	|119        | 400  		 |SheetPath_DataCollection   		  |SheetName_DataCollection     |CUSTOMINVALIDFIELDVALUE    |
	
	

@smokeTest
Scenario Outline: DC120 - Data Collection Template Test- Validate the response when Custom Field with multiple valid values for Radio Field is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  user hit "post" requests with "multiple" "validCustomFieldValue" field
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath											|SheetName									  |Message                    |
	|120      | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     | CUSTOMINVALIDFIELDVALUE   |		
	
	
@smokeTest
Scenario Outline: DC121 - Data Collection Template Test- Validate the response when Non Mandatory Blank Custom Radio Field Id is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  user hit "post" requests with "blank" "customFieldId" field
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath											|SheetName									  |Message                      |
	|121      | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     | CUSTOMNONMANDATORYBLANKID   |	
	
	

@smokeTest
Scenario Outline: DC122 - Data Collection Template Test- Validate the response when Non Mandatory Invalid Radio type Custom Field Value is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  user hit "post" requests with "invalid" "invalidCustomFieldValue" field
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath											|SheetName									  |Message                      |
	|122      | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     | CUSTOMINVALIDFIELDVALUE     |		
	
	

@smokeTest
Scenario Outline: DC123 - Data Collection Template Test- Validate the response when Non Mandatory Custom Radio Field with multiple valid values is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  user hit "post" requests with "multiple" "validCustomFieldValue" field
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath											|SheetName									  |Message                    |
	|123      | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     | CUSTOMINVALIDFIELDVALUE   |		
	
	
	

@smokeTest
Scenario Outline: DC124 - Data Collection Template Test- Validate the response when blank custom id of DropDown field type Custom field data is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  user hit "post" requests with "blank" "customFieldId" field
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex 	| code		 |SheetPath												|SheetName					  				|Message					     |
	|124        | 400  		 |SheetPath_DataCollection   		  |SheetName_DataCollection     |CUSTOMBLANKFIELDID    |
	
	
	
@smokeTest
Scenario Outline: DC125 - Data Collection Template Test- Validate the response when blank custom value of DropDown type Custom field data is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  user hit "post" requests with "blank" "customFieldValue" field
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex 	| code		 |SheetPath												|SheetName					  				|Message					        |
	|125        | 400  		 |SheetPath_DataCollection   		  |SheetName_DataCollection     |CUSTOMBLANKFIELDVALUE    |
	

	
@smokeTest
Scenario Outline: DC126 - Data Collection Template Test- Validate the response when Invalid custom value of DropDown type Custom field is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  user hit "post" requests with "invalid" "invalidCustomFieldValue" field
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex 	| code		 |SheetPath												|SheetName					  				|Message			   		        |
	|126        | 400  		 |SheetPath_DataCollection   		  |SheetName_DataCollection     |CUSTOMINVALIDFIELDVALUE    |
	
	

@smokeTest
Scenario Outline: DC127 - Data Collection Template Test- Validate the response when Custom Field with multiple valid values for DropDown Field is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  user hit "post" requests with "multiple" "validCustomFieldValue" field
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath											|SheetName									  |Message                    |
	|127      | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     | CUSTOMINVALIDFIELDVALUE   |		
	

@smokeTest
Scenario Outline: DC128 - Data Collection Template Test- Validate the response when Non Mandatory Blank Custom DropDown Field Id is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  user hit "post" requests with "blank" "customFieldId" field
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath											|SheetName									  |Message                      |
	|128      | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     | CUSTOMNONMANDATORYBLANKID   |	
	
	

@smokeTest
Scenario Outline: DC129 - Data Collection Template Test- Validate the response when Non Mandatory Invalid DropDown type Custom Field Value is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  user hit "post" requests with "invalid" "invalidCustomFieldValue" field
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath											|SheetName									  |Message                      |
	|129      | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     | CUSTOMINVALIDFIELDVALUE     |		
	
	

@smokeTest
Scenario Outline: DC130 - Data Collection Template Test- Validate the response when Non Mandatory Custom DropDown Field with multiple valid values is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  user hit "post" requests with "multiple" "validCustomFieldValue" field
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath											|SheetName									  |Message                    |
	|130      | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     | CUSTOMINVALIDFIELDVALUE   |		
		