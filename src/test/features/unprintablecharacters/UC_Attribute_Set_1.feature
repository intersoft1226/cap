@unprintable_characters_field_validation_suite
Feature: Validating Sign Up services for all UN-Printable Characters

@smokeTest
Scenario Outline: UnPrintableCharactersCheck- Validate Sign Up services for all Un-Printable Characters
	Given User want to hit Sign up service for template <index> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should receive response status <code> and Field validation message for "FirstName" field
	
	Examples:	
	|index|code|SheetPath|SheetName	|
	|2 |400|SheetPath_UnPrintableCharacters|SheetName_UnPrintableCharacters|
	|3 |400|SheetPath_UnPrintableCharacters|SheetName_UnPrintableCharacters|
	|4 |400|SheetPath_UnPrintableCharacters|SheetName_UnPrintableCharacters|
	|5 |400|SheetPath_UnPrintableCharacters|SheetName_UnPrintableCharacters|
  |6 |400|SheetPath_UnPrintableCharacters|SheetName_UnPrintableCharacters|
  |7 |400|SheetPath_UnPrintableCharacters|SheetName_UnPrintableCharacters|
  |8 |400|SheetPath_UnPrintableCharacters|SheetName_UnPrintableCharacters|
	|9 |400|SheetPath_UnPrintableCharacters|SheetName_UnPrintableCharacters|
	|10|400|SheetPath_UnPrintableCharacters|SheetName_UnPrintableCharacters|
	|11|400|SheetPath_UnPrintableCharacters|SheetName_UnPrintableCharacters|
	|12|400|SheetPath_UnPrintableCharacters|SheetName_UnPrintableCharacters|
	|13|400|SheetPath_UnPrintableCharacters|SheetName_UnPrintableCharacters|
	|14|400|SheetPath_UnPrintableCharacters|SheetName_UnPrintableCharacters|
	|15|400|SheetPath_UnPrintableCharacters|SheetName_UnPrintableCharacters|
	|16|400|SheetPath_UnPrintableCharacters|SheetName_UnPrintableCharacters|
	|17|400|SheetPath_UnPrintableCharacters|SheetName_UnPrintableCharacters|
	|18|400|SheetPath_UnPrintableCharacters|SheetName_UnPrintableCharacters|
	|19|400|SheetPath_UnPrintableCharacters|SheetName_UnPrintableCharacters|
	|20|400|SheetPath_UnPrintableCharacters|SheetName_UnPrintableCharacters|
	|21|400|SheetPath_UnPrintableCharacters|SheetName_UnPrintableCharacters|
	|22|400|SheetPath_UnPrintableCharacters|SheetName_UnPrintableCharacters|
	|23|400|SheetPath_UnPrintableCharacters|SheetName_UnPrintableCharacters|
	|24|400|SheetPath_UnPrintableCharacters|SheetName_UnPrintableCharacters|
	|25|400|SheetPath_UnPrintableCharacters|SheetName_UnPrintableCharacters|
	|26|400|SheetPath_UnPrintableCharacters|SheetName_UnPrintableCharacters|
	|27|400|SheetPath_UnPrintableCharacters|SheetName_UnPrintableCharacters|
	|28|400|SheetPath_UnPrintableCharacters|SheetName_UnPrintableCharacters|
	|29|400|SheetPath_UnPrintableCharacters|SheetName_UnPrintableCharacters|
	|30|400|SheetPath_UnPrintableCharacters|SheetName_UnPrintableCharacters|
	|31|400|SheetPath_UnPrintableCharacters|SheetName_UnPrintableCharacters|
	|32|400|SheetPath_UnPrintableCharacters|SheetName_UnPrintableCharacters|
	|33|400|SheetPath_UnPrintableCharacters|SheetName_UnPrintableCharacters|
	|34|400|SheetPath_UnPrintableCharacters|SheetName_UnPrintableCharacters|
	|35|400|SheetPath_UnPrintableCharacters|SheetName_UnPrintableCharacters|
	|36|400|SheetPath_UnPrintableCharacters|SheetName_UnPrintableCharacters|
	|37|400|SheetPath_UnPrintableCharacters|SheetName_UnPrintableCharacters|
	|38|400|SheetPath_UnPrintableCharacters|SheetName_UnPrintableCharacters|
	|39|400|SheetPath_UnPrintableCharacters|SheetName_UnPrintableCharacters|
	|40|400|SheetPath_UnPrintableCharacters|SheetName_UnPrintableCharacters|
	|41|400|SheetPath_UnPrintableCharacters|SheetName_UnPrintableCharacters|
	|42|400|SheetPath_UnPrintableCharacters|SheetName_UnPrintableCharacters|
	|43|400|SheetPath_UnPrintableCharacters|SheetName_UnPrintableCharacters|
	|44|400|SheetPath_UnPrintableCharacters|SheetName_UnPrintableCharacters|
	|45|400|SheetPath_UnPrintableCharacters|SheetName_UnPrintableCharacters|
	|46|400|SheetPath_UnPrintableCharacters|SheetName_UnPrintableCharacters|
	|47|400|SheetPath_UnPrintableCharacters|SheetName_UnPrintableCharacters|
	|48|400|SheetPath_UnPrintableCharacters|SheetName_UnPrintableCharacters|
	|49|400|SheetPath_UnPrintableCharacters|SheetName_UnPrintableCharacters|
	|50|400|SheetPath_UnPrintableCharacters|SheetName_UnPrintableCharacters|
	|51|400|SheetPath_UnPrintableCharacters|SheetName_UnPrintableCharacters|
	|52|400|SheetPath_UnPrintableCharacters|SheetName_UnPrintableCharacters|
	|53|400|SheetPath_UnPrintableCharacters|SheetName_UnPrintableCharacters|
	|54|400|SheetPath_UnPrintableCharacters|SheetName_UnPrintableCharacters|
	|55|400|SheetPath_UnPrintableCharacters|SheetName_UnPrintableCharacters|
	|56|400|SheetPath_UnPrintableCharacters|SheetName_UnPrintableCharacters|
	|57|400|SheetPath_UnPrintableCharacters|SheetName_UnPrintableCharacters|
	|58|400|SheetPath_UnPrintableCharacters|SheetName_UnPrintableCharacters|
	|59|400|SheetPath_UnPrintableCharacters|SheetName_UnPrintableCharacters|
