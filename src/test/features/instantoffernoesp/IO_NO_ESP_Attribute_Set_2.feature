# Instant Offer No ESP Field validations covered for fields - Children[1],Children[2],Children[3]																																						
Feature: Validating Field Validation of Sign Up services for Field "BabyField-Children" of template type Instant Offer No ESP. 																																	
																																	
																														
@smokeTest																																	
Scenario Outline: IONOESP082 - Instant Offer No ESP Template Test- Validate the response when blank FIRSTNAMEUNKNOWN of First children is sent in request.																																	
	Given User want to hit "InstantOfferNoESP" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex  | code |SheetPath										 |SheetName							  							|Message					          		 |		
	|83        | 400  |SheetPath_InstantOfferNoESP   		 |SheetName_InstantOfferNoESP_Attributes     |CHILDRENFIRSTNAMEUNKNOWNBLANK   |																														
																																	
																																	
																																	
@smokeTest																																	
Scenario Outline: IONOESP083 - Instant Offer No ESP Template Test- Validate the response when blank FIRSTNAME of First children is sent in request.																																	
	Given User want to hit "InstantOfferNoESP" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath												|SheetName											  				 |Message								   |
	|84       | 400  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes      |CHILDRENFIRSTNAMEBLANK   |																														
																																	
																																	
																																	
@smokeTest																																	
Scenario Outline: IONOESP084 - Instant Offer No ESP Template Test- Validate the response when blank HASDOB of First children is sent in request.																																	
	Given User want to hit "InstantOfferNoESP" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath												|SheetName								  		  				 |Message				 		  	 |			
	|85       | 400  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes      |CHILDRENHASDOBBLANK    |																														
																																	
																																
@smokeTest																																	
Scenario Outline: IONOESP085 - Instant Offer No ESP Template Test- Validate the response when blank DATE OF BIRTH of First children is sent in request.																																	
	Given User want to hit "InstantOfferNoESP" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath												|SheetName											 						|Message				 				      |
	|86       | 400  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes   	  |CHILDRENDATEOFBIRTHBLANK 	  |																												
																																	
																																	
																																		
@smokeTest																																	
Scenario Outline: IONOESP086 - Instant Offer No ESP Template Test- Validate the response when DATEOF BIRTH of First children is  in FUTURE is sent in request.																																	
	Given User want to hit "InstantOfferNoESP" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath												|SheetName					  				 						 |Message				          			   |	
	|87       | 400  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes      |CHILDRENWITHFUTUREDATEOFBIRTH    |																														
																																	
																																	
																																	
@smokeTest																																	
Scenario Outline: IONOESP087 - Instant Offer No ESP Template Test- Validate the response when  EXPECTED BIRTH MONTH is BLANK of First children is sent in request.																																	
	Given User want to hit "InstantOfferNoESP" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath												|SheetName						  				  				 |Message				          		      |			
	|88       | 400  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes      |CHILDRENEXPECTEDBIRTHMONTHBLANK   |																														
																																	
																																	
																																	
@smokeTest																																	
Scenario Outline: IONOESP088 - Instant Offer No ESP Template Test- Validate the response when ExpectedBirthYIOr = current yIOr AND ExpectedMonth<currentMonth of First children is sent in request.																																	
	Given User want to hit "InstantOfferNoESP" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath									  |SheetName							  			  				 |Message				       				         |			
	|89       | 400  |SheetPath_InstantOfferNoESP   |SheetName_InstantOfferNoESP_Attributes      |CHILDRENWITHPASTEXPECTEDBIRTHMONTH   |																																
																																	
																																	
																																	
																																
@smokeTest																																	
Scenario Outline: IONOESP089 - Instant Offer No ESP Template Test- Validate the response when blank CHILDREN EXPECTED BIRTH YIOR of First children is sent in request.																																	
	Given User want to hit "InstantOfferNoESP" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath								|SheetName										   			 |Message				              		 |					
	|90       | 400  |SheetPath_InstantOfferNoESP 	|SheetName_InstantOfferNoESP_Attributes     |CHILDRENEXPECTEDBIRTHYEARBLANK   |																															
																																	
																																		
@smokeTest																																	
Scenario Outline: IONOESP090 - Instant Offer No ESP Template Test- Validate the response when Future EXPECTED BIRTH YIOR of First children is sent in request.																																	
	Given User want to hit "InstantOfferNoESP" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath										|SheetName					  					 			 |Message				                    	|				
	|91       | 400  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     |CHILDRENWITHFUTUREEXPECTEDBIRTHYEAR |																														
																																	
# *****This is not implemented yet since cant able to set Null values																																	
#@smokeTest																																	
#Scenario Outline: IONOESP091 - Instant Offer No ESP Template Test- Validate the response when CHILDREN WITH GENDER NULL of First children is sent in request.																																	
#	Given User want to hit "InstantOfferNoESP" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
#	When  When user hit "post" requests with "null" "gender" field																																
#	Then  User should received response status <code>																																
#	And   User validated the response <Message> received																																
#																																	
#	Examples:																																
#	|Rowindex | code |SheetPath							|SheetName					  |Message				       |																
#	|92       | 400  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     |CHILDRENWITHGENDERNULL_1    |																														
																																	
																																	
																																	
@smokeTest																																	
Scenario Outline: IONOESP092 - Instant Offer No ESP Template Test- Validate the response when Invalid Gender Value of First children is sent in request.																																	
	Given User want to hit "InstantOfferNoESP" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath										|SheetName					 					   					|Message				   				         |			
	|93       | 400  |SheetPath_InstantOfferNoESP   |SheetName_InstantOfferNoESP_Attributes     |CHILDRENWITHINVALIDGENDERVALUE    |																																
																																	
																																	
																																
@smokeTest																																	
Scenario Outline: IONOESP093 - Instant Offer No ESP Template Test- Validate the response when ExpectedBirthMonth < 0 of First children is sent in request.																																	
	Given User want to hit "InstantOfferNoESP" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath										|SheetName									  				 |Message				                 		    |						
	|94       | 400  |SheetPath_InstantOfferNoESP  	|SheetName_InstantOfferNoESP_Attributes  |CHILDRENWITHINVALIDEXPECTEDBIRTHMONTH |																															
																																	
																																		
@smokeTest																																	
Scenario Outline: IONOESP094 - Instant Offer No ESP Template Test- Validate the response when ExpectedBirthMonth > 12 of First children is sent in request.																																	
	Given User want to hit "InstantOfferNoESP" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath								  			|SheetName					            				  |Message				                 				  |							
	|95       | 400  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     |CHILDRENWITHINVALIDEXPECTEDBIRTHMONTH    |																														
																																	
																																
@smokeTest																																	
Scenario Outline: IONOESP095 - Instant Offer No ESP Template Test- Validate the response when ExpectedBirthYIOr < current yIOr of First children is sent in request.																																	
	Given User want to hit "InstantOfferNoESP" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath												|SheetName									  				 |Message				            			     |			
	|96       | 400  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes  |CHILDRENWITHPASTEXPECTEDBIRTHMONTH   |																														
																																	
																																	
																																			
@smokeTest																																	
Scenario Outline: IO96 - Instant Offer No ESP Template Test- Validate the response when blank FIRSTNAMEUNKNOWN of Second children is sent in request.																																	
	Given User want to hit "InstantOfferNoESP" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath										  |SheetName					             					|Message					       					  |					
	|97      | 400  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     |CHILDRENFIRSTNAMEUNKNOWNBLANK_1    |																														
																																	
																																	
																																	
@smokeTest																																	
Scenario Outline: IONOESP097 - Instant Offer No ESP Template Test- Validate the response when blank FIRSTNAME of Second children is sent in request.																																	
	Given User want to hit "InstantOfferNoESP" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath										|SheetName										  				 |Message									   |	
	|98      | 400  |SheetPath_InstantOfferNoESP   	|SheetName_InstantOfferNoESP_Attributes    |CHILDRENFIRSTNAMEBLANK_1   |																															
																																	
																																	
																																	
@smokeTest																																	
Scenario Outline: IONOESP098 - Instant Offer No ESP Template Test- Validate the response when blank HASDOB of Second children is sent in request.																																	
	Given User want to hit "InstantOfferNoESP" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath						  				|SheetName					  					 					|Message				  				|																
	|99      | 400  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     |CHILDRENHASDOBBLANK_1    |																														
																																	
																																
@smokeTest																																	
Scenario Outline: IONOESP099 - Instant Offer No ESP Template Test- Validate the response when blank DATE OF BIRTH of Second children is sent in request.																																	
	Given User want to hit "InstantOfferNoESP" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath						  					|SheetName					  					 					|Message				    			     |																
	|100      | 400  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     |CHILDRENDATEOFBIRTHBLANK_1    |																														
																																	
																																	
																																		
@smokeTest																																	
Scenario Outline: IONOESP100 - Instant Offer No ESP Template Test- Validate the response when DATEOF BIRTH of Second children is in FUTURE is sent in request.																																	
	Given User want to hit "InstantOfferNoESP" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath											|SheetName					 										  |Message				        				    |	
	|101      | 400  |SheetPath_InstantOfferNoESP   	|SheetName_InstantOfferNoESP_Attributes     |CHILDRENWITHFUTUREDATEOFBIRTH_1    |																															
																																	
																																	
																																
@smokeTest																																	
Scenario Outline: IONOESP101 - Instant Offer No ESP Template Test- Validate the response when  EXPECTED BIRTH MONTH is BLANK of Second children is sent in request.																																	
	Given User want to hit "InstantOfferNoESP" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath									|SheetName										  				 |Message				               				|				
	|102      | 400  |SheetPath_InstantOfferNoESP |SheetName_InstantOfferNoESP_Attributes    |CHILDRENEXPECTEDBIRTHMONTHBLANK_1   |																																
																																	
																																	
																																	
@smokeTest																																	
Scenario Outline: IONOESP102 - Instant Offer No ESP Template Test- Validate the response when ExpectedBirthYIOr = current yIOr AND ExpectedMonth<currentMonth of Second children is sent in request.																																	
	Given User want to hit "InstantOfferNoESP" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath												|SheetName									     					|Message				                       |																
	|103      | 400  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     |CHILDRENWITHPASTEXPECTEDBIRTHMONTH_1  |																														
																																	
																																	
																																	
																																	
@smokeTest																																	
Scenario Outline: IONOESP103 - Instant Offer No ESP Template Test- Validate the response when blank CHILDREN EXPECTED BIRTH YIOR of Second children is sent in request.																																	
	Given User want to hit "InstantOfferNoESP" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath										|SheetName									  	 			 |Message				              			 |																
	|104      | 400  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     |CHILDRENEXPECTEDBIRTHYEARBLANK_1   |																														
																																	

																																
@smokeTest																																	
Scenario Outline: IONOESP104 - Instant Offer No ESP Template Test- Validate the response when Future EXPECTED BIRTH YIOR of Second children is sent in request.																																	
	Given User want to hit "InstantOfferNoESP" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath										|SheetName						  				 			 |Message				                   			 |																
	|105      | 400  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     |CHILDRENWITHFUTUREEXPECTEDBIRTHYEAR_1  |																														
																																	
# *****This is not implemented yet since cant able to set Null values																																	
#@smokeTest																																	
#Scenario Outline: IONOESP105 - Instant Offer No ESP Template Test- Validate the response when CHILDREN WITH GENDER NULL of Second children is sent in request.																																	
#	Given User want to hit "InstantOfferNoESP" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
#	When  When user hit "post" requests with "null" "gender" field																																
#	Then  User should received response status <code>																																
#	And   User validated the response <Message> received																																
#																																	
#	Examples:																																
#	|Rowindex | code |SheetPath							|SheetName					  |Message				       |																
#	|106       | 400  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     |CHILDRENWITHGENDERNULL_2    |																														
																																	
																																	
																																		
@smokeTest																																	
Scenario Outline: IONOESP106 - Instant Offer No ESP Template Test- Validate the response when Invalid Gender Value of Second children is sent in request.																																	
	Given User want to hit "InstantOfferNoESP" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath												|SheetName					  					 					|Message				               			 |																
	|107      | 400  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     |CHILDRENWITHINVALIDGENDERVALUE_1    |																														
																																	
																																	
																																	
@smokeTest																																	
Scenario Outline: IONOESP107 - Instant Offer No ESP Template Test- Validate the response when ExpectedBirthMonth < 0 of Second children is sent in request.																																	
	Given User want to hit "InstantOfferNoESP" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath												|SheetName					  					 				  |Message				                      			|																
	|108      | 400  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     |CHILDRENWITHINVALIDEXPECTEDBIRTHMONTH_1    |																														
																																	
																																
@smokeTest																																	
Scenario Outline: IONOESP108 - Instant Offer No ESP Template Test- Validate the response when ExpectedBirthMonth > 12 of Second children is sent in request.																																	
	Given User want to hit "InstantOfferNoESP" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex  | code |SheetPath											|SheetName					  										|Message				                      			|																
	|109       | 400  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     |CHILDRENWITHINVALIDEXPECTEDBIRTHMONTH_1    |																														
																																	

																																		
@smokeTest																																	
Scenario Outline: IONOESP109 - Instant Offer No ESP Template Test- Validate the response when ExpectedBirthYIOr < current yIOr of Second children is sent in request.																																	
	Given User want to hit "InstantOfferNoESP" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath												|SheetName					 										  |Message				                   			 |																
	|110      | 400  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     |CHILDRENWITHPASTEXPECTEDBIRTHMONTH_1    |																														
																																	
																																	
																															
# The below test scenarios were writter in last thats why S.No is not in serial.
																																																																																												
@smokeTest																																	
Scenario Outline: IONOESP131 - Instant Offer No ESP Template Test- Validate the response when blank FIRSTNAMEUNKNOWN of Third children is sent in request.																																	
	Given User want to hit "InstantOfferNoESP" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath												  |SheetName					             |Message					       					   |					
	|132      | 400  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     |CHILDRENFIRSTNAMEUNKNOWNBLANK_2    |																														
																																	
																																	
																																		
@smokeTest																																	
Scenario Outline: IONOESP132 - Instant Offer No ESP Template Test- Validate the response when blank FIRSTNAME of Third children is sent in request.																																	
	Given User want to hit "InstantOfferNoESP" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath												|SheetName										  |Message									   |	
	|133      | 400  |SheetPath_InstantOfferNoESP   	|SheetName_InstantOfferNoESP_Attributes    |CHILDRENFIRSTNAMEBLANK_2    |																															
																																	
																																	
																																
@smokeTest																																	
Scenario Outline: IONOESP133 - Instant Offer No ESP Template Test- Validate the response when blank HASDOB of Third children is sent in request.																																	
	Given User want to hit "InstantOfferNoESP" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath													|SheetName					  					 |Message				    			 |																
	|134      | 400  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     |CHILDRENHASDOBBLANK_2    |																														
																																	
																																		
@smokeTest																																	
Scenario Outline: IONOESP134 - Instant Offer No ESP Template Test- Validate the response when blank DATE OF BIRTH of Third children is sent in request.																																	
	Given User want to hit "InstantOfferNoESP" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath													|SheetName					  					 |Message				      				  |																
	|135      | 400  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     |CHILDRENDATEOFBIRTHBLANK_2    |																														
																																	
																																	
																																
@smokeTest																																	
Scenario Outline: IONOESP135 - Instant Offer No ESP Template Test- Validate the response when DATEOF BIRTH of Third children is  in FUTURE is sent in request.																																	
	Given User want to hit "InstantOfferNoESP" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath												|SheetName					 						 |Message				        				     |	
	|136      | 400  |SheetPath_InstantOfferNoESP   	|SheetName_InstantOfferNoESP_Attributes     |CHILDRENWITHFUTUREDATEOFBIRTH_2    |																															
																																	
																																	
																																
@smokeTest																																	
Scenario Outline: IONOESP136 - Instant Offer No ESP Template Test- Validate the response when  EXPECTED BIRTH MONTH is BLANK of Third children is sent in request.																																	
	Given User want to hit "InstantOfferNoESP" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath										|SheetName										  |Message				               				|				
	|137      | 400  |SheetPath_InstantOfferNoESP |SheetName_InstantOfferNoESP_Attributes    |CHILDRENEXPECTEDBIRTHMONTHBLANK_2    |																																
																																	
																																	
																																			
@smokeTest																																	
Scenario Outline: IONOESP137 - Instant Offer No ESP Template Test- Validate the response when ExpectedBirthYIOr = current yIOr AND ExpectedMonth<currentMonth of Third children is sent in request.																																	
	Given User want to hit "InstantOfferNoESP" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath										|SheetName					  					 |Message				                   				|																
	|138      | 400  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     |CHILDRENWITHPASTEXPECTEDBIRTHMONTH_2    |																														
																																	
																																	
																																	
																																	
@smokeTest																																	
Scenario Outline: IONOESP138 - Instant Offer No ESP Template Test- Validate the response when blank CHILDREN EXPECTED BIRTH YIOR of Third children is sent in request.																																	
	Given User want to hit "InstantOfferNoESP" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath										|SheetName					  					 			 |Message				             				  |																
	|139      | 400  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     |CHILDRENEXPECTEDBIRTHYEARBLANK_2    |																														
																																	
																																
@smokeTest																																	
Scenario Outline: IONOESP139 - Instant Offer No ESP Template Test- Validate the response when Future EXPECTED BIRTH YIOR of Third children is sent in request.																																	
	Given User want to hit "InstantOfferNoESP" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath									  		|SheetName									    			  		 |Message				                        |																
	|140      | 400  |SheetPath_InstantOfferNoESP   	|SheetName_InstantOfferNoESP_Attributes      |CHILDRENWITHFUTUREEXPECTEDBIRTHYEAR_2 |																												
																																	
																																

@smokeTest																																	
Scenario Outline: IONOESP140 - Instant Offer No ESP Template Test- Validate the response when CHILDREN WITH GENDER NULL of Third children is sent in request.																																	
	Given User want to hit "InstantOfferNoESP" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  user hit "post" requests with "null" "gender" field	of "Third" children																														
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath											  |SheetName					  									  |Message				       			 |																
	|141      | 400  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     |CHILDRENWITHGENDERNULL_2    |																														
																																	
																																	
																																		
@smokeTest																																	
Scenario Outline: IONOESP141 - Instant Offer No ESP Template Test- Validate the response when Invalid Gender Value of Third children is sent in request.																																	
	Given User want to hit "InstantOfferNoESP" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath							|SheetName					  |Message				               |																
	|142       | 400  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     |CHILDRENWITHINVALIDGENDERVALUE_2    |																														
																																	
																																	
																																	
@smokeTest																																	
Scenario Outline: IONOESP142 - Instant Offer No ESP Template Test- Validate the response when ExpectedBirthMonth < 0 of Third children is sent in request.																																	
	Given User want to hit "InstantOfferNoESP" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath							|SheetName					  |Message				                      |																
	|143       | 400  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     |CHILDRENWITHINVALIDEXPECTEDBIRTHMONTH_2    |																														
																																	
																																	
@smokeTest																																	
Scenario Outline: IONOESP143 - Instant Offer No ESP Template Test- Validate the response when ExpectedBirthMonth > 12 of Third children is sent in request.																																	
	Given User want to hit "InstantOfferNoESP" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath							|SheetName					  |Message				                      |																
	|144       | 400  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     |CHILDRENWITHINVALIDEXPECTEDBIRTHMONTH_2    |																														
																																	
																																
@smokeTest																																	
Scenario Outline: IONOESP144 - Instant Offer No ESP Template Test- Validate the response when ExpectedBirthYIOr < current yIOr of Third children is sent in request.																																	
	Given User want to hit "InstantOfferNoESP" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath													|SheetName					 						 |Message				                   				|																
	|145      | 400  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     |CHILDRENWITHPASTEXPECTEDBIRTHMONTH_2    |																														
																																	
																																
																															