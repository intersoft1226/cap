# Instant Offer No ESP Field validations covered for Custom fields
Feature: Validating Sign Up services for Field "Custom fields" of template type Instant Offer No ESP


@smokeTest
Scenario Outline: IONOESP110 - Instant Offer No ESP Template Test- Validate the response when blank custom id of CheckBoxtype Custom field data is sent in request.
	Given User want to hit "InstantOfferNoESP" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  user hit "post" requests with "blank" "customFieldId" field
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex 	| code		 |SheetPath												|SheetName					    			  				 |Message					     |
	|111        | 400  		 |SheetPath_InstantOfferNoESP   	|SheetName_InstantOfferNoESP_Attributes  |CUSTOMBLANKFIELDID   |
	

@smokeTest
Scenario Outline: IONOESP111 - Instant Offer No ESP Template Test- Validate the response when blank custom value of CheckBoxtype Custom field data is sent in request.
	Given User want to hit "InstantOfferNoESP" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  user hit "post" requests with "blank" "customFieldValue" field
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex 	| code		 |SheetPath												|SheetName					  				|Message					        |
	|112        | 400  		 |SheetPath_InstantOfferNoESP   	|SheetName_InstantOfferNoESP_Attributes  |CUSTOMBLANKFIELDVALUE    |
	


@smokeTest
Scenario Outline: IONOESP112 - Instant Offer No ESP Template Test- Validate the response when Invalid custom value of CheckBox type Custom field is sent in request.
	Given User want to hit "InstantOfferNoESP" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  user hit "post" requests with "invalid" "invalidCustomFieldValue" field
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex 	| code		 |SheetPath												|SheetName					  				|Message			   		        |
	|113        | 400  		 |SheetPath_InstantOfferNoESP   	|SheetName_InstantOfferNoESP_Attributes     |CUSTOMINVALIDFIELDVALUE    |
	
	

@smokeTest
Scenario Outline: IONOESP113 - Instant Offer No ESP Template Test- Validate the response when Custom Field with multiple valid values for Checkbox type is sent in request.
	Given User want to hit "InstantOfferNoESP" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath											|SheetName									     |Message    |
	|114      | 200  |SheetPath_InstantOfferNoESP   |SheetName_InstantOfferNoESP_Attributes     | success   |		
	


@smokeTest
Scenario Outline: IONOESP114 - Instant Offer No ESP Template Test- Validate the response when Non mandatory Blank Custom CheckBox type Field Id is sent in request.
	Given User want to hit "InstantOfferNoESP" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  user hit "post" requests with "blank" "customFieldId" field
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName									     				  |Message               				|
	|115      | 400  |SheetPath_InstantOfferNoESP   |SheetName_InstantOfferNoESP_Attributes     | CUSTOMNONMANDATORYBLANKID   |	
	
	

@smokeTest
Scenario Outline: IONOESP115 - Instant Offer No ESP Template Test- Validate the response when Non IOndatory Invalid Custom Field Value is sent in request.
	Given User want to hit "InstantOfferNoESP" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  user hit "post" requests with "invalid" "invalidCustomFieldValue" field
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath											|SheetName									  |Message                      |
	|116      | 400  |SheetPath_InstantOfferNoESP   |SheetName_InstantOfferNoESP_Attributes  | CUSTOMINVALIDFIELDVALUE     |		
		
			


@smokeTest
Scenario Outline: IONOESP116 - Instant Offer No ESP Template Test- Validate the response when Non Mandatory Custom Field with multiple valid values is sent in request.
	Given User want to hit "InstantOfferNoESP" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath											|SheetName									  				 |Message    |
	|117      | 200  |SheetPath_InstantOfferNoESP   	|SheetName_InstantOfferNoESP_Attributes  | success   |		
	

	
@smokeTest
Scenario Outline: IONOESP117 - Instant Offer No ESP Template Test- Validate the response when blank custom id of Radio field type Custom field data is sent in request.
	Given User want to hit "InstantOfferNoESP" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  user hit "post" requests with "blank" "customFieldId" field
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex 	| code		 |SheetPath												|SheetName					  								 |Message					     |
	|118        | 400  		 |SheetPath_InstantOfferNoESP   	  |SheetName_InstantOfferNoESP_Attributes  |CUSTOMBLANKFIELDID   |
	
	
	
@smokeTest
Scenario Outline: IONOESP118 - Instant Offer No ESP Template Test- Validate the response when blank custom value of Radio type Custom field data is sent in request.
	Given User want to hit "InstantOfferNoESP" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  user hit "post" requests with "blank" "customFieldValue" field
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex 	| code		 |SheetPath											|SheetName					  								 |Message					        |
	|119        | 400  		 |SheetPath_InstantOfferNoESP   	|SheetName_InstantOfferNoESP_Attributes  |CUSTOMBLANKFIELDVALUE   |
	


@smokeTest
Scenario Outline: IONOESP119 - Instant Offer No ESP Template Test- Validate the response when Invalid custom value of Radio type Custom field is sent in request.
	Given User want to hit "InstantOfferNoESP" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  user hit "post" requests with "invalid" "invalidCustomFieldValue" field
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex 	| code		 |SheetPath											|SheetName					  								 |Message			   		        |
	|120        | 400  		 |SheetPath_InstantOfferNoESP   	|SheetName_InstantOfferNoESP_Attributes  |CUSTOMINVALIDFIELDVALUE   |
	
	

@smokeTest
Scenario Outline: IONOESP120 - Instant Offer No ESP Template Test- Validate the response when Custom Field with multiple valid values for Radio Field is sent in request.
	Given User want to hit "InstantOfferNoESP" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  user hit "post" requests with "multiple" "validCustomFieldValue" field
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath											|SheetName									  				 		|Message                    |
	|121      | 400  |SheetPath_InstantOfferNoESP   |SheetName_InstantOfferNoESP_Attributes   | CUSTOMINVALIDFIELDVALUE   |		
	

@smokeTest
Scenario Outline: IONOESP121 - Instant Offer No ESP Template Test- Validate the response when Non Mandatory Blank Custom Radio Field Id is sent in request.
	Given User want to hit "InstantOfferNoESP" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  user hit "post" requests with "blank" "customFieldId" field
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName									  				 |Message                      |
	|122      | 400  |SheetPath_InstantOfferNoESP   |SheetName_InstantOfferNoESP_Attributes  | CUSTOMNONMANDATORYBLANKID   |	
	
	

@smokeTest
Scenario Outline: IONOESP122 - Instant Offer No ESP Template Test- Validate the response when Non Mandatory Invalid Radio type Custom Field Value is sent in request.
	Given User want to hit "InstantOfferNoESP" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  user hit "post" requests with "invalid" "invalidCustomFieldValue" field
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName									  				 |Message                      |
	|123      | 400  |SheetPath_InstantOfferNoESP   |SheetName_InstantOfferNoESP_Attributes  | CUSTOMINVALIDFIELDVALUE     |		
	
	

@smokeTest
Scenario Outline: IONOESP123 - Instant Offer No ESP Template Test- Validate the response when Non Mandatory Custom Radio Field with multiple valid values is sent in request.
	Given User want to hit "InstantOfferNoESP" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  user hit "post" requests with "multiple" "validCustomFieldValue" field
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName									  				 |Message                    |
	|124      | 400  |SheetPath_InstantOfferNoESP   |SheetName_InstantOfferNoESP_Attributes  | CUSTOMINVALIDFIELDVALUE   |		
	
	
	

@smokeTest
Scenario Outline: IONOESP124 - Instant Offer No ESP Template Test- Validate the response when blank custom id of DropDown field type Custom field data is sent in request.
	Given User want to hit "InstantOfferNoESP" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  user hit "post" requests with "blank" "customFieldId" field
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex 	| code		 |SheetPath											|SheetName					  								 |Message					     |
	|125        | 400  		 |SheetPath_InstantOfferNoESP   	|SheetName_InstantOfferNoESP_Attributes  |CUSTOMBLANKFIELDID   |
	
	
	
@smokeTest
Scenario Outline: IONOESP125 - Instant Offer No ESP Template Test- Validate the response when blank custom value of DropDown type Custom field data is sent in request.
	Given User want to hit "InstantOfferNoESP" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  user hit "post" requests with "blank" "customFieldValue" field
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex 	| code		 |SheetPath											|SheetName					  								 |Message					        |
	|126        | 400  		 |SheetPath_InstantOfferNoESP   	|SheetName_InstantOfferNoESP_Attributes  |CUSTOMBLANKFIELDVALUE   |
	

	
@smokeTest
Scenario Outline: IONOESP126 - Instant Offer No ESP Template Test- Validate the response when Invalid custom value of DropDown type Custom field is sent in request.
	Given User want to hit "InstantOfferNoESP" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  user hit "post" requests with "invalid" "invalidCustomFieldValue" field
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex 	| code		 |SheetPath											|SheetName					  								 |Message			   		        |
	|127        | 400  		 |SheetPath_InstantOfferNoESP   	|SheetName_InstantOfferNoESP_Attributes  |CUSTOMINVALIDFIELDVALUE   |
	
	

@smokeTest
Scenario Outline: IONOESP127 - Instant Offer No ESP Template Test- Validate the response when Custom Field with multiple valid values for DropDown Field is sent in request.
	Given User want to hit "InstantOfferNoESP" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  user hit "post" requests with "multiple" "validCustomFieldValue" field
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName											  				 | Message                   |
	|128      | 400  |SheetPath_InstantOfferNoESP   |SheetName_InstantOfferNoESP_Attributes      | CUSTOMINVALIDFIELDVALUE   |		
	

@smokeTest
Scenario Outline: IONOESP128 - Instant Offer No ESP Template Test- Validate the response when Non Mandatory Blank Custom DropDown Field Id is sent in request.
	Given User want to hit "InstantOfferNoESP" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  user hit "post" requests with "blank" "customFieldId" field
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName	    								  				 |Message                      |
	|129      | 400  |SheetPath_InstantOfferNoESP   |SheetName_InstantOfferNoESP_Attributes      | CUSTOMNONMANDATORYBLANKID   |	
	
	

@smokeTest
Scenario Outline: IONOESP129 - Instant Offer No ESP Template Test- Validate the response when Non Mandatory Invalid DropDown type Custom Field Value is sent in request.
	Given User want to hit "InstantOfferNoESP" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  user hit "post" requests with "invalid" "invalidCustomFieldValue" field
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName									  				 |Message                      |
	|130      | 400  |SheetPath_InstantOfferNoESP   |SheetName_InstantOfferNoESP_Attributes  | CUSTOMINVALIDFIELDVALUE     |		
	
	

@smokeTest
Scenario Outline: IONOESP130 - Instant Offer No ESP Template Test- Validate the response when Non Mandatory Custom DropDown Field with multiple valid values is sent in request.
	Given User want to hit "InstantOfferNoESP" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  user hit "post" requests with "multiple" "validCustomFieldValue" field
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName									  				 |Message                    |
	|131      | 400  |SheetPath_InstantOfferNoESP   |SheetName_InstantOfferNoESP_Attributes  | CUSTOMINVALIDFIELDVALUE   |		
		