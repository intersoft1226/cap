@functional
@instant_offer_No_ESP_functional_suite
Feature: Functional Validations of Instant Offer No ESP
     		
@instant_offer_No_ESP
Scenario Outline: Instant Offer No ESP 01 : Verify that for new user offer is assigned successfully
	Given User updated the field "email" in workbook "SheetName_InstantOfferNoESP" of file "SheetPath_InstantOfferNoESP" on row 2
	And User want to hit "InstantOffer_NoESP" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests 
	Then  User should received response status <code>
	And   User verify "offerStatusCd" flag of "myOffersResult" array  is "0" in results
	
	Examples:
	|Rowindex 	 	| code |SheetPath												|SheetName								 |
	|2  	   			| 200  |SheetPath_InstantOfferNoESP 		|SheetName_InstantOfferNoESP |
	

Scenario Outline: Instant Offer No ESP 02 : Verify that for existing user, offer should not be assigned
	Given User want to hit "InstantOffer_NoESP" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests 
	Then  User should received response status <code>
	And   User verify "offerStatusCd" flag of "myOffersResult" array  is "1" in results
	
	Examples:
	|Rowindex 	 	| code |SheetPath												|SheetName									 |
	|2  	   			| 200  |SheetPath_InstantOfferNoESP 		|SheetName_InstantOfferNoESP |
	

	Scenario Outline: Instant Offer No ESP 03 : Validate that the invalid field passes in payload
	Given User want to hit "InstantOffer_NoESP" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests 
	Then  User should received response status <code>
	And   User verify "responsePage" flag of "" array  is "null" in results
	
	Examples:
	|Rowindex 	 	| code |SheetPath												|SheetName									 |
	|3  	   			| 400  |SheetPath_InstantOfferNoESP 		|SheetName_InstantOfferNoESP |
	
	
@200
	Scenario Outline: Instant Offer No ESP 04 : Validate the Api respone on passing incorrect Source id "offerStatusCd" should be "701"
	Given User want to hit "InstantOffer_NoESP" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests 
	Then  User should received response status <code>
	And   User verify "offerStatusCd" flag of "myOffersResult" array  is "701" in results
	
	Examples:
	|Rowindex 	 	| code |SheetPath												|SheetName									 |
	|4  	   			| 200  |SheetPath_InstantOfferNoESP 		|SheetName_InstantOfferNoESP |
	

	Scenario Outline: Instant Offer No ESP 05 : Validate the Api respone on passing incorrect templateid, response code should be 400
	Given User want to hit "InstantOffer_NoESP" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests 
	Then  User should received response status <code>
	And   User verify "responsePage" flag of "" array  is "null" in results
	
	Examples:
	|Rowindex 	 	| code |SheetPath												|SheetName									 |
	|5  	   			| 400  |SheetPath_InstantOfferNoESP 		|SheetName_InstantOfferNoESP |
	

	Scenario Outline: Instant Offer No ESP 06 : "Validate that if a Coupon  is Expired & user hit the api to get the offer
	Given User want to hit "InstantOffer_NoESP" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests 
	Then  User should received response status <code>
	And   User verify "offerStatusCd" flag of "myOffersResult" array  is "615" in results
	
	Examples:
	|Rowindex 	 	| code |SheetPath												|SheetName									 |
	|6  	   			| 200  |SheetPath_InstantOfferNoESP 		|SheetName_InstantOfferNoESP |
	


	Scenario Outline: Instant Offer No ESP 07 :Veriy that if MO Code is already assigned to another user api will return "offerStatusCd": "580" in response
	Given User want to hit "InstantOffer_NoESP" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests 
	Then  User should received response status <code>
	And   User verify "offerStatusCd" flag of "myOffersResult" array  is "580" in results
	
	Examples:
	|Rowindex 	 	| code |SheetPath												|SheetName									 |
	|7  	   			| 200  |SheetPath_InstantOfferNoESP 		|SheetName_InstantOfferNoESP |
	
	

	Scenario Outline: Instant Offer No ESP 08 :Veriy that if Sourceid is Invalid api will return 132 status code
	Given User want to hit "InstantOffer_NoESP" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests 
	Then  User should received response status <code>
	And   User verify "offerStatusCd" flag of "myOffersResult" array  is "132" in results
	
	Examples:
	|Rowindex 	 	| code |SheetPath												|SheetName									 |
	|8  	   			| 200  |SheetPath_InstantOfferNoESP 		|SheetName_InstantOfferNoESP |
	

	Scenario Outline: Instant Offer No ESP 09 :Veriy that if MO Code is Invalid api will return 132 status code
	Given User want to hit "InstantOffer_NoESP" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests 
	Then  User should received response status <code>
	And   User verify "offerStatusCd" flag of "myOffersResult" array  is "132" in results
	
	Examples:
	|Rowindex 	 	| code |SheetPath												|SheetName									 |
	|9  	   			| 200  |SheetPath_InstantOfferNoESP 		|SheetName_InstantOfferNoESP |