# Instant Offer No ESP Field validations covered for fields - Title, PostalCode, 
# Province, State, Phone, MOCode, FirstName, LastName, EmailConsent, Email, City, 
# BounceXFields, BabyField, Address1, Address2, MobilePhone, Zipcode
Feature: Validating Field Validation for Sign Up services of "Instant Offer No ESP" template type. 


@smokeTest
Scenario Outline: IONOESP001 - Instant Offer No ESP Template Test- Validate the valid response when valid input parameters is provided.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	
	Examples:	
	|Rowindex | code |SheetPath											|SheetName									             |
	|2        | 200  |SheetPath_InstantOfferNoESP   |SheetName_InstantOfferNoESP_Attributes  |



@smokeTest
Scenario Outline: IONOESP002 - Instant Offer No ESP Template Test- Validate the response when Blank Mobile Phone is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests with "blank" "mobilePhone" field 
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath											|SheetName					  										|Message			   			|
	|3        | 400  |SheetPath_InstantOfferNoESP   	|SheetName_InstantOfferNoESP_Attributes     |MOBILEPHONEREQUIRED  |
#	


@smokeTest
Scenario Outline: IONOESP003 - Instant Offer No ESP Template Test- Validate the response when Invalid Mobile Phone is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message					  		   |
	|4        | 400  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     | MOBILEPHONEWITHLESSTHANMINLENGTH   |
	


@smokeTest
Scenario Outline: IONOESP004 - Instant Offer No ESP Template Test- Validate the response when Mobile Phone Exceeding Max Length is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message					  	    |
	|5        | 400  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     |MOBILEPHONEEXCEEDINGMAXLENGTH    |

	

@smokeTest
Scenario Outline: IONOESP005 - Instant Offer No ESP Template Test- Validate the response when Mobile Phone less than Min Length is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message					  		   |
	|6        | 400  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     |MOBILEPHONEWITHLESSTHANMINLENGTH    |
	
	
@smokeTest
Scenario Outline: IONOESP006 - Instant Offer No ESP Template Test- Validate the response when Mobile Phone having Two Hypens Invalid Format is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message					  			    |
	|7        | 400  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     |MOBILEPHONEWITHTWOHYPENSINVALIDFORMAT    |
		

@smokeTest
Scenario Outline: IONOESP007 - Instant Offer No ESP Template Test- Validate the response when Mobile Phone having AlphaNumeric value is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message					  	  |
	|8        | 400  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     |MOBILEPHONEWITHALPHANUMERIC    |


@smokeTest
Scenario Outline: IONOESP008 - Instant Offer No ESP Template Test- Validate the response when Mobile Phone having 12Digit value is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath								|SheetName					  								 |Message					 					|
	|9        | 400  |SheetPath_InstantOfferNoESP  |SheetName_InstantOfferNoESP_Attributes     |MOBILEPHONEWITH12DIGIT    |
	
	

@smokeTest
Scenario Outline: IONOESP009 - Instant Offer No ESP Template Test- Validate the response when address1 with Exceeding Max Length is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message					     					|
	|10       | 400  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     |ADDRESS1EXCEEDINGMAXLENGTH    |
	


@smokeTest
Scenario Outline: IONOESP010 - Instant Offer No ESP Template Test- Validate the response when address1 with Alphanumeric is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message  |
	|11       | 200  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     | success |
	


@smokeTest
Scenario Outline: IONOESP011 - Instant Offer No ESP Template Test- Validate the response when address1 with SpecialCharacters is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName									  				 |Message    |
	|12       | 200  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     | success   |	
	


@smokeTest
Scenario Outline: IONOESP012 - Instant Offer No ESP Template Test- Validate the response when address2 with Exceeding Max Length is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName									  				 |Message					    				 |
	|13       | 400  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     |ADDRESS2EXCEEDINGMAXLENGTH   |
	


@smokeTest
Scenario Outline: IONOESP013 - Instant Offer No ESP Template Test- Validate the response when address2 with Alphanumeric is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  									|Message  |
	|14       | 200  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes      | success |
	


@smokeTest
Scenario Outline: IONOESP013 - Instant Offer No ESP Template Test- Validate the response when address2 with SpecialCharacters is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message    |
	|15       | 200  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     | success   |	


	
	
@smokeTest
Scenario Outline: IONOESP014 - Instant Offer No ESP Template Test- Validate the response when babyField Exceeding MaxValue is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message                       |
	|16       | 400  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     | BABYFIELDEXCEEDINGMAXVALUE   |	
	
	
		
@smokeTest
Scenario Outline: IONOESP015 - Instant Offer No ESP Template Test- Validate the response when babyField With LessThan MinValue is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message                          |
	|17       | 400  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     | BABYFIELDWITHLESSTHANMINVALUE   |
	
# This will be valid if Baby Field is non mandatory		
#@smokeTest
#@currentTest1
#Scenario Outline: IONOESP016 - Instant Offer No ESP Template Test- Validate the response when babyField Accepting Zero is sent in request.
#	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
#	When  When user hit "post" requests
#	Then  User should received response status <code>
#	And   User validated the response <Message> received
#	
#	Examples:	
#	|Rowindex | code |SheetPath							|SheetName					  |Message    |
#	|18       | 200  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     | success   |	
#	
#@currentTest		
#@smokeTest
#Scenario Outline: IONOESP017 - Instant Offer No ESP Template Test- Validate the response when babyField Count Less Than ChildrenCount is sent in request.
#	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
#	When  user hit "post" requests with BabyField less than children count
#	Then  User should received response status <code>
#	And   User validated the response <Message> received
#	
#	Examples:	
#	|Rowindex | code |SheetPath							|SheetName					  |Message                                |
#	|19       | 200  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     | BABYFIELDCOUNTLESSTHANCHILDRENCOUNT   |		
	
		
@smokeTest
Scenario Outline: IONOESP018 - Instant Offer No ESP Template Test- Validate the response when CmpDeploymentStrategy field exceeds MaxLength is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message                           |
	|20       | 400  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     | CMPDEPLOYMENTSTRATEGYMAXLENGTH   |		
	
	
		
@smokeTest
Scenario Outline: IONOESP019 - Instant Offer No ESP Template Test- Validate the response when CmpNameMaxLength field exceeds MaxLength is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message             |
	|21       | 400  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     | CMPNAMEMAXLENGTH   |	
	
		
@smokeTest
Scenario Outline: IONOESP020 - Instant Offer No ESP Template Test- Validate the response when CmpVariationID field exceeds MaxLength is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message                    |
	|22       | 400  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     | CMPVARIATIONIDMAXLENGTH   |	
	
	
	
		
@smokeTest
Scenario Outline: IONOESP021 - Instant Offer No ESP Template Test- Validate the response when CmpConcept field exceeds MaxLength is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message                |
	|23       | 400  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     | CMPCONCEPTMAXLENGTH   |	
	
	
	
@smokeTest
Scenario Outline: IONOESP022 - Instant Offer No ESP Template Test- Validate the response when CmpDevice field exceeds MaxLength is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message               |
	|24       | 400  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     | CMPDEVICEMAXLENGTH   |	
	
	
	
@smokeTest
Scenario Outline: IONOESP023 - Instant Offer No ESP Template Test- Validate the response when CmpFormat field exceeds MaxLength is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					 									 |Message               |
	|25       | 400  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     | CMPFORMATMAXLENGTH   |		
	
	
	
@smokeTest
Scenario Outline: IONOESP024 - Instant Offer No ESP Template Test- Validate the response when City field exceeds MaxLength is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath									|SheetName					  								 |Message                   |
	|26       |400   |SheetPath_InstantOfferNoESP   	|SheetName_InstantOfferNoESP_Attributes     | CITYEXCEEDINGMAXLENGTH   |		
	
	
		
@smokeTest
Scenario Outline: IONOESP025 - Instant Offer No ESP Template Test- Validate the response when City Accepting Alphanumeric is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							 			|SheetName					  								 |Message    |
	|27       | 200  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     | success   |		
	
	
@smokeTest
Scenario Outline: IONOESP027 - Instant Offer No ESP Template Test- Validate the response when City Accepting SpecialCharacters is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					 								   |Message    |
	|28       | 200  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     | success   |		
	
		
@smokeTest
Scenario Outline: IONOESP028 - Instant Offer No ESP Template Test- Validate the response when email Without At The Rate is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message                  |
	|29       | 400  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     | EMAILWITHOUTATTHERATE   |	
	
		
@smokeTest
Scenario Outline: IONOESP029 - Instant Offer No ESP Template Test- Validate the response when email Without At Dot is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message            |
	|30       | 400  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     | EMAILWITHOUTDOT   |	
	
	
	
		
@smokeTest
Scenario Outline: IONOESP030 - Instant Offer No ESP Template Test- Validate the response when email With No Prefix before At The Rate is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message                             |
	|31       | 400  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     | EMAILWITHNOPREFIXBEFOREATTHERATE   |
	
	
		
@smokeTest
Scenario Outline: IONOESP031 - Instant Offer No ESP Template Test- Validate the response when email With exceeding Max length is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName													   |Message                               |
	|32       | 400  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     | EMAILWITHEMAILIDEXCEEDINGMAXLENGTH   |	
	
		
@smokeTest
Scenario Outline: IONOESP032 - Instant Offer No ESP Template Test- Validate the response when email With Multiple At the rate is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message                               |
	|33       | 400  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     | EMAILWITHMULTIPLEATTHERATEINEMAIL    |	
	
		
@smokeTest
Scenario Outline: IONOESP033 - Instant Offer No ESP Template Test- Validate the response when email With SpecialCharacters is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					 								   |Message                        |
	|34       | 400  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     | EMAILWITHSPECIALCHARACTERS    |		
	
		
@smokeTest
Scenario Outline: IONOESP034 - Instant Offer No ESP Template Test- Validate the response when email With Spaces is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message            |
	|35       | 400  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     | EMAILWITHSPACE    |	
	

		
@smokeTest
Scenario Outline: IONOESP035 - Instant Offer No ESP Template Test- Validate the response when email With LIOding Spaces is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message     |
	|36       | 200  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     | success    |	
	
		
@smokeTest
Scenario Outline: IONOESP036 - Instant Offer No ESP Template Test- Validate the response when email With Trailing Spaces is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message     |
	|37       | 200  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     | success    |	
	
		
@smokeTest
Scenario Outline: IONOESP037 - Instant Offer No ESP Template Test- Validate the response when EMAILCONSENT2MANDATORY is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message           				 |
	|38       | 400  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     | EMAILCONSENT2MANDATORY    |							
	


@smokeTest
Scenario Outline: IONOESP038 - Instant Offer No ESP Template Test- Validate the response when FirstName with Exceeding Max Length is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName													   |Message					    				   |
	|39       | 400  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     |FIRSTNAMEEXCEEDINGMAXLENGTH    |
	


@smokeTest
Scenario Outline: IONOESP039 - Instant Offer No ESP Template Test- Validate the response when FirstName with Alphanumeric is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message  |
	|40       | 200  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     | success |
	


@smokeTest
Scenario Outline: IONOESP040 - Instant Offer No ESP Template Test- Validate the response when FirstName with SpecialCharacters is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message    |
	|41       | 200  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     | success   |	
	
	
						
@smokeTest
Scenario Outline: IONOESP041 - Instant Offer No ESP Template Test- Validate the response when LastName with Exceeding Max Length is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message					      				 |
	|42       | 400  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     |LASTNAMEEXCEEDINGMAXLENGTH     |
	


@smokeTest
Scenario Outline: IONOESP042 - Instant Offer No ESP Template Test- Validate the response when LastName with Alphanumeric is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message  |
	|43       | 200  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     | success |
	


@smokeTest
Scenario Outline: IONOESP043 - Instant Offer No ESP Template Test- Validate the response when LastName with SpecialCharacters is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message    |
	|44       | 200  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     | success   |	
	
	

@smokeTest
Scenario Outline: IONOESP044 - Instant Offer No ESP Template Test- Validate the response when MoCode Exceeding Max Length is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message					   					|
	|45       | 400  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     |MOCODEEXCEEDINGMAXLENGTH    |

	

@smokeTest
Scenario Outline: IONOESP045 - Instant Offer No ESP Template Test- Validate the response when MOCode less than Min Length is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message					     					 |
	|46       | 400  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     |MOCODELESSTHANMINIMUMLENGTH    |
	
	
@smokeTest
Scenario Outline: IONOESP046 - Instant Offer No ESP Template Test- Validate the response when MO Code Alphanumeric is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message		 |
	|47       | 200  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     |success    |
		

@smokeTest
Scenario Outline: IONOESP047 - Instant Offer No ESP Template Test- Validate the response when MOCode having SpecialCharacters value is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message					  	  				 |
	|48       | 400  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     |MOCODEWITHSPECIALCHARACTERS    |
	

@smokeTest
Scenario Outline: IONOESP048 - Instant Offer No ESP Template Test- Validate the response when MOCode having HyphenValidFormat value is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message	   |
	|49       | 200  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     |success    |	
	

@smokeTest
Scenario Outline: IONOESP049 - Instant Offer No ESP Template Test- Validate the response when MOCode having Hyphen InValidFormat value is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message	                        |
	|50       | 400  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     |MOCODEWITHHYPHENINVALIDFORMAT   |	
		
#Mo Code ends	


@smokeTest
Scenario Outline: IONOESP050 - Instant Offer No ESP Template Test- Validate the response when Phone Exceeding Max Length is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message					   				 |
	|51       | 400  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     |PHONEEXCEEDINGMAXLENGTH    |

	

@smokeTest
Scenario Outline: IONOESP051 - Instant Offer No ESP Template Test- Validate the response when Phone less than Min Length is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message					      				|
	|52       | 400  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     |PHONEWITHLESSTHANMINLENGTH    |
	

@smokeTest
Scenario Outline: IONOESP052 - Instant Offer No ESP Template Test- Validate the response when phone With Two Hypens InvalidFormat is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message					  			    			 |
	|53       | 400  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     |PHONEWITHTWOHYPENSINVALIDFORMAT    |
		

@smokeTest
Scenario Outline: IONOESP053 - Instant Offer No ESP Template Test- Validate the response when Phone having AlphaNumeric value is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message					  	  	 |
	|54       | 400  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     |PHONEWITHALPHANUMERIC    |
	

@smokeTest
Scenario Outline: IONOESP054 - Instant Offer No ESP Template Test- Validate the response when Phone having 12Digit value is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message	   					|
	|55       | 400  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     |PHONEWITH12DIGIT    |	
	

@smokeTest
Scenario Outline: IONOESP055 - Instant Offer No ESP Template Test- Validate the response when Phone having Single Hyphen InValidFormat value is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message    |
	|56       | 200  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     |success    |	
		
		

@smokeTest
Scenario Outline: IONOESP056 - Instant Offer No ESP Template Test- Validate the response when PostalCode Exceeding Max Length is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message					  						  |
	|57       | 400  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     |POSTALCODEEXCEEDINGMAXLENGTH    |

	

@smokeTest
Scenario Outline: IONOESP057 - Instant Offer No ESP Template Test- Validate the response when PostalCode with Invalid Format is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message					      				 |
	|58       | 400  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     |POSTALCODEWITHINVALIDFORMAT    |
	


@smokeTest
Scenario Outline: IONOESP058 - Instant Offer No ESP Template Test- Validate the response when PostalCode With SpecialCharacters is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message					  			    			|
	|59       | 400  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     |POSTALCODEWITHSPECIALCHARACTER    |
		


@smokeTest
Scenario Outline: IONOESP059 - Instant Offer No ESP Template Test- Validate the response when PostalCode with Invalid Format2 value is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  							   |Message					  	 					  |
	|60       | 400  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     |POSTALCODEWITHINVALIDFORMAT2    |
	

@smokeTest
Scenario Outline: IONOESP060 - Instant Offer No ESP Template Test- Validate the response when PostalCode WITH INVALID FORMAT3 value is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message	   										  |
	|61       | 400  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     |POSTALCODEWITHINVALIDFORMAT3    |	
	
	

@smokeTest
Scenario Outline: IONOESP061 - Instant Offer No ESP Template Test- Validate the response when province Exceeding Max Length is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message					   						|
	|62       | 400  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     |PROVINCEEXCEEDINGMAXLENGTH    |

	

@smokeTest
Scenario Outline: IONOESP062 - Instant Offer No ESP Template Test- Validate the response when province with AlphaNumeric is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message					      					 |
	|63       | 400  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     |PROVINCEACCEPTINGALPHANUMERIC    |
	


@smokeTest
Scenario Outline: IONOESP063 - Instant Offer No ESP Template Test- Validate the response when province With SpecialCharacters is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message					  			   					  |
	|64       | 400  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     |PROVINCEACCEPTINGSPECIALCHARACTERS    |
		

@smokeTest
Scenario Outline: IONOESP064 - Instant Offer No ESP Template Test- Validate the response when province with Invalid value value is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message					  	  			|
	|65       | 400  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     |PROVINCEWITHINVALIDVALUE    |
	
	

@smokeTest
Scenario Outline: IONOESP065 - Instant Offer No ESP Template Test- Validate the response when State Exceeding Max Length is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message					  				 |
	|66       | 400  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     |STATEEXCEEDINGMAXLENGTH    |

	

@smokeTest
Scenario Outline: IONOESP066 - Instant Offer No ESP Template Test- Validate the response when state With LessThan Min Length is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message					      				|
	|67       | 400  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     |STATEWITHLESSTHANMINLENGTH    |
	


@smokeTest
Scenario Outline: IONOESP067 - Instant Offer No ESP Template Test- Validate the response when state With Non Capital Valid Value is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message					  			    		 |
	|68       | 400  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     |STATEWITHNONCAPITALVALIDVALUE    |
		


@smokeTest
Scenario Outline: IONOESP068 - Instant Offer No ESP Template Test- Validate the response when state With Special Characters is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  							   |Message					  	  				|
	|69       | 400  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     |STATEWITHSPECIALCHARACTERS    |
	

@smokeTest
Scenario Outline: IONOESP069 - Instant Offer No ESP Template Test- Validate the response when state With AlphaNumeric is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					 									 |Message	  							 |
	|70       | 400  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     |STATEWITHALPHANUMERIC    |	
	

@smokeTest
Scenario Outline: IONOESP070 - Instant Offer No ESP Template Test- Validate the response when PostalCode having Single Hyphen InValidFormat value is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message    							 |
	|71       | 400  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     |STATEWITHNUMERICVALUE    |	
		
		
	

@smokeTest
Scenario Outline: IONOESP071 - Instant Offer No ESP Template Test- Validate the response when Title Exceeding Max Length is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  							   |Message					   				 |
	|72       | 400  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     |TITLEEXCEEDINGMAXLENGTH    |

	

@smokeTest
Scenario Outline: IONOESP072 - Instant Offer No ESP Template Test- Validate the response when title Accepting Alphanumeric is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message    |
	|73       | 200  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     |success    |
	


@smokeTest
Scenario Outline: IONOESP073 - Instant Offer No ESP Template Test- Validate the response when title Accepting Special Characters is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message	   |
	|74       | 200  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     |success    |
		

	

@smokeTest
Scenario Outline: IONOESP074 - Instant Offer No ESP Template Test- Validate the response when zip Exceeding Max Length is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message					   			 |
	|75       | 400  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     |ZIPEXCEEDINGMAXLENGTH    |

	

@smokeTest
Scenario Outline: IONOESP075 - Instant Offer No ESP Template Test- Validate the response when zip With Less Than MaxLength is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message					      			|
	|76       | 400  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     |ZIPWITHLESSTHANMAXLENGTH    |
	

@smokeTest
Scenario Outline: IONOESP076 - Instant Offer No ESP Template Test- Validate the response when zip With CharAndDigit InValid Value is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:		
	|Rowindex | code |SheetPath										|SheetName					 									 |Message					  			   				 |
	|77       | 400  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     |ZIPWITHCHARANDDIGITINVALIDVALUE    |
		


@smokeTest
Scenario Outline: IONOESP077 - Instant Offer No ESP Template Test- Validate the response when zip With Special Characters is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath								|SheetName					 								   |Message					  	  			|
	|78       | 400  |SheetPath_InstantOfferNoESP  |SheetName_InstantOfferNoESP_Attributes     |ZIPWITHSPECIALCHARACTERS    |
	


@smokeTest
Scenario Outline: IONOESP078 - Instant Offer No ESP Template Test- Validate the response when zip With AlphaNumeric is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					 									 |Message	   						 |
	|79       | 400  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     |ZIPWITHALPHANUMERIC    |	
	
	

@smokeTest
Scenario Outline: IONOESP079 - Instant Offer No ESP Template Test- Validate the response when zip With Numeric Value  is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message   						 |
	|80       | 400  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     |ZIPWITHNUMERICVALUE    |	
		
		
		

@smokeTest
Scenario Outline: IONOESP080 - Instant Offer No ESP Template Test- Validate the response when zip With Numeric And Char Value  is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					 								   |Message    									  |
	|81       | 400  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     |ZIPWITHNUMERICANDCHARVALUE    |	
			
	
@smokeTest
Scenario Outline: IONOESP081 - Instant Offer No ESP Template Test- Validate the response when zip With 10Numeric Value  is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message    |
	|82       | 200  |SheetPath_InstantOfferNoESP   		|SheetName_InstantOfferNoESP_Attributes     |success    |	
	

	
	
			
		
		