@instant_offer_functional_suite
Feature: Functional Validations of Instant Offer

  Scenario Outline: Instant Offer DFD 01 : Verify that Mocode is already assigned to another user for new email
    Given User updated the field "email" in workbook "SheetName_InstantOffer" of file "SheetPath_InstantOffer" on row <Rowindex>
    And User updated the field "email" in workbook "SheetName_InstantOffer" of file "SheetPath_InstantOffer" on row <Rowindex>
    And User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
    When When user hit "post" requests
    Then User should received response status <code>
    And User verify "isNewUser" flag of "emailResult" array  is "true" in results
    And User verify "offerStatusCd" flag of "myOffersResult" array  is "580" in results
    And User verify Response Page

    Examples: 
      | Rowindex | code | SheetPath              | SheetName              |
      |        2 |  200 | SheetPath_InstantOffer | SheetName_InstantOffer |

  Scenario Outline: Instant Offer DFD 02 : Verify that offer not get assigned to Spam email if useSpamEmail flag is false
    #Given User updated the field "email" in workbook "SheetName_InstantOffer" of file "SheetPath_InstantOffer" on row <Rowindex>
    Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
    When When user hit "post" requests
    Then User should received response status <code>
    And User verify "isSpam" flag of "emailResult" array  is "true" in results
    And User verify "" flag of "myOffersResult" array  is "null" in results
    And User verify Response Page

    Examples: 
      | Rowindex | code | SheetPath              | SheetName              |
      |        3 |  200 | SheetPath_InstantOffer | SheetName_InstantOffer |
  #BBBCA
  		|23  	   			| 200  |SheetPath_InstantOffer 		|SheetName_InstantOffer |
  		
  Scenario Outline: Instant Offer DFD 03 : Verify that offer not get assigned to undel email if useUndelEmail flag is false
    #Given User updated the field "email" in workbook "SheetName_InstantOffer" of file "SheetPath_InstantOffer" on row <Rowindex>
    Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
    When When user hit "post" requests
    Then User should received response status <code>
    And User verify "isUndel" flag of "emailResult" array  is "true" in results
    And User verify "" flag of "myOffersResult" array  is "null" in results
    And User verify Response Page

    Examples: 
      | Rowindex | code | SheetPath              | SheetName              |
      |        4 |  200 | SheetPath_InstantOffer | SheetName_InstantOffer |
      #BBBCA
      |       26 |  200 | SheetPath_InstantOffer | SheetName_InstantOffer |

  # Add new inactive email of BBBY in test data before executing
  Scenario Outline: Instant Offer DFD 04 : Verify that offer get assigned to inactive email if useLtiEmail flag is true
    Given User updated the field "email" in workbook "SheetName_InstantOffer" of file "SheetPath_InstantOffer" on row <Rowindex>
    Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
    When When user hit "post" requests
    Then User should received response status <code>
    And User verify "isLti" flag of "emailResult" array  is "true" in results
    And User verify "offerStatusCd" flag of "myOffersResult" array  is "0" in results
    And User verify Response Page
    And user is waiting for 10000 seconds
    When When user hit "post" requests
    #commented due to bug from responsys M2M-573
    #And   User verify "isLti" flag of "emailResult" array  is "false" in results
    And User verify "offerStatusCd" flag of "myOffersResult" array  is "1" in results
    And User verify Response Page

    Examples: 
      | Rowindex | code | SheetPath              | SheetName              |
      |        5 |  200 | SheetPath_InstantOffer | SheetName_InstantOffer |
      #BBBCA
      |       21 |  200 | SheetPath_InstantOffer | SheetName_InstantOffer |


  Scenario Outline: Instant Offer DFD 05 : Verify that offer get assigned to new email if usenewEmail flag is true
    Given User updated the field "email" in workbook "SheetName_InstantOffer" of file "SheetPath_InstantOffer" on row <Rowindex>
    And User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
    When When user hit "post" requests
    Then User should received response status <code>
    And User verify "isNewUser" flag of "emailResult" array  is "true" in results
    And User verify "offerStatusCd" flag of "myOffersResult" array  is "0" in results
    And User verify Response Page
    And user is waiting for 10000 seconds
    When When user hit "post" requests
    And User verify "isNewUser" flag of "emailResult" array  is "false" in results
    And User verify "offerStatusCd" flag of "myOffersResult" array  is "1" in results
    And User verify Response Page

    Examples: 
      | Rowindex | code | SheetPath              | SheetName              |
      |        6 |  200 | SheetPath_InstantOffer | SheetName_InstantOffer |
      #BBBCA
      |       19 |  200 | SheetPath_InstantOffer | SheetName_InstantOffer |

  # Add new inactive email of BBBY in test data before executing
   @Fail
  Scenario Outline: Instant Offer DFD 06 : Verify that offer not get assigned to inactive email if useLtiEmail flag is false
    Given User updated the field "email" in workbook "SheetName_InstantOffer" of file "SheetPath_InstantOffer" on row <Rowindex>
    And User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
    When When user hit "post" requests
    Then User should received response status <code>
    And User verify "isNewUser" flag of "emailResult" array  is "false" in results
    And User verify "isLti" flag of "emailResult" array  is "false" in results
    And User verify "" flag of "myOffersResult" array  is "null" in results
   

    Examples: 
      | Rowindex | code | SheetPath              | SheetName              |
      |        7 |  200 | SheetPath_InstantOffer | SheetName_InstantOffer |
      #BBBCA
      |       22 |  200 | SheetPath_InstantOffer | SheetName_InstantOffer |

  # Add optout email of BBBY in test data before executing
  @InstantOffer
  Scenario Outline: Instant Offer DFD 07 : Verify that offer not get assigned to optout email if useOptoutEmail flag is false
    Given User updated the field "email" in workbook "SheetName_InstantOffer" of file "SheetPath_InstantOffer" on row <Rowindex>
    Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
    When When user hit "post" requests
    Then User should received response status <code>
    And User verify "isNewUser" flag of "emailResult" array  is "false" in results
    And User verify "isOptIn" flag of "emailResult" array  is "false" in results
    And User verify "" flag of "myOffersResult" array  is "null" in results
    And User verify Response Page

    Examples: 
      | Rowindex | code | SheetPath              | SheetName              |
      |        8 |  200 | SheetPath_InstantOffer | SheetName_InstantOffer |

  @InstantOffer
  Scenario Outline: Instant Offer DFD 08 : Verify that offer get assigned to optout email if useOptoutEmail flag is true
    Given User updated the field "email" in workbook "SheetName_InstantOffer" of file "SheetPath_InstantOffer" on row <Rowindex>
    Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
    When When user hit "post" requests
    Then User should received response status <code>
    And User verify "isNewUser" flag of "emailResult" array  is "false" in results
    And User verify "isOptIn" flag of "emailResult" array  is "true" in results
    And User verify "offerStatusCd" flag of "myOffersResult" array  is "0" in results
    And user is waiting for 10000 seconds
    When When user hit "post" requests
    And User verify "isOptIn" flag of "emailResult" array  is "false" in results
    And User verify "offerStatusCd" flag of "myOffersResult" array  is "1" in results
    And User verify Response Page

    Examples: 
      | Rowindex | code | SheetPath              | SheetName              |
      |        9 |  200 | SheetPath_InstantOffer | SheetName_InstantOffer |

  #run together test case 09,10, 11 they are dependent on each other
  @InstantOfferNew
  Scenario Outline: Instant Offer DFD 09 : Verify that offer not get assigned to new email if useNewEmail flag is false
    Given User updated the field "email" in workbook "SheetName_InstantOffer" of file "SheetPath_InstantOffer" on row <Rowindex>
    And User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
    When When user hit "post" requests
    Then User should received response status <code>
    And User verify "isNewUser" flag of "emailResult" array  is "true" in results
    And User verify "" flag of "myOffersResult" array  is "null" in results
    And user is waiting for 10000 seconds
    When When user hit "post" requests
    And User verify "isNewUser" flag of "emailResult" array  is "false" in results
    And User verify "" flag of "myOffersResult" array  is "null" in results
    And User verify Response Page

    Examples: 
      | Rowindex | code | SheetPath              | SheetName              |
      |       10 |  200 | SheetPath_InstantOffer | SheetName_InstantOffer |
      #BBBCA
      |       20 |  200 | SheetPath_InstantOffer | SheetName_InstantOffer |

  @InstantOfferNew
  Scenario Outline: Instant Offer DFD 10 : Verify that offer not get assigned to existing email if useExistingEmail flag is false
    Given User updated the field "email" in workbook "SheetName_InstantOffer" of file "SheetPath_InstantOffer" on row <Rowindex>
    And User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
    When When user hit "post" requests
    Then User should received response status <code>
    And User verify "isNewUser" flag of "emailResult" array  is "false" in results
    And User verify "" flag of "myOffersResult" array  is "null" in results
    And User verify Response Page

    Examples: 
      | Rowindex | code | SheetPath              | SheetName              |
      |       11 |  200 | SheetPath_InstantOffer | SheetName_InstantOffer |

  @InstantOfferNew
  Scenario Outline: Instant Offer DFD 11 : Verify that offer get assigned to existing email if useExistingEmail flag is true
    Given User updated the field "email" in workbook "SheetName_InstantOffer" of file "SheetPath_InstantOffer" on row <Rowindex>
    And User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
    When When user hit "post" requests
    Then User should received response status <code>
    And User verify "isNewUser" flag of "emailResult" array  is "false" in results
    And User verify "offerStatusCd" flag of "myOffersResult" array  is "0" in results
    And User verify Response Page

    Examples: 
      | Rowindex | code | SheetPath              | SheetName              |
      |       12 |  200 | SheetPath_InstantOffer | SheetName_InstantOffer |

  Scenario Outline: Instant Offer DFD 12 : Verify that offer not get assigned to new email if usenewEmail flag is true but sourceid:offfer is expired
    Given User updated the field "email" in workbook "SheetName_InstantOffer" of file "SheetPath_InstantOffer" on row <Rowindex>
    And User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
    When When user hit "post" requests
    Then User should received response status <code>
    And User verify "isNewUser" flag of "emailResult" array  is "true" in results
    And User verify "offerStatusCd" flag of "myOffersResult" array  is "615" in results
    And User verify Response Page

    Examples: 
      | Rowindex | code | SheetPath              | SheetName              |
      |       13 |  200 | SheetPath_InstantOffer | SheetName_InstantOffer |

  Scenario Outline: Instant Offer DFD 13 : Verify that offer get assigned to new email if usenewEmail flag is true but sourceid:offfer is multiple
    Given User updated the field "email" in workbook "SheetName_InstantOffer" of file "SheetPath_InstantOffer" on row <Rowindex>
    And User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
    When When user hit "post" requests
    Then User should received response status <code>
    And User verify "isNewUser" flag of "emailResult" array  is "true" in results
    And User verify "offerStatusCd" flag of "myOffersResult" array  is "0" in results
    And User verify Response Page
    And user is waiting for 10000 seconds
    When When user hit "post" requests
    And User verify "isNewUser" flag of "emailResult" array  is "false" in results
    And User verify "offerStatusCd" flag of "myOffersResult" array  is "1" in results
    And User verify Response Page

    Examples: 
      | Rowindex | code | SheetPath              | SheetName              |
      |       14 |  200 | SheetPath_InstantOffer | SheetName_InstantOffer |

  
  Scenario Outline: Instant Offer DFD 14 : Verify that offer not get assigned to new email if useNewEmail flag is true but Mocode is expired
    Given User updated the field "email" in workbook "SheetName_InstantOffer" of file "SheetPath_InstantOffer" on row <Rowindex>
    And User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
    When When user hit "post" requests
    Then User should received response status <code>
    And User verify "isNewUser" flag of "emailResult" array  is "true" in results
    And User verify "offerStatusCd" flag of "myOffersResult" array  is "580" in results
    And User verify Response Page

    Examples: 
      | Rowindex | code | SheetPath              | SheetName              |
      |       15 |  200 | SheetPath_InstantOffer | SheetName_InstantOffer |

  #required new mocode need to be run seperately.
  @validMOcode
  Scenario Outline: Instant Offer DFD 15 : Verify that offer get assigned to new email if useNewEmail flag is true and Mocode is valid
    Given User updated the field "email" in workbook "SheetName_InstantOffer" of file "SheetPath_InstantOffer" on row <Rowindex>
    And User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
    When When user hit "post" requests
    Then User should received response status <code>
    And User verify "isNewUser" flag of "emailResult" array  is "true" in results
    And User verify "offerStatusCd" flag of "myOffersResult" array  is "0" in results
    And User verify Response Page

    Examples: 
      | Rowindex | code | SheetPath              | SheetName              |
      |       16 |  200 | SheetPath_InstantOffer | SheetName_InstantOffer |

  @rrp
  Scenario Outline: Instant Offer DFD 16 : Verify that offer not get assigned to new email if useNewEmail flag is true but Sourceid is Invalid
    Given User updated the field "email" in workbook "SheetName_InstantOffer" of file "SheetPath_InstantOffer" on row <Rowindex>
    And User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
    When When user hit "post" requests
    Then User should received response status <code>
    And User verify "isNewUser" flag of "emailResult" array  is "true" in results
    And User verify "offerStatusCd" flag of "myOffersResult" array  is "701" in results
    And User verify Response Page

    Examples: 
      | Rowindex | code | SheetPath              | SheetName              |
      |       17 |  200 | SheetPath_InstantOffer | SheetName_InstantOffer |

  Scenario Outline: Instant Offer DFD 17 : Verify that offer not get assigned to new email if useNewEmail flag is true but Mocode/proxycode is Invalid (Proxy date not started)
    Given User updated the field "email" in workbook "SheetName_InstantOffer" of file "SheetPath_InstantOffer" on row <Rowindex>
    And User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
    When When user hit "post" requests
    Then User should received response status <code>
    And User verify "isNewUser" flag of "emailResult" array  is "true" in results
    And User verify "offerStatusCd" flag of "myOffersResult" array  is "132" in results
    And User verify Response Page

    Examples: 
      | Rowindex | code | SheetPath              | SheetName              |
      |       18 |  200 | SheetPath_InstantOffer | SheetName_InstantOffer |

  Scenario Outline: Instant Offer DFD 18 : Verify that offer get assigned to Spam email if useSpamEmail flag is true
    #Given User updated the field "email" in workbook "SheetName_InstantOffer" of file "SheetPath_InstantOffer" on row <Rowindex>
    Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
    When When user hit "post" requests
    Then User should received response status <code>
    And User verify "isSpam" flag of "emailResult" array  is "true" in results
    And User verify "offerStatusCd" flag of "myOffersResult" array  is "0" in results
    And User verify Response Page

    Examples: 
      | Rowindex | code | SheetPath              | SheetName              |
      |       24 |  200 | SheetPath_InstantOffer | SheetName_InstantOffer |

  Scenario Outline: Instant Offer DFD 19 : Verify that offer get assigned to udel email if useUndelEmail flag is true
    #Given User updated the field "email" in workbook "SheetName_InstantOffer" of file "SheetPath_InstantOffer" on row <Rowindex>
    Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
    When When user hit "post" requests
    Then User should received response status <code>
    And User verify "isUndel" flag of "emailResult" array  is "true" in results
    And User verify "offerStatusCd" flag of "myOffersResult" array  is "0" in results
    And User verify Response Page

    Examples: 
      | Rowindex | code | SheetPath              | SheetName              |
      |       25 |  200 | SheetPath_InstantOffer | SheetName_InstantOffer |
