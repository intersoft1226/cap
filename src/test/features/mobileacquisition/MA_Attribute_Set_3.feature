# Mobile Acquisition Field validations covered for Custom fields
Feature: Validating Sign Up services for Field "Custom fields" of template type Mobile Acquisition


@smokeTest
Scenario Outline: MA114 - Mobile Acquisition Template Test- Validate the response when blank custom id of CheckBoxtype Custom field data is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  user hit "post" requests with "blank" "customFieldId" field
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex 	| code		 |SheetPath												|SheetName					  				|Message					     |
	|114        | 400  		 |SheetPath_MobileAcquisition   	|SheetName_MobileAcquisition  |CUSTOMBLANKFIELDID    |
	

@smokeTest
Scenario Outline: MA115 - Mobile Acquisition Template Test- Validate the response when blank custom value of CheckBoxtype Custom field data is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  user hit "post" requests with "blank" "customFieldValue" field
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex 	| code		 |SheetPath												|SheetName					  				|Message					        |
	|115        | 400  		 |SheetPath_MobileAcquisition   	|SheetName_MobileAcquisition  |CUSTOMBLANKFIELDVALUE    |
	


@smokeTest
Scenario Outline: MA116 - Mobile Acquisition Template Test- Validate the response when Invalid custom value of CheckBox type Custom field is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  user hit "post" requests with "invalid" "invalidCustomFieldValue" field
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex 	| code		 |SheetPath												|SheetName					  				|Message			   		        |
	|116        | 400  		 |SheetPath_MobileAcquisition   	|SheetName_MobileAcquisition     |CUSTOMINVALIDFIELDVALUE    |
	
	

@smokeTest
Scenario Outline: MA117 - Mobile Acquisition Template Test- Validate the response when Custom Field with multiple valid values for Checkbox type is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath											|SheetName									     |Message    |
	|117      | 200  |SheetPath_MobileAcquisition   |SheetName_MobileAcquisition     | success   |		
	


@smokeTest
Scenario Outline: MA118 - Mobile Acquisition Template Test- Validate the response when Non Mandatory Blank Custom CheckBox type Field Id is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  user hit "post" requests with "blank" "customFieldId" field
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath											|SheetName									     |Message                      |
	|118      | 400  |SheetPath_MobileAcquisition   |SheetName_MobileAcquisition     | CUSTOMNONMANDATORYBLANKID   |	
	
	

@smokeTest
Scenario Outline: MA119 - Mobile Acquisition Template Test- Validate the response when Non Mandatory Invalid Custom Field Value is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  user hit "post" requests with "invalid" "invalidCustomFieldValue" field
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath											|SheetName									  |Message                      |
	|119      | 400  |SheetPath_MobileAcquisition   |SheetName_MobileAcquisition  | CUSTOMINVALIDFIELDVALUE     |		
		
			


@smokeTest
Scenario Outline: MA120 - Mobile Acquisition Template Test- Validate the response when Non Mandatory Custom Field with multiple valid values is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath											|SheetName									  |Message    |
	|120      | 200  |SheetPath_MobileAcquisition   |SheetName_MobileAcquisition  | success   |		
	

	
@smokeTest
Scenario Outline: MA121 - Mobile Acquisition Template Test- Validate the response when blank custom id of Radio field type Custom field data is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  user hit "post" requests with "blank" "customFieldId" field
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex 	| code		 |SheetPath												|SheetName					  				|Message					     |
	|121        | 400  		 |SheetPath_MobileAcquisition   	|SheetName_MobileAcquisition  |CUSTOMBLANKFIELDID    |
	
	
	
@smokeTest
Scenario Outline: MA122 - Mobile Acquisition Template Test- Validate the response when blank custom value of Radio type Custom field data is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  user hit "post" requests with "blank" "customFieldValue" field
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex 	| code		 |SheetPath												|SheetName					  				|Message					        |
	|122        | 400  		 |SheetPath_MobileAcquisition   	|SheetName_MobileAcquisition  |CUSTOMBLANKFIELDVALUE    |
	


@smokeTest
Scenario Outline: MA123 - Mobile Acquisition Template Test- Validate the response when Invalid custom value of Radio type Custom field is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  user hit "post" requests with "invalid" "invalidCustomFieldValue" field
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex 	| code		 |SheetPath												|SheetName					  				|Message			   		        |
	|123        | 400  		 |SheetPath_MobileAcquisition   	|SheetName_MobileAcquisition  |CUSTOMINVALIDFIELDVALUE    |
	
	

@smokeTest
Scenario Outline: MA124 - Mobile Acquisition Template Test- Validate the response when Custom Field with multiple valid values for Radio Field is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  user hit "post" requests with "multiple" "validCustomFieldValue" field
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath											|SheetName									  |Message                    |
	|124      | 400  |SheetPath_MobileAcquisition   |SheetName_MobileAcquisition  | CUSTOMINVALIDFIELDVALUE   |		
	

@smokeTest
Scenario Outline: MA125 - Mobile Acquisition Template Test- Validate the response when Non Mandatory Blank Custom Radio Field Id is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  user hit "post" requests with "blank" "customFieldId" field
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath											|SheetName									  |Message                      |
	|125      | 400  |SheetPath_MobileAcquisition   |SheetName_MobileAcquisition  | CUSTOMNONMANDATORYBLANKID   |	
	
	

@smokeTest
Scenario Outline: MA126 - Mobile Acquisition Template Test- Validate the response when Non Mandatory Invalid Radio type Custom Field Value is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  user hit "post" requests with "invalid" "invalidCustomFieldValue" field
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath											|SheetName									  |Message                      |
	|126      | 400  |SheetPath_MobileAcquisition   |SheetName_MobileAcquisition  | CUSTOMINVALIDFIELDVALUE     |		
	
	

@smokeTest
Scenario Outline: MA127 - Mobile Acquisition Template Test- Validate the response when Non Mandatory Custom Radio Field with multiple valid values is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  user hit "post" requests with "multiple" "validCustomFieldValue" field
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath											|SheetName									  |Message                    |
	|127      | 400  |SheetPath_MobileAcquisition   |SheetName_MobileAcquisition  | CUSTOMINVALIDFIELDVALUE   |		
	
	
	

@smokeTest
Scenario Outline: MA128 - Mobile Acquisition Template Test- Validate the response when blank custom id of DropDown field type Custom field data is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  user hit "post" requests with "blank" "customFieldId" field
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex 	| code		 |SheetPath												|SheetName					  				|Message					     |
	|128        | 400  		 |SheetPath_MobileAcquisition   	|SheetName_MobileAcquisition  |CUSTOMBLANKFIELDID    |
	
	
	
@smokeTest
Scenario Outline: MA129 - Mobile Acquisition Template Test- Validate the response when blank custom value of DropDown type Custom field data is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  user hit "post" requests with "blank" "customFieldValue" field
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex 	| code		 |SheetPath												|SheetName					  				|Message					        |
	|129        | 400  		 |SheetPath_MobileAcquisition   	|SheetName_MobileAcquisition  |CUSTOMBLANKFIELDVALUE    |
	

	
@smokeTest
Scenario Outline: MA130 - Mobile Acquisition Template Test- Validate the response when Invalid custom value of DropDown type Custom field is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  user hit "post" requests with "invalid" "invalidCustomFieldValue" field
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex 	| code		 |SheetPath												|SheetName					  				|Message			   		        |
	|130        | 400  		 |SheetPath_MobileAcquisition   	|SheetName_MobileAcquisition  |CUSTOMINVALIDFIELDVALUE    |
	
	

@smokeTest
Scenario Outline: MA131 - Mobile Acquisition Template Test- Validate the response when Custom Field with multiple valid values for DropDown Field is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  user hit "post" requests with "multiple" "validCustomFieldValue" field
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath											|SheetName											  | Message                   |
	|131      | 400  |SheetPath_MobileAcquisition   |SheetName_MobileAcquisition      | CUSTOMINVALIDFIELDVALUE   |		
	

@smokeTest
Scenario Outline: MA132 - Mobile Acquisition Template Test- Validate the response when Non Mandatory Blank Custom DropDown Field Id is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  user hit "post" requests with "blank" "customFieldId" field
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath											|SheetName	    								  |Message                      |
	|132      | 400  |SheetPath_MobileAcquisition   |SheetName_MobileAcquisition      | CUSTOMNONMANDATORYBLANKID   |	
	
	

@smokeTest
Scenario Outline: MA133 - Mobile Acquisition Template Test- Validate the response when Non Mandatory Invalid DropDown type Custom Field Value is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  user hit "post" requests with "invalid" "invalidCustomFieldValue" field
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath											|SheetName									  |Message                      |
	|133      | 400  |SheetPath_MobileAcquisition   |SheetName_MobileAcquisition  | CUSTOMINVALIDFIELDVALUE     |		
	
	

@smokeTest
Scenario Outline: MA134 - Mobile Acquisition Template Test- Validate the response when Non Mandatory Custom DropDown Field with multiple valid values is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  user hit "post" requests with "multiple" "validCustomFieldValue" field
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath											|SheetName									  |Message                    |
	|134      | 400  |SheetPath_MobileAcquisition   |SheetName_MobileAcquisition  | CUSTOMINVALIDFIELDVALUE   |		
		