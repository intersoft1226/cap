# Mobile Acquisition Field validations covered for fields - Title, PostalCode, 
# Province, State, Phone, MOCode, FirstName, LastName, EmailConsent, Email, City, 
# BounceXFields, BabyField, Address1, Address2, MobilePhone, Zipcode
Feature: Validating Field Validation for Sign Up services of Mobile Acquisition template type. 


@smokeTest
Scenario Outline: MA007 - Mobile Acquisition TemplateTest- Validate the response when Blank Mobile Phone is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath												|SheetName					  				|Message			    		 |
	|7        | 400  |SheetPath_MobileAcquisition   	|SheetName_MobileAcquisition  |MOBILEPHONEREQUIRED2  |
	

@smokeTest
Scenario Outline: MA008 - Mobile Acquisition TemplateTest- Validate the response when Invalid Mobile Phone is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath												|SheetName					  				|Message					  		 					   |
	|8        | 400  |SheetPath_MobileAcquisition   	|SheetName_MobileAcquisition  | MOBILEPHONEWITHLESSTHANMINLENGTH   |
	
	


@smokeTest
Scenario Outline: MA009 - Mobile Acquisition Template Test- Validate the response when Mobile Phone Exceeding Max Length is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath													|SheetName					  					 |Message					  	    				 |
	|9        | 400  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition     |MOBILEPHONEEXCEEDINGMAXLENGTH    |

	

@smokeTest
Scenario Outline: MA010 - Mobile Acquisition Template Test- Validate the response when Mobile Phone having Two Hypens Invalid Format is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath					      		|SheetName					  					 |Message						 					  			    |
	|10       | 400  |SheetPath_MobileAcquisition |SheetName_MobileAcquisition     |MOBILEPHONEWITHTWOHYPENSINVALIDFORMAT   |
		

@smokeTest
Scenario Outline: MA011 - Mobile Acquisition Template Test- Validate the response when Mobile Phone having AlphaNumeric value is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath	            						|SheetName					             |Message					  	           |
	|11       | 400  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition     |MOBILEPHONEWITHALPHANUMERIC    |


	
@smokeTest
Scenario Outline: MA012 - Mobile Acquisition Template Test- Validate the response when Mobile Phone having 12Digit value is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath													|SheetName					  					 |Message									  |
	|12       | 400  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition     |MOBILEPHONEWITH12DIGIT    |
	

@smokeTest
Scenario Outline: MA013 - Mobile Acquisition Template Test- Validate the response when address1 with Exceeding Max Length is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath												  |SheetName					  					 |Message					    				  |
	|13       | 400  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition     |ADDRESS1EXCEEDINGMAXLENGTH    |
	


@smokeTest
Scenario Outline: MA014 - Mobile Acquisition Template Test- Validate the response when address1 with Alphanumeric is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath													|SheetName					  					 |Message  |
	|14       | 200  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition     | success |
	


@smokeTest
Scenario Outline: MA015 - Mobile Acquisition Template Test- Validate the response when address1 with SpecialCharacters is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath											|SheetName									     |Message    |
	|15       | 200  |SheetPath_MobileAcquisition   |SheetName_MobileAcquisition     | success   |	
	


@smokeTest
Scenario Outline: MA016 - Mobile Acquisition Template Test- Validate the response when address2 with Exceeding Max Length is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath												|SheetName									     |Message					    				 |
	|16       | 400  |SheetPath_MobileAcquisition   	|SheetName_MobileAcquisition     |ADDRESS2EXCEEDINGMAXLENGTH   |
	


@smokeTest
Scenario Outline: MA017 - Mobile Acquisition Template Test- Validate the response when address2 with Alphanumeric is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath											   	|SheetName					 						 |Message  |
	|17       | 200  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition     | success |
	


@smokeTest
Scenario Outline: MA018 - Mobile Acquisition Template Test- Validate the response when address2 with SpecialCharacters is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath													|SheetName					 						 |Message    |
	|18       | 200  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition     | success   |	


	

@smokeTest
Scenario Outline: MA019 - Mobile Acquisition Template Test- Validate the response when babyField Exceeding MaxValue is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath													|SheetName					  					 |Message                       |
	|19       | 400  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition     | BABYFIELDEXCEEDINGMAXVALUE   |	
	
	
			
@smokeTest
Scenario Outline: MA020 - Mobile Acquisition Template Test- Validate the response when babyField With LessThan MinValue is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath													|SheetName					 						 |Message                          |
	|20       | 400  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition     | BABYFIELDWITHLESSTHANMINVALUE   |
	
# This will be valid if Baby Field is non mandatory		
#@smokeTest
#@currentTest1
#Scenario Outline: MA021 - Mobile Acquisition Template Test- Validate the response when babyField Accepting Zero is sent in request.
#	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
#	When  When user hit "post" requests
#	Then  User should received response status <code>
#	And   User validated the response <Message> received
#	
#	Examples:	
#	|Rowindex | code |SheetPath							|SheetName					  |Message    |
#	|21       | 200  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition     | success   |	
#	
#@currentTest		
#@smokeTest
#Scenario Outline: MA022 - Mobile Acquisition Template Test- Validate the response when babyField Count Less Than ChildrenCount is sent in request.
#	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
#	When  user hit "post" requests with BabyField less than children count
#	Then  User should received response status <code>
#	And   User validated the response <Message> received
#	
#	Examples:	
#	|Rowindex | code |SheetPath							|SheetName					  |Message                                |
#	|22       | 200  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition     | BABYFIELDCOUNTLESSTHANCHILDRENCOUNT   |		
	
	
@smokeTest
Scenario Outline: MA023 - Mobile Acquisition Template Test- Validate the response when CmpDeploymentStrategy field exceeds MaxLength is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath													|SheetName					  					 |Message                           |
	|23       | 400  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition     | CMPDEPLOYMENTSTRATEGYMAXLENGTH   |		
	
	
		
@smokeTest
Scenario Outline: MA024 - Mobile Acquisition Template Test- Validate the response when CmpNameMaxLength field exceeds MaxLength is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath													|SheetName									  	  |Message             |
	|24       | 400  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition      | CMPNAMEMAXLENGTH   |	
	
			
@smokeTest
Scenario Outline: MA025 - Mobile Acquisition Template Test- Validate the response when CmpVariationID field exceeds MaxLength is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath													|SheetName					 						 |Message                    |
	|25       | 400  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition     | CMPVARIATIONIDMAXLENGTH   |	
	
	
	
		
@smokeTest
Scenario Outline: MA026 - Mobile Acquisition Template Test- Validate the response when CmpConcept field exceeds MaxLength is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath													|SheetName										   |Message                |
	|26       | 400  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition     | CMPCONCEPTMAXLENGTH   |	
	
	

@smokeTest
Scenario Outline: MA027 - Mobile Acquisition Template Test- Validate the response when CmpDevice field exceeds MaxLength is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath													|SheetName					  					 |Message               |
	|27       | 400  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition     | CMPDEVICEMAXLENGTH   |	
	
	

@smokeTest
Scenario Outline: MA028 - Mobile Acquisition Template Test- Validate the response when CmpFormat field exceeds MaxLength is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath													|SheetName										   |Message               |
	|28       | 400  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition     | CMPFORMATMAXLENGTH   |		
	
	

@smokeTest
Scenario Outline: MA029 - Mobile Acquisition Template Test- Validate the response when City field exceeds MaxLength is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							 						|SheetName					  					 |Message                   |
	|29       |400   |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition     | CITYEXCEEDINGMAXLENGTH   |		
	
	
			
@smokeTest
Scenario Outline: MA030 - Mobile Acquisition Template Test- Validate the response when City Accepting Alphanumeric is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath													|SheetName					  					 |Message    |
	|30       | 200  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition     | success   |		
	
	
@smokeTest
Scenario Outline: MA031- Mobile Acquisition Template Test- Validate the response when City Accepting SpecialCharacters is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath													|SheetName					  					 |Message    |
	|31       | 200  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition     | success   |		
	
		
@smokeTest
Scenario Outline: MA032 - Mobile Acquisition Template Test- Validate the response when email Without At The Rate is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath													|SheetName					  					 |Message                  |
	|32       | 400  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition     | EMAILWITHOUTATTHERATE   |	
	
		
@smokeTest
Scenario Outline: MA033 - Mobile Acquisition Template Test- Validate the response when email Without At Dot is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath													|SheetName					  					 |Message            |
	|33       | 400  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition     | EMAILWITHOUTDOT   |	
	
	
	
			
@smokeTest
Scenario Outline: MA034 - Mobile Acquisition Template Test- Validate the response when email With No Prefix before At The Rate is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath													|SheetName											 |Message                             |
	|34       | 400  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition     | EMAILWITHNOPREFIXBEFOREATTHERATE   |
	
	
			
@smokeTest
Scenario Outline: MA035 - Mobile Acquisition Template Test- Validate the response when email With exceeding Max length is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath													|SheetName							    		 |Message                               |
	|35       | 400  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition     | EMAILWITHEMAILIDEXCEEDINGMAXLENGTH   |	
	
	
@smokeTest
Scenario Outline: MA036 - Mobile Acquisition Template Test- Validate the response when email With Multiple At the rate is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath												 	|SheetName									 	   |Message                               |
	|36       | 400  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition     | EMAILWITHMULTIPLEATTHERATEINEMAIL    |	
	
	
@smokeTest
Scenario Outline: MA037 - Mobile Acquisition Template Test- Validate the response when email With SpecialCharacters is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath													|SheetName					  					 |Message                        |
	|37       | 400  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition     | EMAILWITHSPECIALCHARACTERS    |		
	
		
@smokeTest
Scenario Outline: MA038 - Mobile Acquisition Template Test- Validate the response when email With Spaces is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath													|SheetName					  					 |Message            |
	|38       | 400  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition     | EMAILWITHSPACE    |	
	

	
@smokeTest
Scenario Outline: MA039 - Mobile Acquisition Template Test- Validate the response when email With Leading Spaces is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath									  			|SheetName								  		  |Message     |
	|39       | 200  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition      | success    |	
	
		
@smokeTest
Scenario Outline: MA040 - Mobile Acquisition Template Test- Validate the response when email With Trailing Spaces is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath												  |SheetName					  					 |Message     |
	|40       | 200  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition     | success    |	
	
		
@smokeTest
Scenario Outline: MA041 - Mobile Acquisition Template Test- Validate the response when EMAILCONSENT2MANDATORY is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath													|SheetName											  |Message     					       |
	|41       | 400  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition      | EMAILCONSENT2MANDATORY     |							
	


@smokeTest
Scenario Outline: MA042 - Mobile Acquisition Template Test- Validate the response when FirstName with Exceeding Max Length is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex  | code |SheetPath												|SheetName											  |Message									      |
	|42        | 400  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition      |FIRSTNAMEEXCEEDINGMAXLENGTH    |
	


@smokeTest
Scenario Outline: MA043 - Mobile Acquisition Template Test- Validate the response when FirstName with Alphanumeric is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath													|SheetName											  |Message  |
	|43       | 200  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition      | success |
	


@smokeTest
Scenario Outline: MA044 - Mobile Acquisition Template Test- Validate the response when FirstName with SpecialCharacters is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath													|SheetName					 						 |Message    |
	|44       | 200  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition     | success   |	
	
	
						
@smokeTest
Scenario Outline: MA045 - Mobile Acquisition Template Test- Validate the response when LastName with Exceeding Max Length is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath													|SheetName								  		  |Message									      |
	|45       | 400  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition      |LASTNAMEEXCEEDINGMAXLENGTH     |
	


@smokeTest
Scenario Outline: MA046 - Mobile Acquisition Template Test- Validate the response when LastName with Alphanumeric is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath												  |SheetName								       |Message  |
	|46       | 200  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition     | success |
	


@smokeTest
Scenario Outline: MA047 - Mobile Acquisition Template Test- Validate the response when LastName with SpecialCharacters is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath													|SheetName								  		 |Message    |
	|47       | 200  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition     | success   |	
	
	

@smokeTest
Scenario Outline: MA048 - Mobile Acquisition Template Test- Validate the response when MoCode Exceeding Max Length is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath													|SheetName					 						 |Message					   					|
	|48       | 400  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition     |MOCODEEXCEEDINGMAXLENGTH    |

	

@smokeTest
Scenario Outline: MA049 - Mobile Acquisition Template Test- Validate the response when MOCode less than Min Length is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath													|SheetName					  					 |Message					    				   |
	|49       | 400  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition     |MOCODELESSTHANMINIMUMLENGTH    |
	

@smokeTest
Scenario Outline: MA050 - Mobile Acquisition Template Test- Validate the response when MO Code Alphanumeric is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath													|SheetName					 						 |Message		 |
	|50       | 200  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition     |success    |
		

@smokeTest
Scenario Outline: MA051 - Mobile Acquisition Template Test- Validate the response when MOCode having SpecialCharacters value is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath													|SheetName					  					 |Message					  	 					 |
	|51       | 400  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition     |MOCODEWITHSPECIALCHARACTERS    |
	

@smokeTest
Scenario Outline: MA052 - Mobile Acquisition Template Test- Validate the response when MOCode having HyphenValidFormat value is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath													|SheetName					  					 |Message	   |
	|52       | 200  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition     |success    |	
	

@smokeTest
Scenario Outline: MA053 - Mobile Acquisition Template Test- Validate the response when MOCode having Hyphen InValidFormat value is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath													|SheetName					  					 |Message	                        |
	|53       | 400  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition     |MOCODEWITHHYPHENINVALIDFORMAT   |	
		
#Mo Code ends	


@smokeTest
Scenario Outline: MA054 - Mobile Acquisition Template Test- Validate the response when Phone Exceeding Max Length is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath													|SheetName											 |Message									   |
	|54       | 400  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition     |PHONEEXCEEDINGMAXLENGTH    |

	

@smokeTest
Scenario Outline: MA055 - Mobile Acquisition Template Test- Validate the response when Phone less than Min Length is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath											  	|SheetName										   |Message					     					|
	|55       | 400  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition     |PHONEWITHLESSTHANMINLENGTH    |
	

@smokeTest
Scenario Outline: MA056 - Mobile Acquisition Template Test- Validate the response when phone With Two Hypens InvalidFormat is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath													|SheetName					 						 |Message					  							   |
	|56       | 400  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition     |PHONEWITHTWOHYPENSINVALIDFORMAT    |
		

@smokeTest
Scenario Outline: MA057 - Mobile Acquisition Template Test- Validate the response when Phone having AlphaNumeric value is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath											  	|SheetName										   |Message					  	     |
	|57       | 400  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition     |PHONEWITHALPHANUMERIC    |
	

@smokeTest
Scenario Outline: MA058 - Mobile Acquisition Template Test- Validate the response when Phone having 12Digit value is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath													|SheetName					 						 |Message	 					  |
	|58       | 400  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition     |PHONEWITH12DIGIT    |	
	

@smokeTest
Scenario Outline: MA059 - Mobile Acquisition Template Test- Validate the response when Phone having Single Hyphen InValidFormat value is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath													|SheetName					  						|Message    |
	|59       | 200  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition      |success    |	
		
		

@smokeTest
Scenario Outline: MA060 - Mobile Acquisition Template Test- Validate the response when PostalCode Exceeding Max Length is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath												  |SheetName					  					 |Message											    |
	|60       | 400  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition     |POSTALCODEEXCEEDINGMAXLENGTH    |

	

@smokeTest
Scenario Outline: MA061 - Mobile Acquisition Template Test- Validate the response when PostalCode with Invalid Format is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										  		|SheetName										   |Message					   				     |
	|61       | 400  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition     |POSTALCODEWITHINVALIDFORMAT    |
	


@smokeTest
Scenario Outline: MA062 - Mobile Acquisition Template Test- Validate the response when PostalCode With SpecialCharacters is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							    				|SheetName										   |Message					  			  		    |
	|62       | 400  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition     |POSTALCODEWITHSPECIALCHARACTER    |
		


@smokeTest
Scenario Outline: MA063 - Mobile Acquisition Template Test- Validate the response when PostalCode with Invalid Format2 value is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath												  |SheetName											 |Message										  	  |
	|63       | 400  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition     |POSTALCODEWITHINVALIDFORMAT2    |
	

@smokeTest
Scenario Outline: MA064 - Mobile Acquisition Template Test- Validate the response when PostalCode WITH INVALID FORMAT3 value is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath													|SheetName										   |Message	 											  |
	|64       | 400  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition     |POSTALCODEWITHINVALIDFORMAT3    |	
	
	

@smokeTest
Scenario Outline: MA065 - Mobile Acquisition Template Test- Validate the response when province Exceeding Max Length is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:		
	|Rowindex | code |SheetPath													|SheetName										   |Message								   	   |
	|65       | 400  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition     |PROVINCEEXCEEDINGMAXLENGTH   |

	

@smokeTest
Scenario Outline: MA066 - Mobile Acquisition Template Test- Validate the response when province with AlphaNumeric is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath													|SheetName					  					 |Message					       					 |
	|66       | 400  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition     |PROVINCEACCEPTINGALPHANUMERIC    |
	


@smokeTest
Scenario Outline: MA067 - Mobile Acquisition Template Test- Validate the response when province With SpecialCharacters is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath													|SheetName										   |Message						  			  			    |
	|67       | 400  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition     |PROVINCEACCEPTINGSPECIALCHARACTERS    |
		

@smokeTest
Scenario Outline: MA068 - Mobile Acquisition Template Test- Validate the response when province with Invalid value value is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath													|SheetName											 |Message							    	  |
	|68       | 400  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition     |PROVINCEWITHINVALIDVALUE    |
	
	

@smokeTest
Scenario Outline: MA069 - Mobile Acquisition Template Test- Validate the response when State Exceeding Max Length is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath												  |SheetName					   					 |Message					   				 |
	|69       | 400  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition     |STATEEXCEEDINGMAXLENGTH    |

	

@smokeTest
Scenario Outline: MA070 - Mobile Acquisition Template Test- Validate the response when state With LessThan Min Length is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath													|SheetName					  					 |Message					    			    |
	|70       | 400  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition     |STATEWITHLESSTHANMINLENGTH    |
	


@smokeTest
Scenario Outline: MA071 - Mobile Acquisition Template Test- Validate the response when state With Non Capital Valid Value is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath													|SheetName									     |Message					  			         |
	|71       | 400  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition     |STATEWITHNONCAPITALVALIDVALUE    |
		


@smokeTest
Scenario Outline: MA072 - Mobile Acquisition Template Test- Validate the response when state With Special Characters is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath													|SheetName											  |Message							      	  |
	|72       | 400  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition      |STATEWITHSPECIALCHARACTERS     |
	

@smokeTest
Scenario Outline: MA073 - Mobile Acquisition Template Test- Validate the response when state With AlphaNumeric is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath													|SheetName					  					 |Message	   							 |
	|73       | 400  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition     |STATEWITHALPHANUMERIC    |	
	

@smokeTest
Scenario Outline: MA074 - Mobile Acquisition Template Test- Validate the response when State with Numeric value is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath												  |SheetName									  	  |Message  						    |
	|74       | 400  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition      |STATEWITHNUMERICVALUE    |	
		
		
	

@smokeTest
Scenario Outline: MA075 - Mobile Acquisition Template Test- Validate the response when Title Exceeding Max Length is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath											  	|SheetName					 					   |Message					  				 |
	|75       | 400  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition     |TITLEEXCEEDINGMAXLENGTH    |

	

@smokeTest
Scenario Outline: MA076 - Mobile Acquisition Template Test- Validate the response when title Accepting Alphanumeric is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath									  			|SheetName										   |Message		 |
	|76       | 200  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition     |success    |
	


@smokeTest
Scenario Outline: MA077 - Mobile Acquisition Template Test- Validate the response when title Accepting Special Characters is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath									  			|SheetName										   |Message		 |
	|77       | 200  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition     |success    |
		

	

@smokeTest
Scenario Outline: MA078 - Mobile Acquisition Template Test- Validate the response when zip Exceeding Max Length is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath													|SheetName									  	  |Message								   |
	|78       | 400  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition      |ZIPEXCEEDINGMAXLENGTH     |

	

@smokeTest
Scenario Outline: MA079 - Mobile Acquisition Template Test- Validate the response when zip With Less Than MaxLength is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath									  			|SheetName									  	  |Message					     			 |
	|79       | 400  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition      |ZIPWITHLESSTHANMAXLENGTH    |
	

@smokeTest
Scenario Outline: MA080 - Mobile Acquisition Template Test- Validate the response when zip With CharAndDigit InValid Value is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath													|SheetName						  				  |Message					  						    |
	|80       | 400  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition      |ZIPWITHCHARANDDIGITINVALIDVALUE    |
		


@smokeTest
Scenario Outline: MA081 - Mobile Acquisition Template Test- Validate the response when zip With Special Characters is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath											  	|SheetName									  	  |Message					 				 	  |
	|81       | 400  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition      |ZIPWITHSPECIALCHARACTERS     |
	


@smokeTest
Scenario Outline: MA082 - Mobile Acquisition Template Test- Validate the response when zip With AlphaNumeric is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath											  	|SheetName					 						 |Message	 						   |
	|82       | 400  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition     |ZIPWITHALPHANUMERIC    |	
	
	

@smokeTest
Scenario Outline: MA083 - Mobile Acquisition Template Test- Validate the response when zip With Numeric Value  is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath													|SheetName					 						 |Message							   |
	|83       | 400  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition     |ZIPWITHNUMERICVALUE    |	
		
		
		

@smokeTest
Scenario Outline: MA084 - Mobile Acquisition Template Test- Validate the response when zip With Numeric And Char Value  is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							  				  |SheetName										   |Message   										|
	|84       | 400  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition     |ZIPWITHNUMERICANDCHARVALUE    |	
			

@smokeTest
Scenario Outline: MA085 - Mobile Acquisition Template Test- Validate the response when zip With 10Numeric Value  is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath													|SheetName									  	  |Message    |
	|85       | 200  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition      |success    |	
	

	
	
			
		
		