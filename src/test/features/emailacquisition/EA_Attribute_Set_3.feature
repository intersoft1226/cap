# Email Acquisition Field validations covered for Custom fields
Feature: Validating Sign Up services for Field "Custom fields" of template type Email Acquisition


@smokeTest
Scenario Outline: EA110 - Email Acquisition Template Test- Validate the response when blank custom id of CheckBoxtype Custom field data is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  user hit "post" requests with "blank" "customFieldId" field
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex 	| code		 |SheetPath											|SheetName					    			  			 |Message					     |
	|111        | 400  		 |SheetPath_EmailAcquistion   	|SheetName_EmailAcquistion_Attributes  |CUSTOMBLANKFIELDID   |
	

@smokeTest
Scenario Outline: EA111 - Email Acquisition Template Test- Validate the response when blank custom value of CheckBoxtype Custom field data is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  user hit "post" requests with "blank" "customFieldValue" field
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex 	| code		 |SheetPath												|SheetName					  				|Message					        |
	|112        | 400  		 |SheetPath_EmailAcquistion   	|SheetName_EmailAcquistion_Attributes  |CUSTOMBLANKFIELDVALUE    |
	


@smokeTest
Scenario Outline: EA112 - Email Acquisition Template Test- Validate the response when Invalid custom value of CheckBox type Custom field is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  user hit "post" requests with "invalid" "invalidCustomFieldValue" field
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex 	| code		 |SheetPath												|SheetName					  				|Message			   		        |
	|113        | 400  		 |SheetPath_EmailAcquistion   	|SheetName_EmailAcquistion_Attributes     |CUSTOMINVALIDFIELDVALUE    |
	
	

@smokeTest
Scenario Outline: EA113 - Email Acquisition Template Test- Validate the response when Custom Field with multiple valid values for Checkbox type is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath											|SheetName									     |Message    |
	|114      | 200  |SheetPath_EmailAcquistion   |SheetName_EmailAcquistion_Attributes     | success   |		
	


@smokeTest
Scenario Outline: EA114 - Email Acquisition Template Test- Validate the response when Non mandatory Blank Custom CheckBox type Field Id is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  user hit "post" requests with "blank" "customFieldId" field
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName									     				  |Message               				|
	|115      | 400  |SheetPath_EmailAcquistion   |SheetName_EmailAcquistion_Attributes     | CUSTOMNONMANDATORYBLANKID   |	
	
	

@smokeTest
Scenario Outline: EA115 - Email Acquisition Template Test- Validate the response when Non EAndatory Invalid Custom Field Value is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  user hit "post" requests with "invalid" "invalidCustomFieldValue" field
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath											|SheetName									  |Message                      |
	|116      | 400  |SheetPath_EmailAcquistion   |SheetName_EmailAcquistion_Attributes  | CUSTOMINVALIDFIELDVALUE     |		
		
			


@smokeTest
Scenario Outline: EA116 - Email Acquisition Template Test- Validate the response when Non Mandatory Custom Field with multiple valid values is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath											|SheetName									  				 |Message    |
	|117      | 200  |SheetPath_EmailAcquistion   	|SheetName_EmailAcquistion_Attributes  | success   |		
	

	
@smokeTest
Scenario Outline: EA117 - Email Acquisition Template Test- Validate the response when blank custom id of Radio field type Custom field data is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  user hit "post" requests with "blank" "customFieldId" field
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex 	| code		 |SheetPath												|SheetName					  								 |Message					     |
	|118        | 400  		 |SheetPath_EmailAcquistion   	  |SheetName_EmailAcquistion_Attributes  |CUSTOMBLANKFIELDID   |
	
	
	
@smokeTest
Scenario Outline: EA118 - Email Acquisition Template Test- Validate the response when blank custom value of Radio type Custom field data is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  user hit "post" requests with "blank" "customFieldValue" field
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex 	| code		 |SheetPath											|SheetName					  								 |Message					        |
	|119        | 400  		 |SheetPath_EmailAcquistion   	|SheetName_EmailAcquistion_Attributes  |CUSTOMBLANKFIELDVALUE   |
	


@smokeTest
Scenario Outline: EA119 - Email Acquisition Template Test- Validate the response when Invalid custom value of Radio type Custom field is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  user hit "post" requests with "invalid" "invalidCustomFieldValue" field
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex 	| code		 |SheetPath											|SheetName					  								 |Message			   		        |
	|120        | 400  		 |SheetPath_EmailAcquistion   	|SheetName_EmailAcquistion_Attributes  |CUSTOMINVALIDFIELDVALUE   |
	
	

@smokeTest
Scenario Outline: EA120 - Email Acquisition Template Test- Validate the response when Custom Field with multiple valid values for Radio Field is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  user hit "post" requests with "multiple" "validCustomFieldValue" field
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName									  				 |Message                    |
	|121      | 400  |SheetPath_EmailAcquistion   |SheetName_EmailAcquistion_Attributes  | CUSTOMINVALIDFIELDVALUE   |		
	

@smokeTest
Scenario Outline: EA121 - Email Acquisition Template Test- Validate the response when Non Mandatory Blank Custom Radio Field Id is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  user hit "post" requests with "blank" "customFieldId" field
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName									  				 |Message                      |
	|122      | 400  |SheetPath_EmailAcquistion   |SheetName_EmailAcquistion_Attributes  | CUSTOMNONMANDATORYBLANKID   |	
	
	

@smokeTest
Scenario Outline: EA122 - Email Acquisition Template Test- Validate the response when Non Mandatory Invalid Radio type Custom Field Value is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  user hit "post" requests with "invalid" "invalidCustomFieldValue" field
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName									  				 |Message                      |
	|123      | 400  |SheetPath_EmailAcquistion   |SheetName_EmailAcquistion_Attributes  | CUSTOMINVALIDFIELDVALUE     |		
	
	

@smokeTest
Scenario Outline: EA123 - Email Acquisition Template Test- Validate the response when Non Mandatory Custom Radio Field with multiple valid values is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  user hit "post" requests with "multiple" "validCustomFieldValue" field
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName									  				 |Message                    |
	|124      | 400  |SheetPath_EmailAcquistion   |SheetName_EmailAcquistion_Attributes  | CUSTOMINVALIDFIELDVALUE   |		
	
	
	

@smokeTest
Scenario Outline: EA124 - Email Acquisition Template Test- Validate the response when blank custom id of DropDown field type Custom field data is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  user hit "post" requests with "blank" "customFieldId" field
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex 	| code		 |SheetPath											|SheetName					  								 |Message					     |
	|125        | 400  		 |SheetPath_EmailAcquistion   	|SheetName_EmailAcquistion_Attributes  |CUSTOMBLANKFIELDID   |
	
	
	
@smokeTest
Scenario Outline: EA125 - Email Acquisition Template Test- Validate the response when blank custom value of DropDown type Custom field data is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  user hit "post" requests with "blank" "customFieldValue" field
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex 	| code		 |SheetPath											|SheetName					  								 |Message					        |
	|126        | 400  		 |SheetPath_EmailAcquistion   	|SheetName_EmailAcquistion_Attributes  |CUSTOMBLANKFIELDVALUE   |
	

	
@smokeTest
Scenario Outline: EA126 - Email Acquisition Template Test- Validate the response when Invalid custom value of DropDown type Custom field is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  user hit "post" requests with "invalid" "invalidCustomFieldValue" field
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex 	| code		 |SheetPath											|SheetName					  								 |Message			   		        |
	|127        | 400  		 |SheetPath_EmailAcquistion   	|SheetName_EmailAcquistion_Attributes  |CUSTOMINVALIDFIELDVALUE   |
	
	

@smokeTest
Scenario Outline: EA127 - Email Acquisition Template Test- Validate the response when Custom Field with multiple valid values for DropDown Field is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  user hit "post" requests with "multiple" "validCustomFieldValue" field
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName											  				 | Message                   |
	|128      | 400  |SheetPath_EmailAcquistion   |SheetName_EmailAcquistion_Attributes      | CUSTOMINVALIDFIELDVALUE   |		
	

@smokeTest
Scenario Outline: EA128 - Email Acquisition Template Test- Validate the response when Non Mandatory Blank Custom DropDown Field Id is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  user hit "post" requests with "blank" "customFieldId" field
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName	    								  				 |Message                      |
	|129      | 400  |SheetPath_EmailAcquistion   |SheetName_EmailAcquistion_Attributes      | CUSTOMNONMANDATORYBLANKID   |	
	
	

@smokeTest
Scenario Outline: EA129 - Email Acquisition Template Test- Validate the response when Non Mandatory Invalid DropDown type Custom Field Value is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  user hit "post" requests with "invalid" "invalidCustomFieldValue" field
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName									  				 |Message                      |
	|130      | 400  |SheetPath_EmailAcquistion   |SheetName_EmailAcquistion_Attributes  | CUSTOMINVALIDFIELDVALUE     |		
	
	

@smokeTest
Scenario Outline: EA130 - Email Acquisition Template Test- Validate the response when Non Mandatory Custom DropDown Field with multiple valid values is sent in request.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  user hit "post" requests with "multiple" "validCustomFieldValue" field
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName									  				 |Message                    |
	|131      | 400  |SheetPath_EmailAcquistion   |SheetName_EmailAcquistion_Attributes  | CUSTOMINVALIDFIELDVALUE   |		
		