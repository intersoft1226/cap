@email_functional_suite_prod
Feature: Functional Validations of Email 
  

@NewEmailFunctionalityProd
Scenario Outline: Email DFD 01 - Validate using new email, flag for "isNewUser" is correct   
	Given User updated the field "email" in workbook "SheetName_EmailAcquistion" of file "SheetPath_EmailAcquistion" on row <Rowindex>
	And User want to hit "Email" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests 
	Then  User should received response status <code>
	And  User verify "isNewUser" flag of "emailResult" array  is "true" in results
	And user is waiting for 10000 seconds
	When  When user hit "post" requests
	And  User verify "isNewUser" flag of "emailResult" array  is "false" in results
	
	
	Examples:
	|Rowindex 	 	| code |SheetPath											|SheetName								 |
 	|2  	   			| 200  |SheetPath_EmailAcquistion 		|SheetName_EmailAcquistion |
	|3  	   			| 200  |SheetPath_EmailAcquistion 		|SheetName_EmailAcquistion |
	|4  	   			| 200  |SheetPath_EmailAcquistion 		|SheetName_EmailAcquistion |
	|5  	   			| 200  |SheetPath_EmailAcquistion 		|SheetName_EmailAcquistion |
	|6  	   			| 200  |SheetPath_EmailAcquistion 		|SheetName_EmailAcquistion |
	
	
@SpamEmailFunctionality
Scenario Outline: Email DFD 02 - Validate using spam email, flag for "isSpam" should be true   
	Given User want to hit "Email" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests 
	Then  User should received response status <code>
	And  User verify "isSpam" flag of "emailResult" array  is "true" in results
	And  User verify "isNewUser" flag of "emailResult" array  is "false" in results
	
	
	Examples:
	|Rowindex 	 	| code |SheetPath											|SheetName								 |
	|12  	   			| 200  |SheetPath_EmailAcquistion 		|SheetName_EmailAcquistion |	
	|13  	   			| 200  |SheetPath_EmailAcquistion 		|SheetName_EmailAcquistion |
	|14  	   			| 200  |SheetPath_EmailAcquistion 		|SheetName_EmailAcquistion |
	|15  	   			| 200  |SheetPath_EmailAcquistion 		|SheetName_EmailAcquistion |
	|16  	   			| 200  |SheetPath_EmailAcquistion 		|SheetName_EmailAcquistion |
	
	
	
@UndelEmailFunctionality
Scenario Outline: Email DFD 03 - Validate using undel email, flag for "isUndel" should be true   
	Given User want to hit "Email" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests 
	Then  User should received response status <code>
	And  User verify "isUndel" flag of "emailResult" array  is "true" in results
	And  User verify "isNewUser" flag of "emailResult" array  is "false" in results
	
	
	Examples:
	|Rowindex 	 	| code |SheetPath											|SheetName								 |
	|7  	   			| 200  |SheetPath_EmailAcquistion 		|SheetName_EmailAcquistion |	
	|8  	   			| 200  |SheetPath_EmailAcquistion 		|SheetName_EmailAcquistion |	
	|9  	   			| 200  |SheetPath_EmailAcquistion 		|SheetName_EmailAcquistion |	
	|10  	   			| 200  |SheetPath_EmailAcquistion 		|SheetName_EmailAcquistion |	
	|11  	   			| 200  |SheetPath_EmailAcquistion 		|SheetName_EmailAcquistion |		
	
	
	
#known Bug Lti not changing flag	
@LtiFunctionality
Scenario Outline: Email DFD 04 - Validate Lti email, flag for "isLti" should be true   
 Given User updated the field "email" in workbook "SheetName_EmailAcquistion" of file "SheetPath_EmailAcquistion" on row <Rowindex>
	And User want to hit "Email" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests 
	Then  User should received response status <code>
	And  User verify "isLti" flag of "emailResult" array  is "true" in results
	And  User verify "isNewUser" flag of "emailResult" array  is "false" in results
	
	
	
	Examples:
	|Rowindex 	 	| code |SheetPath											|SheetName								 |
	|17  	   			| 200  |SheetPath_EmailAcquistion 		|SheetName_EmailAcquistion |	
	
	
	
@optoutFunctionality2
Scenario Outline: Email DFD 07 - Validate optout email&lti,In template optin flag is true & ltiflag is false & email is optout, response field "isOptIn" should be true    
 Given User updated the field "email" in workbook "SheetName_EmailAcquistion" of file "SheetPath_EmailAcquistion" on row <Rowindex>
	And User want to hit "Email" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests 
	Then  User should received response status <code>
	And  User verify "isOptIn" flag of "emailResult" array  is "true" in results
	And  User verify "isNewUser" flag of "emailResult" array  is "false" in results
	
	
	Examples:
	|Rowindex 	 	| code |SheetPath											|SheetName								 |
	|18  	   			| 200  |SheetPath_EmailAcquistion 		|SheetName_EmailAcquistion |
	
	
	

	