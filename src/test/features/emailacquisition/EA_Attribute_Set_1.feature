# Email Acquisition Field validations covered for fields - Title, PostalCode, 
# Province, State, Phone, MOCode, FirstName, LastName, EmailConsent, Email, City, 
# BounceXFields, BabyField, Address1, Address2, MobilePhone, Zipcode
Feature: Validating Field Validation for Sign Up services of Email Acquisition template type. 


@smokeTest
Scenario Outline: EA001 - Email Acquisition Template Test- Validate the valid response when valid input parameters is provided.
	Given User want to hit "EmailAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	
	Examples:	
	|Rowindex | code |SheetPath													|SheetName									              |
	|2        | 200  |SheetPath_EmailAcquistion   			|SheetName_EmailAcquistion_Attributes     |



@smokeTest
Scenario Outline: EA002 - Email Acquisition Template Test- Validate the response when Blank Mobile Phone is sent in request.
	Given User want to hit "EmailAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests with "blank" "mobilePhone" field 
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath											|SheetName					  										|Message			   			|
	|3        | 400  |SheetPath_EmailAcquistion   	|SheetName_EmailAcquistion_Attributes     |MOBILEPHONEREQUIRED  |
#	


@smokeTest
Scenario Outline: EA003 - Email Acquisition Template Test- Validate the response when Invalid Mobile Phone is sent in request.
	Given User want to hit "EmailAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message					  		   |
	|4        | 400  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     | MOBILEPHONEWITHLESSTHANMINLENGTH   |
	


@smokeTest
Scenario Outline: EA004 - Email Acquisition Template Test- Validate the response when Mobile Phone Exceeding Max Length is sent in request.
	Given User want to hit "EmailAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message					  	    |
	|5        | 400  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     |MOBILEPHONEEXCEEDINGMAXLENGTH    |

	

@smokeTest
Scenario Outline: EA005 - Email Acquisition Template Test- Validate the response when Mobile Phone less than Min Length is sent in request.
	Given User want to hit "EmailAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message					  		   |
	|6        | 400  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     |MOBILEPHONEWITHLESSTHANMINLENGTH    |
	
	
@smokeTest
Scenario Outline: EA006 - Email Acquisition Template Test- Validate the response when Mobile Phone having Two Hypens Invalid Format is sent in request.
	Given User want to hit "EmailAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message					  			    |
	|7        | 400  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     |MOBILEPHONEWITHTWOHYPENSINVALIDFORMAT    |
		

@smokeTest
Scenario Outline: EA007 - Email Acquisition Template Test- Validate the response when Mobile Phone having AlphaNumeric value is sent in request.
	Given User want to hit "EmailAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message					  	  |
	|8        | 400  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     |MOBILEPHONEWITHALPHANUMERIC    |


@smokeTest
Scenario Outline: EA008 - Email Acquisition Template Test- Validate the response when Mobile Phone having 12Digit value is sent in request.
	Given User want to hit "EmailAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message					 |
	|9        | 400  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     |MOBILEPHONEWITH12DIGIT    |
	
	

@smokeTest
Scenario Outline: EA009 - Email Acquisition Template Test- Validate the response when address1 with Exceeding Max Length is sent in request.
	Given User want to hit "EmailAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message					     |
	|10        | 400  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     |ADDRESS1EXCEEDINGMAXLENGTH    |
	


@smokeTest
Scenario Outline: EA010 - Email Acquisition Template Test- Validate the response when address1 with Alphanumeric is sent in request.
	Given User want to hit "EmailAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message  |
	|11       | 200  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     | success |
	


@smokeTest
Scenario Outline: EA011 - Email Acquisition Template Test- Validate the response when address1 with SpecialCharacters is sent in request.
	Given User want to hit "EmailAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath											|SheetName									  |Message    |
	|12       | 200  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     | success   |	
	


@smokeTest
Scenario Outline: EA012 - Email Acquisition Template Test- Validate the response when address2 with Exceeding Max Length is sent in request.
	Given User want to hit "EmailAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath												|SheetName									  |Message					    				 |
	|13        | 400  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     |ADDRESS2EXCEEDINGMAXLENGTH    |
	


@smokeTest
Scenario Outline: EA013 - Email Acquisition Template Test- Validate the response when address2 with Alphanumeric is sent in request.
	Given User want to hit "EmailAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message  |
	|14       | 200  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     | success |
	


@smokeTest
Scenario Outline: EA013 - Email Acquisition Template Test- Validate the response when address2 with SpecialCharacters is sent in request.
	Given User want to hit "EmailAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message    |
	|15       | 200  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     | success   |	


	
	
@smokeTest
Scenario Outline: EA014 - Email Acquisition Template Test- Validate the response when babyField Exceeding MaxValue is sent in request.
	Given User want to hit "EmailAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message                       |
	|16       | 400  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     | BABYFIELDEXCEEDINGMAXVALUE   |	
	
	
		
@smokeTest
Scenario Outline: EA015 - Email Acquisition Template Test- Validate the response when babyField With LessThan MinValue is sent in request.
	Given User want to hit "EmailAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message                          |
	|17       | 400  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     | BABYFIELDWITHLESSTHANMINVALUE   |
	
# This will be valid if Baby Field is non mandatory		
#@smokeTest
#@currentTest1
#Scenario Outline: EA016 - Email Acquisition Template Test- Validate the response when babyField Accepting Zero is sent in request.
#	Given User want to hit "EmailAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
#	When  When user hit "post" requests
#	Then  User should received response status <code>
#	And   User validated the response <Message> received
#	
#	Examples:	
#	|Rowindex | code |SheetPath							|SheetName					  |Message    |
#	|18       | 200  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     | success   |	
#	
#@currentTest		
#@smokeTest
#Scenario Outline: EA017 - Email Acquisition Template Test- Validate the response when babyField Count Less Than ChildrenCount is sent in request.
#	Given User want to hit "EmailAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
#	When  user hit "post" requests with BabyField less than children count
#	Then  User should received response status <code>
#	And   User validated the response <Message> received
#	
#	Examples:	
#	|Rowindex | code |SheetPath							|SheetName					  |Message                                |
#	|19       | 200  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     | BABYFIELDCOUNTLESSTHANCHILDRENCOUNT   |		
	
		
@smokeTest
Scenario Outline: EA018 - Email Acquisition Template Test- Validate the response when CmpDeploymentStrategy field exceeds MaxLength is sent in request.
	Given User want to hit "EmailAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message                           |
	|20       | 400  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     | CMPDEPLOYMENTSTRATEGYMAXLENGTH   |		
	
	
		
@smokeTest
Scenario Outline: EA019 - Email Acquisition Template Test- Validate the response when CmpNameMaxLength field exceeds MaxLength is sent in request.
	Given User want to hit "EmailAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message             |
	|21       | 400  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     | CMPNAMEMAXLENGTH   |	
	
		
@smokeTest
Scenario Outline: EA020 - Email Acquisition Template Test- Validate the response when CmpVariationID field exceeds MaxLength is sent in request.
	Given User want to hit "EmailAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message                    |
	|22       | 400  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     | CMPVARIATIONIDMAXLENGTH   |	
	
	
	
		
@smokeTest
Scenario Outline: EA021 - Email Acquisition Template Test- Validate the response when CmpConcept field exceeds MaxLength is sent in request.
	Given User want to hit "EmailAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message                |
	|23       | 400  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     | CMPCONCEPTMAXLENGTH   |	
	
	
	
@smokeTest
Scenario Outline: EA022 - Email Acquisition Template Test- Validate the response when CmpDevice field exceeds MaxLength is sent in request.
	Given User want to hit "EmailAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message               |
	|24       | 400  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     | CMPDEVICEMAXLENGTH   |	
	
	
	
@smokeTest
Scenario Outline: EA023 - Email Acquisition Template Test- Validate the response when CmpFormat field exceeds MaxLength is sent in request.
	Given User want to hit "EmailAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message               |
	|25       | 400  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     | CMPFORMATMAXLENGTH   |		
	
	
	
@smokeTest
Scenario Outline: EA024 - Email Acquisition Template Test- Validate the response when City field exceeds MaxLength is sent in request.
	Given User want to hit "EmailAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message                   |
	|26       |400  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     | CITYEXCEEDINGMAXLENGTH   |		
	
	
		
@smokeTest
Scenario Outline: EA025 - Email Acquisition Template Test- Validate the response when City Accepting Alphanumeric is sent in request.
	Given User want to hit "EmailAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message    |
	|27       | 200  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     | success   |		
	
	
@smokeTest
Scenario Outline: EA027 - Email Acquisition Template Test- Validate the response when City Accepting SpecialCharacters is sent in request.
	Given User want to hit "EmailAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message    |
	|28       | 200  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     | success   |		
	
		
@smokeTest
Scenario Outline: EA028 - Email Acquisition Template Test- Validate the response when email Without At The Rate is sent in request.
	Given User want to hit "EmailAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message                  |
	|29       | 400  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     | EMAILWITHOUTATTHERATE   |	
	
		
@smokeTest
Scenario Outline: EA029 - Email Acquisition Template Test- Validate the response when email Without At Dot is sent in request.
	Given User want to hit "EmailAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message            |
	|30       | 400  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     | EMAILWITHOUTDOT   |	
	
	
	
		
@smokeTest
Scenario Outline: EA030 - Email Acquisition Template Test- Validate the response when email With No Prefix before At The Rate is sent in request.
	Given User want to hit "EmailAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message                             |
	|31       | 400  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     | EMAILWITHNOPREFIXBEFOREATTHERATE   |
	
	
		
@smokeTest
Scenario Outline: EA031 - Email Acquisition Template Test- Validate the response when email With exceeding Max length is sent in request.
	Given User want to hit "EmailAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message                               |
	|32       | 400  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     | EMAILWITHEMAILIDEXCEEDINGMAXLENGTH   |	
	
		
@smokeTest
Scenario Outline: EA032 - Email Acquisition Template Test- Validate the response when email With Multiple At the rate is sent in request.
	Given User want to hit "EmailAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message                               |
	|33       | 400  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     | EMAILWITHMULTIPLEATTHERATEINEMAIL    |	
	
		
@smokeTest
Scenario Outline: EA033 - Email Acquisition Template Test- Validate the response when email With SpecialCharacters is sent in request.
	Given User want to hit "EmailAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message                        |
	|34       | 400  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     | EMAILWITHSPECIALCHARACTERS    |		
	
		
@smokeTest
Scenario Outline: EA034 - Email Acquisition Template Test- Validate the response when email With Spaces is sent in request.
	Given User want to hit "EmailAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message            |
	|35       | 400  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     | EMAILWITHSPACE    |	
	

		
@smokeTest
Scenario Outline: EA035 - Email Acquisition Template Test- Validate the response when email With Leading Spaces is sent in request.
	Given User want to hit "EmailAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message     |
	|36       | 200  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     | success    |	
	
		
@smokeTest
Scenario Outline: EA036 - Email Acquisition Template Test- Validate the response when email With Trailing Spaces is sent in request.
	Given User want to hit "EmailAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message     |
	|37       | 200  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     | success    |	
	
		
@smokeTest
Scenario Outline: EA037 - Email Acquisition Template Test- Validate the response when EMAILCONSENT2MANDATORY is sent in request.
	Given User want to hit "EmailAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message            |
	|38       | 400  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     | EMAILCONSENT2MANDATORY    |							
	


@smokeTest
Scenario Outline: EA038 - Email Acquisition Template Test- Validate the response when FirstName with Exceeding Max Length is sent in request.
	Given User want to hit "EmailAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message					      |
	|39        | 400  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     |FIRSTNAMEEXCEEDINGMAXLENGTH    |
	


@smokeTest
Scenario Outline: EA039 - Email Acquisition Template Test- Validate the response when FirstName with Alphanumeric is sent in request.
	Given User want to hit "EmailAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message  |
	|40       | 200  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     | success |
	


@smokeTest
Scenario Outline: EA040 - Email Acquisition Template Test- Validate the response when FirstName with SpecialCharacters is sent in request.
	Given User want to hit "EmailAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message    |
	|41       | 200  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     | success   |	
	
	
						
@smokeTest
Scenario Outline: EA041 - Email Acquisition Template Test- Validate the response when LastName with Exceeding Max Length is sent in request.
	Given User want to hit "EmailAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message					      |
	|42        | 400  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     |LASTNAMEEXCEEDINGMAXLENGTH     |
	


@smokeTest
Scenario Outline: EA042 - Email Acquisition Template Test- Validate the response when LastName with Alphanumeric is sent in request.
	Given User want to hit "EmailAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message  |
	|43       | 200  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     | success |
	


@smokeTest
Scenario Outline: EA043 - Email Acquisition Template Test- Validate the response when LastName with SpecialCharacters is sent in request.
	Given User want to hit "EmailAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message    |
	|44       | 200  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     | success   |	
	
	

@smokeTest
Scenario Outline: EA044 - Email Acquisition Template Test- Validate the response when MoCode Exceeding Max Length is sent in request.
	Given User want to hit "EmailAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message					   |
	|45        | 400  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     |MOCODEEXCEEDINGMAXLENGTH    |

	

@smokeTest
Scenario Outline: EA045 - Email Acquisition Template Test- Validate the response when MOCode less than Min Length is sent in request.
	Given User want to hit "EmailAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message					      |
	|46       | 400  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     |MOCODELESSTHANMINIMUMLENGTH    |
	
	
@smokeTest
Scenario Outline: EA046 - Email Acquisition Template Test- Validate the response when MO Code Alphanumeric is sent in request.
	Given User want to hit "EmailAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message					  			    |
	|47        | 200  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     |success    |
		

@smokeTest
Scenario Outline: EA047 - Email Acquisition Template Test- Validate the response when MOCode having SpecialCharacters value is sent in request.
	Given User want to hit "EmailAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message					  	  |
	|48        | 400  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     |MOCODEWITHSPECIALCHARACTERS    |
	

@smokeTest
Scenario Outline: EA048 - Email Acquisition Template Test- Validate the response when MOCode having HyphenValidFormat value is sent in request.
	Given User want to hit "EmailAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message	   |
	|49        | 200  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     |success    |	
	

@smokeTest
Scenario Outline: EA049 - Email Acquisition Template Test- Validate the response when MOCode having Hyphen InValidFormat value is sent in request.
	Given User want to hit "EmailAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message	                        |
	|50        | 400  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     |MOCODEWITHHYPHENINVALIDFORMAT    |	
		
#Mo Code ends	


@smokeTest
Scenario Outline: EA050 - Email Acquisition Template Test- Validate the response when Phone Exceeding Max Length is sent in request.
	Given User want to hit "EmailAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message					   |
	|51        | 400  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     |PHONEEXCEEDINGMAXLENGTH    |

	

@smokeTest
Scenario Outline: EA051 - Email Acquisition Template Test- Validate the response when Phone less than Min Length is sent in request.
	Given User want to hit "EmailAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message					      |
	|52       | 400  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     |PHONEWITHLESSTHANMINLENGTH    |
	

@smokeTest
Scenario Outline: EA052 - Email Acquisition Template Test- Validate the response when phone With Two Hypens InvalidFormat is sent in request.
	Given User want to hit "EmailAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message					  			    |
	|53        | 400  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     |PHONEWITHTWOHYPENSINVALIDFORMAT    |
		

@smokeTest
Scenario Outline: EA053 - Email Acquisition Template Test- Validate the response when Phone having AlphaNumeric value is sent in request.
	Given User want to hit "EmailAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message					  	  |
	|54        | 400  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     |PHONEWITHALPHANUMERIC    |
	

@smokeTest
Scenario Outline: EA054 - Email Acquisition Template Test- Validate the response when Phone having 12Digit value is sent in request.
	Given User want to hit "EmailAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message	   |
	|55        | 400  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     |PHONEWITH12DIGIT    |	
	

@smokeTest
Scenario Outline: EA055 - Email Acquisition Template Test- Validate the response when Phone having Single Hyphen InValidFormat value is sent in request.
	Given User want to hit "EmailAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message    |
	|56        | 200  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     |success    |	
		
		

@smokeTest
Scenario Outline: EA056 - Email Acquisition Template Test- Validate the response when PostalCode Exceeding Max Length is sent in request.
	Given User want to hit "EmailAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message					   |
	|57        | 400  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     |POSTALCODEEXCEEDINGMAXLENGTH    |

	

@smokeTest
Scenario Outline: EA057 - Email Acquisition Template Test- Validate the response when PostalCode with Invalid Format is sent in request.
	Given User want to hit "EmailAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message					      |
	|58       | 400  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     |POSTALCODEWITHINVALIDFORMAT    |
	


@smokeTest
Scenario Outline: EA058 - Email Acquisition Template Test- Validate the response when PostalCode With SpecialCharacters is sent in request.
	Given User want to hit "EmailAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message					  			    |
	|59        | 400  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     |POSTALCODEWITHSPECIALCHARACTER    |
		


@smokeTest
Scenario Outline: EA059 - Email Acquisition Template Test- Validate the response when PostalCode with Invalid Format2 value is sent in request.
	Given User want to hit "EmailAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message					  	  |
	|60        | 400  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     |POSTALCODEWITHINVALIDFORMAT2    |
	

@smokeTest
Scenario Outline: EA060 - Email Acquisition Template Test- Validate the response when PostalCode WITH INVALID FORMAT3 value is sent in request.
	Given User want to hit "EmailAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message	   |
	|61        | 400  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     |POSTALCODEWITHINVALIDFORMAT3    |	
	
	

@smokeTest
Scenario Outline: EA061 - Email Acquisition Template Test- Validate the response when province Exceeding Max Length is sent in request.
	Given User want to hit "EmailAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message					   |
	|62        | 400  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     |PROVINCEEXCEEDINGMAXLENGTH    |

	

@smokeTest
Scenario Outline: EA062 - Email Acquisition Template Test- Validate the response when province with AlphaNumeric is sent in request.
	Given User want to hit "EmailAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message					      |
	|63       | 400  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     |PROVINCEACCEPTINGALPHANUMERIC    |
	


@smokeTest
Scenario Outline: EA063 - Email Acquisition Template Test- Validate the response when province With SpecialCharacters is sent in request.
	Given User want to hit "EmailAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message					  			    |
	|64        | 400  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     |PROVINCEACCEPTINGSPECIALCHARACTERS    |
		

@smokeTest
Scenario Outline: EA064 - Email Acquisition Template Test- Validate the response when province with Invalid value value is sent in request.
	Given User want to hit "EmailAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message					  	  |
	|65        | 400  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     |PROVINCEWITHINVALIDVALUE    |
	
	

@smokeTest
Scenario Outline: EA065 - Email Acquisition Template Test- Validate the response when State Exceeding Max Length is sent in request.
	Given User want to hit "EmailAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message					   |
	|66        | 400  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     |STATEEXCEEDINGMAXLENGTH    |

	

@smokeTest
Scenario Outline: EA066 - Email Acquisition Template Test- Validate the response when state With LessThan Min Length is sent in request.
	Given User want to hit "EmailAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message					      |
	|67       | 400  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     |STATEWITHLESSTHANMINLENGTH    |
	


@smokeTest
Scenario Outline: EA067 - Email Acquisition Template Test- Validate the response when state With Non Capital Valid Value is sent in request.
	Given User want to hit "EmailAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message					  			    |
	|68        | 400  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     |STATEWITHNONCAPITALVALIDVALUE    |
		


@smokeTest
Scenario Outline: EA068 - Email Acquisition Template Test- Validate the response when state With Special Characters is sent in request.
	Given User want to hit "EmailAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message					  	  |
	|69        | 400  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     |STATEWITHSPECIALCHARACTERS    |
	

@smokeTest
Scenario Outline: EA069 - Email Acquisition Template Test- Validate the response when state With AlphaNumeric is sent in request.
	Given User want to hit "EmailAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message	   |
	|70        | 400  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     |STATEWITHALPHANUMERIC    |	
	

@smokeTest
Scenario Outline: EA070 - Email Acquisition Template Test- Validate the response when PostalCode having Single Hyphen InValidFormat value is sent in request.
	Given User want to hit "EmailAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message    |
	|71        | 400  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     |STATEWITHNUMERICVALUE    |	
		
		
	

@smokeTest
Scenario Outline: EA071 - Email Acquisition Template Test- Validate the response when Title Exceeding Max Length is sent in request.
	Given User want to hit "EmailAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message					   |
	|72        | 400  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     |TITLEEXCEEDINGMAXLENGTH    |

	

@smokeTest
Scenario Outline: EA072 - Email Acquisition Template Test- Validate the response when title Accepting Alphanumeric is sent in request.
	Given User want to hit "EmailAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message					      |
	|73       | 200  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     |success    |
	


@smokeTest
Scenario Outline: EA073 - Email Acquisition Template Test- Validate the response when title Accepting Special Characters is sent in request.
	Given User want to hit "EmailAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message					  			    |
	|74        | 200  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     |success    |
		

	

@smokeTest
Scenario Outline: EA074 - Email Acquisition Template Test- Validate the response when zip Exceeding Max Length is sent in request.
	Given User want to hit "EmailAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message					   |
	|75        | 400  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     |ZIPEXCEEDINGMAXLENGTH    |

	

@smokeTest
Scenario Outline: EA075 - Email Acquisition Template Test- Validate the response when zip With Less Than MaxLength is sent in request.
	Given User want to hit "EmailAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message					      |
	|76       | 400  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     |ZIPWITHLESSTHANMAXLENGTH    |
	

@smokeTest
Scenario Outline: EA076 - Email Acquisition Template Test- Validate the response when zip With CharAndDigit InValid Value is sent in request.
	Given User want to hit "EmailAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message					  			    |
	|77        | 400  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     |ZIPWITHCHARANDDIGITINVALIDVALUE    |
		


@smokeTest
Scenario Outline: EA077 - Email Acquisition Template Test- Validate the response when zip With Special Characters is sent in request.
	Given User want to hit "EmailAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message					  	  |
	|78        | 400  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     |ZIPWITHSPECIALCHARACTERS    |
	


@smokeTest
Scenario Outline: EA078 - Email Acquisition Template Test- Validate the response when zip With AlphaNumeric is sent in request.
	Given User want to hit "EmailAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message	   |
	|79        | 400  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     |ZIPWITHALPHANUMERIC    |	
	
	

@smokeTest
Scenario Outline: EA079 - Email Acquisition Template Test- Validate the response when zip With Numeric Value  is sent in request.
	Given User want to hit "EmailAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message    |
	|80        | 400  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     |ZIPWITHNUMERICVALUE    |	
		
		
		

@smokeTest
Scenario Outline: EA080 - Email Acquisition Template Test- Validate the response when zip With Numeric And Char Value  is sent in request.
	Given User want to hit "EmailAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message    |
	|81        | 400  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     |ZIPWITHNUMERICANDCHARVALUE    |	
			
	
@smokeTest
Scenario Outline: EA081 - Email Acquisition Template Test- Validate the response when zip With 10Numeric Value  is sent in request.
	Given User want to hit "EmailAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath												|SheetName					  										|Message    |
	|82       | 200  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     |success    |	
	

	
	
			
		
		